﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Infrastructure.Initialization;
using LoremIpsumCMS.Infrastructure.Modules;

namespace LoremIpsumCMS.Module.Blogs
{

    /// <summary>
    /// 
    /// </summary>
    [DependsOn(typeof(CoreModule))]
    public class BlogsModule : AbstractModule
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public override string Name => "Lorem Ipsum Blog";

        #endregion

        #region Configure Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public override void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.Install(i => i.FromThisAssembly());

            serviceCollection.Register(r => r
                .FromThisAssembly()
                .Where(c => c.AssignableTo<IInitializer>())
                .WithServices.AllInterfaces());
        }

        #endregion

    }

}

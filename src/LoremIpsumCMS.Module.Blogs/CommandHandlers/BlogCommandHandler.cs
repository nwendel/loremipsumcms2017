﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Module.Blogs.Commands;
using LoremIpsumCMS.Module.Blogs.Events;

namespace LoremIpsumCMS.Module.Blogs.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class BlogCommandHandler :
        IHandleCommand<CreateBlogCommand>
    {

        #region Handle Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreateBlogCommand command)
        {
            yield return new BlogCreatedEvent
            {
                SiteId = command.SiteId,
                Name = command.Name,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

    }

}

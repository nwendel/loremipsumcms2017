﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Module.Blogs.Events;
using LoremIpsumCMS.Module.Blogs.Indexes;
using LoremIpsumCMS.Module.Blogs.Indexes.UniqueConstraints;
using LoremIpsumCMS.Module.Blogs.Model;
using LoremIpsumCMS.Module.Blogs.Model.UniqueConstraints;

namespace LoremIpsumCMS.Module.Blogs.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class BlogEventHandler :
        ISubscribeTo<BlogDeletedEvent>,
        ISubscribeTo<SiteDeletedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public BlogEventHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Handle Site Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(BlogDeletedEvent @event)
        {
            var blog = _documentSession.Load<Blog>(@event.Id);
            _documentSession.Delete(blog);
            _documentSession.Advanced.DeleteByQuery<BlogAwareUniqueConstraint, BlogAwareUniqueConstrantIndex>(x => x.BlogId == @event.Id && x.EntityName == nameof(Blog));
        }

        #endregion

        #region Handle Site Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteDeletedEvent @event)
        {
            _documentSession.Advanced.DeleteByQuery<Blog, BlogIndex>(x => x.SiteId == @event.Id);
            _documentSession.Advanced.DeleteByQuery<BlogAwareUniqueConstraint, BlogAwareUniqueConstrantIndex>(x => x.SiteId == @event.Id && x.EntityName == nameof(Blog));
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Blogs.Model;

namespace LoremIpsumCMS.Module.Blogs.Events
{

    /// <summary>
    /// 
    /// </summary>
    public class PostCreatedEvent : AbstractEvent
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string BlogId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public HtmlText Body { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? PublishAt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PostStatus Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> CategoryIds { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> Tags { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; set; }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Model.UniqueConstraints;
using LoremIpsumCMS.Module.Blogs.Events;

namespace LoremIpsumCMS.Module.Blogs.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Blog : AbstractAggregate, ISiteIdAware,
        IApplyEvent<BlogCreatedEvent>,
        IApplyEvent<BlogUpdatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string SiteIdAndNameSlug => $"{SiteId}/{NameSlug}";

        /// <summary>
        /// 
        /// </summary>
        public HtmlText Description { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(BlogCreatedEvent @event)
        {
            SiteId = @event.SiteId;
            Name = @event.Name;
            Description = @event.Description;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(BlogUpdatedEvent @event)
        {
            Name = @event.Name;
            Description = @event.Description;
        }

        #endregion

    }

}

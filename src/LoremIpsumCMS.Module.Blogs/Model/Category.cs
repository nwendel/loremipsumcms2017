﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Module.Blogs.Events;
using LoremIpsumCMS.Module.Blogs.Model.UniqueConstraints;

namespace LoremIpsumCMS.Module.Blogs.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Category : AbstractAggregate, IBlogIdAware,
        IApplyEvent<CategoryCreatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string BlogId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        [BlogAwareUniqueConstraint]
        public string SiteIdAndNameSlug => $"{SiteId}/{NameSlug}";

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(CategoryCreatedEvent @event)
        {
            SiteId = @event.SiteId;
            BlogId = @event.BlogId;
            Name = @event.Name;
        }

        #endregion

    }

}

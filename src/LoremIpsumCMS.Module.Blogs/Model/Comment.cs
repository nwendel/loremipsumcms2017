﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Module.Blogs.Events;
using LoremIpsumCMS.Module.Blogs.Model.UniqueConstraints;

namespace LoremIpsumCMS.Module.Blogs.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Comment : AbstractAggregate, IBlogIdAware,
        IApplyEvent<CommentCreatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string BlogId { get; protected set; }
        
        /// <summary>
        /// 
        /// </summary>
        public string PostId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Text { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(CommentCreatedEvent @event)
        {
            SiteId = @event.SiteId;
            BlogId = @event.BlogId;
            PostId = @event.PostId;
            Text = @event.Text;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

    }

}

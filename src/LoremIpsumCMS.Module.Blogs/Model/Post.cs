﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Blogs.Events;
using LoremIpsumCMS.Module.Blogs.Model.UniqueConstraints;

namespace LoremIpsumCMS.Module.Blogs.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Post : AbstractAggregate, IBlogIdAware,
        IApplyEvent<PostCreatedEvent>,
        IApplyEvent<PostUpdatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string BlogId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime Date => PublishAtActual.Date;

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string TitleSlug => Title.Slugify();

        /// <summary>
        /// 
        /// </summary>
        [BlogAwareUniqueConstraint]
        public string BlogIdAndDateAndTitleSlug => $"{BlogId}/{Date:d}/{TitleSlug}";

        /// <summary>
        /// 
        /// </summary>
        public HtmlText Body { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? PublishAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime PublishAtActual => PublishAt ?? CreatedAt;

        /// <summary>
        /// 
        /// </summary>
        public PostStatus Status { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> CategoryIds { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> Tags { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> TagSlugs => Tags?.Select(x => x.Slugify());

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdatedAt { get; protected set; }

        #endregion

        #region Is Public

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsPublic()
        {
            var isPublic = Status == PostStatus.Published &&
                           PublishAtActual < DateTime.Now;
            return isPublic;
        }

        #endregion

        #region Is Publish Later

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool IsPublishLater()
        {
            var isPublishLater = Status == PostStatus.Published &&
                                 PublishAtActual > DateTime.Now;
            return isPublishLater;
        }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PostCreatedEvent @event)
        {
            SiteId = @event.SiteId;
            BlogId = @event.BlogId;
            Title = @event.Title;
            Body = @event.Body;
            PublishAt = @event.PublishAt;
            Status = @event.Status;
            CategoryIds = @event.CategoryIds;
            Tags = @event.Tags;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PostUpdatedEvent @event)
        {
            Title = @event.Title;
            Body = @event.Body;
            PublishAt = @event.PublishAt;
            Status = @event.Status;
            CategoryIds = @event.CategoryIds;
            Tags = @event.Tags;
            LastUpdatedAt = @event.UpdatedAt;
        }

        #endregion

    }

}

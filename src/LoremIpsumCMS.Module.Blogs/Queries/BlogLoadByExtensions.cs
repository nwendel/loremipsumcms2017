﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Threading.Tasks;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Blogs.Model;

namespace LoremIpsumCMS.Module.Blogs.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class BlogLoadByExtensions
    {

        #region Login Provider And Key

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="siteReference"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public static Blog SiteReferenceAndNameSlug(this DocumentSessionLoadBy<Blog> self, SiteReference siteReference, string nameSlug)
        {
            if (siteReference == null)
            {
                throw new ArgumentNullException(nameof(siteReference));
            }
            if (string.IsNullOrWhiteSpace(nameSlug))
            {
                throw new ArgumentNullException(nameof(nameSlug));
            }

            var blog = self.Session.LoadByUniqueConstraint<Blog>(x => x.SiteIdAndNameSlug, $"{siteReference.SiteId}/{nameSlug}");
            return blog;
        }

        #endregion

    }

}

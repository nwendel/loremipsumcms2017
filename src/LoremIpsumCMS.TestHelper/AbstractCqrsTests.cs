﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.TestHelper
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractCqrsTests<TAggregate> : AbstractRavenTests
            where TAggregate : AbstractAggregate, new()

    {

        /// <summary>
        /// 
        /// </summary>
        protected AbstractCqrsTests()
        {
            DocumentSession = GetDocumentStore().OpenSession();
        }

        /// <summary>
        /// 
        /// </summary>
        protected IDocumentSession DocumentSession { get; }

        /// <summary>
        /// 
        /// </summary>
        protected object CommandHandler { private get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="given"></param>
        /// <param name="when"></param>
        /// <param name="then"></param>
        protected void Test(Func<TAggregate> given, Func<TAggregate, object> when, Action<object> then)
        {
            var aggregate = given();
            var events = when(aggregate);
            then(events);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="events"></param>
        /// <returns></returns>
        protected Func<TAggregate> Given(params AbstractEvent[] events)
        {
            return () =>
            {
                var aggregate = new TAggregate();
                foreach (var @event in events)
                {
                    aggregate.ApplyEvent(@event);
                }

                // TODO: Is this correct to not create the aggregate if there are no events?
                if (events.Any())
                {
                    DocumentSession.Store(aggregate);
                }

                return aggregate;
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TCommand"></typeparam>
        /// <param name="command"></param>
        /// <returns></returns>
        protected Func<TAggregate, object> When<TCommand>(TCommand command)
            where TCommand : AbstractCommand<TAggregate>
        {
            return aggregate =>
            {
                try
                {
                    // TODO: Is this correct to not set id if there were no events?
                    if (aggregate.Id != null)
                    {
                        var propertyInfo = command.GetType().GetProperty("Id");
                        if (propertyInfo != null)
                        {
                            propertyInfo.SetValue(command, aggregate.Id);
                        }
                    }

                    // TODO: There is too much code here which is similar to code in the RavenCommandSender, can't I use the command sender instead?
                    if (CommandHandler is IValidateCommand<TCommand> validator)
                    {
                        validator.Validate(command);
                    }

                    if (!(CommandHandler is IHandleCommand<TCommand> handler))
                    {
                        throw new CqrsException(string.Format(
                            "Command handler {0} does not yet handle command {1}",
                            CommandHandler.GetType().Name,
                            command.GetType().Name));
                    }

                    var events = handler.Handle(command);
                    var appliedEvents = new List<AbstractEvent>();
                    foreach (var @event in events)
                    {
                        if (@event.GetType().GetCustomAttribute<SkipAggregateApplyAttribute>() == null)
                        {
                            // TODO: Check if should be applied here...
                            aggregate.ApplyEvent(@event);
                        }
                        appliedEvents.Add(@event);
                    }

                    return appliedEvents.ToArray();
                }
                catch (Exception e)
                {
                    return e;
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expectedEvents"></param>
        /// <returns></returns>
        protected Action<object> Then(params AbstractEvent[] expectedEvents)
        {
            return got =>
            {
                switch (got)
                {
                    case AbstractEvent[] gotEvents:
                        var gotEventNames = gotEvents.Select(x => x.GetType().Name).ToList();
                        var expectedEventNames = expectedEvents.Select(x => x.GetType().Name).ToList();

                        if (gotEvents.Length == expectedEvents.Length)
                        {
                            for (var index = 0; index < gotEvents.Length; index++)
                            {
                                var gotEvent = gotEvents[index];
                                var expectedEvent = expectedEvents[index];

                                // I don't know which Id I will get...
                                expectedEvent.Id = gotEvent.Id;

                                if (gotEvent.GetType() == expectedEvent.GetType())
                                {
                                    Assert.Equal(Serialize(expectedEvent), Serialize(gotEvent));
                                }
                                else
                                {
                                    Assert.True(false, string.Format("Unexpected event(s) emitted: {0}",
                                        string.Join(", ", gotEventNames.Except(expectedEventNames))));
                                }
                            }
                        }
                        else if (gotEvents.Length < expectedEvents.Length)
                        {
                            Assert.True(false, string.Format("Expected event(s) missing: {0}",
                                string.Join(", ", expectedEventNames.Except(gotEventNames))));
                        }
                        else
                        {
                            Assert.True(false, string.Format("Unexpected event(s) emitted: {0}",
                                string.Join(", ", gotEventNames.Except(expectedEventNames))));
                        }
                        break;
                    case CqrsException ex:
                        Assert.True(false, ex.Message);
                        break;
                    case Exception _:
                        var exceptionName = got.GetType().Name;
                        Assert.True(false, string.Format("Expected events, but got exception {0}", exceptionName));
                        break;
                    default:
                        Assert.True(false, "Internal error executing test, expected events or exception");
                        break;
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="message"></param>
        /// <returns></returns>
        protected Action<object> ThenFailWithException<TException>(string message = null)
            where TException : Exception
        {
            return got =>
            {
                switch (got)
                {
                    case TException ex:
                        if (message != null && message != ex.Message)
                        {
                            Assert.True(false, string.Format("expected exception message '{0}' got '{1}'", message, ex.Message));
                        }
                        break;
                    case CqrsException ex:
                        Assert.True(false, ex.Message);
                        break;
                    case Exception _:
                        Assert.True(false, string.Format("Expected exception {0}, but got exception {1}",
                            typeof(TException).Name, got.GetType().Name));
                        break;
                    case IEnumerable<AbstractEvent> _:
                        Assert.True(false, string.Format("Expected exception {0}, but got event result",
                            typeof(TException).Name));
                        break;
                    default:
                        Assert.True(false, "Internal error executing test, expected events or exception");
                        break;
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private static string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

    }
}


﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents;
using Raven.Client.Documents.Indexes;
using Raven.TestDriver;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;

namespace LoremIpsumCMS.TestHelper
{

    /// <summary>
    /// 
    /// </summary>
    public class AbstractRavenTests : RavenTestDriver<LocalRavenServerLocator>
    {

        #region Pre Initialize

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        protected override void PreInitialize(IDocumentStore documentStore)
        {
            var listener = new UniqueConstraintsListener();
            documentStore.OnBeforeStore += listener.OnBeforeStore;
            documentStore.OnBeforeDelete += listener.OnBeforeDelete;
        }

        #endregion

        #region Create Index

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TIndex"></typeparam>
        protected void CreateIndex<TIndex>(IDocumentStore documentStore)
            where TIndex : AbstractIndexCreationTask, new()
        {
            var index = new TIndex();
            index.Execute(documentStore);
        }

        #endregion

    }

}

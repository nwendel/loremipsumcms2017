﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.CommandHandlers;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class IdentityUserCommandHandlerTests : AbstractCqrsTests<IdentityUser>
    {

        /// <summary>
        /// 
        /// </summary>
        public IdentityUserCommandHandlerTests()
        {
            CommandHandler = new IdentityUserCommandHandler();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleCreateIdentityUser()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new CreateIdentityUserCommand
                    {
                        DisplayName = "Asdf",
                        FullName = "Asdf Qwerty",
                        Email = "asdf@asdf.com",
                        PasswordHash = "hash",
                        PermissionNames = new[] {"permission"},
                        CreatedAt = now
                    }),
                Then(
                    new IdentityUserCreatedEvent
                    {
                        DisplayName = "Asdf",
                        FullName = "Asdf Qwerty",
                        Email = "asdf@asdf.com",
                        PasswordHash = "hash",
                        PermissionNames = new[] { "permission" },
                        CreatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleCreateIdentityUserExternalAccount()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new CreateIdentityUserExternalAccountCommand
                    {
                        DisplayName = "Asdf",
                        FullName = "Asdf Qwerty",
                        Email = "asdf@asdf.com",
                        LoginProvider = "provider",
                        LoginProviderKey = "key",
                        PermissionNames = new[] { "permission" },
                        CreatedAt = now
                    }),
                Then(
                    new IdentityUserExternalAccountCreatedEvent
                    {
                        DisplayName = "Asdf",
                        FullName = "Asdf Qwerty",
                        Email = "asdf@asdf.com",
                        LoginProvider = "provider",
                        LoginProviderKey = "key",
                        PermissionNames = new[] { "permission" },
                        CreatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleLoginIdentityUser()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new IdentityUserCreatedEvent
                    {
                        DisplayName = "Asdf",
                        FullName = "Asdf Qwerty",
                        Email = "asdf@asdf.com",
                        PasswordHash = "hash",
                        PermissionNames = new[] { "permission" },
                        CreatedAt = now
                        }),
                When(
                    new LoginIdentityUserCommand
                    {
                        At = now
                    }),
                Then(
                    new IdentityUserLoginEvent
                    {
                        At = now
                    }));
        }

    }

}

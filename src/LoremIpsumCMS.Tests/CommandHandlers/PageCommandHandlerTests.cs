﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.CommandHandlers;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Exceptions;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageCommandHandlerTests : AbstractCqrsTests<Page>
    {

        /// <summary>
        /// 
        /// </summary>
        public PageCommandHandlerTests()
        {
            CommandHandler = new PageCommandHandler(DocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleCreatePage()
        {
            var now = DateTime.Now;

            var siteOverview = new SiteOverview("Sites/1-A", "name", new[] {new SiteHost("localhost", true)});
            DocumentSession.Store(siteOverview);
            DocumentSession.SaveChanges();

            Test(
                Given(),
                When(
                    new CreatePageCommand
                    {
                        SiteId = siteOverview.SiteId,
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }),
                Then(
                    new PageCreatedEvent
                    {
                        SiteId = siteOverview.SiteId,
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleCreatePageInvalidSiteId()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new CreatePageCommand
                    {
                        SiteId = "Sites/1-A",
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }),
                ThenFailWithException<SiteNotFoundException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleCreatePageDuplicatePath()
        {
            var now = DateTime.Now;

            var siteOverview = new SiteOverview("Sites/1-A", "name", new[] { new SiteHost("localhost", true) });
            DocumentSession.Store(siteOverview);
            var pageOverview = new PageOverview("pageid", "sites/1-A", "/some-path", "title", "datatypename", now);
            DocumentSession.Store(pageOverview);
            DocumentSession.SaveChanges();

            Test(
                Given(),
                When(
                    new CreatePageCommand
                    {
                        SiteId = "Sites/1-A",
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }),
                ThenFailWithException<PageExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleUpdatePage()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new PageCreatedEvent
                    {
                        SiteId = "sites/1-A",
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new UpdatePageCommand
                    {
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        UpdatedAt = now
                        
                    }),
                Then(
                    new PageUpdatedEvent
                    {
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        UpdatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleUpdatePageSamePath()
        {
            var now = DateTime.Now;

            var pageOverview = new PageOverview("pageid", "sites/1-A", "/some-other-path", "title", "datatypename", now);
            DocumentSession.Store(pageOverview);
            DocumentSession.SaveChanges();

            Test(
                Given(
                    new PageCreatedEvent
                    {
                        SiteId = "sites/1-A",
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new UpdatePageCommand
                    {
                        Path = "/some-other-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        UpdatedAt = now

                    }),
                ThenFailWithException<PageExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleUpdatePageNotFound()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new UpdatePageCommand
                    {
                        Id = "pages/1-A",
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        UpdatedAt = now
                    }),
                ThenFailWithException<PageNotFoundException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleDeletePage()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new PageCreatedEvent
                    {
                        SiteId = "sites/1-A",
                        Path = "/some-path",
                        Data = null,
                        ExtensionProperties = new AbstractPageExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new DeletePageCommand()),
                Then(
                    new PageDeletedEvent()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleDeletePageNotFound()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new DeletePageCommand
                    {
                        Id = "pages/1-A",
                    }),
                ThenFailWithException<PageNotFoundException>());
        }


    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.CommandHandlers;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialCommandHandlerTests : AbstractCqrsTests<Partial>
    {

        /// <summary>
        /// 
        /// </summary>
        public PartialCommandHandlerTests()
        {
            CommandHandler = new PartialCommandHandler();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleCreatePartial()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new CreatePartialCommand
                    {
                        SiteId = "sites/1-A",
                        Name = "some-partial",
                        Data = null,
                        ExtensionProperties = new AbstractPartialExtensionProperties[0],
                        CreatedAt = now
                    }),
                Then(
                    new PartialCreatedEvent
                    {
                        SiteId = "sites/1-A",
                        Name = "some-partial",
                        Data = null,
                        ExtensionProperties = new AbstractPartialExtensionProperties[0],
                        CreatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleUpdatePartial()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new PartialCreatedEvent
                    {
                        SiteId = "sites/1-A",
                        Name = "some-partial",
                        Data = null,
                        ExtensionProperties = new AbstractPartialExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new UpdatePartialCommand
                    {
                        Name = "some-partial",
                        Data = null,
                        ExtensionProperties = new AbstractPartialExtensionProperties[0],
                        UpdatedAt = now
                        
                    }),
                Then(
                    new PartialUpdatedEvent
                    {
                        Name = "some-partial",
                        Data = null,
                        ExtensionProperties = new AbstractPartialExtensionProperties[0],
                        UpdatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleDeletePartial()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new PartialCreatedEvent
                    {
                        SiteId = "sites/1-A",
                        Name = "some-partial",
                        Data = null,
                        ExtensionProperties = new AbstractPartialExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new DeletePartialCommand()),
                Then(
                    new PartialDeletedEvent()));
        }
        
    }

}

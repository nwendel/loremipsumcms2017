﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.CommandHandlers;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Exceptions;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteCommandHandlerTests : AbstractCqrsTests<Site>
    {

        /// <summary>
        /// 
        /// </summary>
        public SiteCommandHandlerTests()
        {
            CommandHandler = new SiteCommandHandler(DocumentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleCreateSite()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new CreateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new []{ new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now,
                        CreatedByUserId = "Users/1-A"
                    }),
                Then(
                    new SiteCreatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleCreateSiteExists()
        {
            var now = DateTime.Now;

            var siteOverview = new SiteOverview("siteId", "some site", new[] { new SiteHost("localhost", true) });
            DocumentSession.Store(siteOverview);
            DocumentSession.SaveChanges();

            Test(
                Given(),
                When(
                    new CreateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new[] {new SiteHost("localhost", true)},
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now,
                        CreatedByUserId = "Users/1-A"
                    }),
                ThenFailWithException<SiteExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleCreateHostExists()
        {
            var now = DateTime.Now;

            var host = new Host("localhost", "siteid", true);
            DocumentSession.Store(host);
            DocumentSession.SaveChanges();

            Test(
                Given(),
                When(
                    new CreateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now,
                        CreatedByUserId = "Users/1-A"
                    }),
                ThenFailWithException<HostExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleUpdateSite()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new SiteCreatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new UpdateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        UpdatedAt = now,
                        UpdatedByUserId = "Users/1-A"
                    }),
                Then(
                    new SiteUpdatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        UpdatedAt = now
                    }));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleUpdateSiteNoSite()
        {
            var now = DateTime.Now;

            Test(
                Given(),
                When(
                    new UpdateSiteCommand
                    {
                        Id = "Sites/1-A",
                        Name = "some site",
                        Hosts = new[] {new SiteHost("localhost", true)},
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        UpdatedAt = now,
                        UpdatedByUserId = "Users/1-A"
                    }),
                ThenFailWithException<SiteNotFoundException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleUpdateDuplicateNameSlug()
        {
            var now = DateTime.Now;

            var siteOverview = new SiteOverview("siteId", "some site", new[] { new SiteHost("localhost", true) });
            DocumentSession.Store(siteOverview);
            DocumentSession.SaveChanges();

            Test(
                Given(
                    new SiteCreatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new UpdateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        UpdatedAt = now,
                        UpdatedByUserId = "Users/1-A"
                    }),
                ThenFailWithException<SiteExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleUpdateDuplicateHostname()
        {
            var now = DateTime.Now;

            var host = new Host("localhost", "siteid", true);
            DocumentSession.Store(host);
            DocumentSession.SaveChanges();

            Test(
                Given(
                    new SiteCreatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(
                    new UpdateSiteCommand
                    {
                        Name = "some site",
                        Hosts = new[] { new SiteHost("localhost", true) },
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        UpdatedAt = now,
                        UpdatedByUserId = "Users/1-A"
                    }),
                ThenFailWithException<HostExistsException>());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleDeleteSite()
        {
            var now = DateTime.Now;

            Test(
                Given(
                    new SiteCreatedEvent
                    {
                        Name = "some site",
                        Hosts = new[] {new SiteHost("localhost", true)},
                        ExtensionProperties = new AbstractSiteExtensionProperties[0],
                        CreatedAt = now
                    }),
                When(new DeleteSiteCommand()),
                Then(new SiteDeletedEvent()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnHandleDeleteSiteNoSite()
        {
            Test(
                Given(),
                When(new DeleteSiteCommand
                {
                    Id = "Sites/1-A"
                }),
                ThenFailWithException<SiteNotFoundException>());
        }
        
    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class AssetEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private IDocumentSession _documentSession;
        private readonly AssetEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public AssetEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new AssetEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleAssetDeleted()
        {
            var asset = new Asset();
            asset.Apply(new AssetCreatedEvent
            {
                SiteId = "Sites/1-A",
                Path = "/asset",
                CreatedAt = DateTime.Now
            });
            _documentSession.Store(asset);
            _documentSession.SaveChanges();

            _tested.Handle(new AssetDeletedEvent
            {
                Id = asset.Id
            });

            var loaded = _documentSession.Load<Asset>(asset.Id);
            Assert.Null(loaded);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteDeleted()
        {
            CreateIndex<AssetIndex>(_documentStore);
            CreateIndex<SiteAwareUniqueConstrantIndex>(_documentStore);

            // TODO: I can I do this on the store instead of the session?
            _documentSession.Advanced.WaitForIndexesAfterSaveChanges();

            var asset = new Asset();
            asset.Apply(new AssetCreatedEvent
            {
                SiteId = "Sites/1-A",
                Path = "/asset",
                CreatedAt = DateTime.Now
            });
            _documentSession.Store(asset);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteDeletedEvent
            {
                Id = asset.SiteId
            });

            _documentSession.Dispose();
            _documentSession = _documentStore.OpenSession();

            var loaded = _documentSession.Load<Asset>(asset.Id);
            Assert.Null(loaded);
        }

    }

}

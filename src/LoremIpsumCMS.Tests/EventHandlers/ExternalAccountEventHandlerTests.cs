﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class ExternalAccountEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private readonly IDocumentSession _documentSession;
        private readonly ExternalAccountEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public ExternalAccountEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new ExternalAccountEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleIdentityUserExternalAccountCreated()
        {
            _tested.Handle(new IdentityUserExternalAccountCreatedEvent
            {
                LoginProvider = "loginprovider",
                LoginProviderKey = "loginproviderkey",
                Id = "IdentityUsers/1-A"
            });
            _documentSession.SaveChanges();

            var loaded = _documentSession.LoadByUniqueConstraint<ExternalAccount>(x => x.LoginProviderAndKey, "loginprovider/loginproviderkey");
            Assert.NotNull(loaded);
            Assert.Equal("loginprovider", loaded.LoginProvider);
            Assert.Equal("loginproviderkey", loaded.LoginProviderKey);
            Assert.Equal("IdentityUsers/1-A", loaded.UserId);
        }

    }

}

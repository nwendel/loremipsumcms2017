﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class HostEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private readonly IDocumentSession _documentSession;
        private readonly HostEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public HostEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new HostEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleCreateSite()
        {
            _tested.Handle(new SiteCreatedEvent
            {
                Id = "sites/1-A",
                Hosts = new[] {new SiteHost("localhost", true)}
            });
            _documentSession.SaveChanges();

            var loaded = _documentSession.LoadBy<Host>(x => x.Name("localhost"));
            Assert.NotNull(loaded);
            Assert.Equal("sites/1-A", loaded.SiteId);
            Assert.Equal("localhost", loaded.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleUpdateSite()
        {
            CreateIndex<HostIndex>(_documentStore);
            CreateIndex<SiteAwareUniqueConstrantIndex>(_documentStore);

            // TODO: I can I do this on the store instead of the session?
            _documentSession.Advanced.WaitForIndexesAfterSaveChanges();

            var host = new Host("another-host", "sites/1-A", true);
            _documentSession.Store(host);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteUpdatedEvent
            {
                Id = "sites/1-A",
                Hosts = new[] { new SiteHost("localhost", true) }
            });
            _documentSession.SaveChanges();

            var deleted = _documentSession.LoadBy<Host>(x => x.Name("another-host"));
            Assert.Null(deleted);

            var loaded = _documentSession.LoadBy<Host>(x => x.Name("localhost"));
            Assert.NotNull(loaded);
            Assert.Equal("sites/1-A", loaded.SiteId);
            Assert.Equal("localhost", loaded.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleDeleteSite()
        {
            CreateIndex<HostIndex>(_documentStore);
            CreateIndex<SiteAwareUniqueConstrantIndex>(_documentStore);

            // TODO: I can I do this on the store instead of the session?
            _documentSession.Advanced.WaitForIndexesAfterSaveChanges();

            var host = new Host("localhost", "sites/1-A", true);
            _documentSession.Store(host);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteDeletedEvent
            {
                Id = "sites/1-A"
            });
            _documentSession.SaveChanges();

            var deleted = _documentSession.LoadBy<Host>(x => x.Name("localhost"));
            Assert.Null(deleted);
        }
        
    }

}

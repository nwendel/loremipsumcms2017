﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private IDocumentSession _documentSession;
        private readonly PageEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public PageEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new PageEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePageDeleted()
        {
            var page = new Page();
            page.Apply(new PageCreatedEvent
            {
                SiteId = "Sites/1-A",
                Path = "/page",
                CreatedAt = DateTime.Now
            });
            _documentSession.Store(page);
            _documentSession.SaveChanges();

            _tested.Handle(new PageDeletedEvent
            {
                Id = page.Id
            });

            var loaded = _documentSession.Load<Asset>(page.Id);
            Assert.Null(loaded);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteDeleted()
        {
            CreateIndex<PageIndex>(_documentStore);
            CreateIndex<SiteAwareUniqueConstrantIndex>(_documentStore);

            // TODO: I can I do this on the store instead of the session?
            _documentSession.Advanced.WaitForIndexesAfterSaveChanges();

            var page = new Page();
            page.Apply(new PageCreatedEvent
            {
                SiteId = "Sites/1-A",
                Path = "/page",
                CreatedAt = DateTime.Now
            });
            _documentSession.Store(page);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteDeletedEvent
            {
                Id = page.SiteId
            });

            _documentSession.Dispose();
            _documentSession = _documentStore.OpenSession();

            var loaded = _documentSession.Load<Page>(page.Id);
            Assert.Null(loaded);
        }

    }

}

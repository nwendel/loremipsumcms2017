﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Tests.EventHandlers.Classes;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialOverviewEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private readonly IDocumentSession _documentSession;
        private readonly PartialOverviewEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public PartialOverviewEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new PartialOverviewEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePartialCreated()
        {
            _tested.Handle(new PartialCreatedEvent
            {
                Id = "partials/1-A",
                SiteId = "sites/1-A",
                Name = "partial",
                Data = new SomePartialData(),
                CreatedAt = DateTime.Now
            });
            _documentSession.SaveChanges();

            var loaded = _documentSession.LoadBy<PartialOverview>(x => x.PartialId("partials/1-A"));
            Assert.NotNull(loaded);
            Assert.Equal("sites/1-A", loaded.SiteId);
            Assert.Equal("partial", loaded.Name);
            Assert.Equal("partials/1-A", loaded.PartialId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePartialUpdated()
        {
            var partialOverview = new PartialOverview("partials/1-A", "sites/1-A", "partial", "data-type-name", DateTime.Now);
            _documentSession.Store(partialOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new PartialUpdatedEvent
            {
                Id = "partials/1-A",
                Name = "new name",
                Data = new SomePartialData(),
                ExtensionProperties = new AbstractPartialExtensionProperties[0],
                UpdatedAt = DateTime.Now
            });
            _documentSession.SaveChanges();

            var notFound = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug("sites/1-A", "partial"));
            Assert.Null(notFound);
            var loaded = _documentSession.Load<PartialOverview>(partialOverview.Id);
            Assert.NotNull(loaded);
            Assert.Equal("new name", loaded.Name);
            Assert.Equal("sites/1-A", loaded.SiteId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePartialDeleted()
        {
            var partialOverview = new PartialOverview("partials/1-A", "sites/1-A", "partial", "data-type-name", DateTime.Now);
            _documentSession.Store(partialOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new PartialDeletedEvent
            {
                Id = "partials/1-A",
            });
            _documentSession.SaveChanges();

            var notFound = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug("sites/1-A", "partial"));
            Assert.Null(notFound);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteDeleted()
        {
            CreateIndex<PartialOverviewIndex>(_documentStore);
            CreateIndex<SiteAwareUniqueConstrantIndex>(_documentStore);

            // TODO: I can I do this on the store instead of the session?
            _documentSession.Advanced.WaitForIndexesAfterSaveChanges();

            var partialOverview = new PartialOverview("partials/1-A", "sites/1-A", "partial", "data-type-name", DateTime.Now);
            _documentSession.Store(partialOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteDeletedEvent
            {
                Id = "sites/1-A",
            });
            _documentSession.SaveChanges();

            var notFound = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug("sites/1-A", "partial"));
            Assert.Null(notFound);
        }
        
    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Tests.EventHandlers.Classes;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageOverviewEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private readonly IDocumentSession _documentSession;
        private readonly PageOverviewEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public PageOverviewEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new PageOverviewEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePageCreated()
        {
            _tested.Handle(new PageCreatedEvent
            {
                Id = "pages/1-A",
                SiteId = "sites/1-A",
                Path = "/path",
                Data = new SomePageData(),
                CreatedAt = DateTime.Now
            });
            _documentSession.SaveChanges();

            var loaded = _documentSession.LoadBy<PageOverview>(x => x.PageId("pages/1-A"));
            Assert.NotNull(loaded);
            Assert.Equal("sites/1-A", loaded.SiteId);
            Assert.Equal("/path", loaded.Path);
            Assert.Equal("pages/1-A", loaded.PageId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePageUpdated()
        {
            var pageOverview = new PageOverview("pages/1-A", "sites/1-A", "/page", "title", "data-type-name", DateTime.Now);
            _documentSession.Store(pageOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new PageUpdatedEvent
            {
                Id = "pages/1-A",
                Path = "/new-path",
                Data = new SomePageData(),
                ExtensionProperties = new AbstractPageExtensionProperties[0],
                UpdatedAt = DateTime.Now
            });
            _documentSession.SaveChanges();

            var notFound = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath("sites/1-A", "/page"));
            Assert.Null(notFound);
            var loaded = _documentSession.Load<PageOverview>(pageOverview.Id);
            Assert.NotNull(loaded);
            Assert.Equal("/new-path", loaded.Path);
            Assert.Equal("sites/1-A", loaded.SiteId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandlePageDeleted()
        {
            var pageOverview = new PageOverview("pages/1-A", "sites/1-A", "/page", "title", "data-type-name", DateTime.Now);
            _documentSession.Store(pageOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new PageDeletedEvent
            {
                Id = "pages/1-A",
            });
            _documentSession.SaveChanges();

            var notFound = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath("sites/1-A", "/page"));
            Assert.Null(notFound);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteDeleted()
        {
            CreateIndex<PageOverviewIndex>(_documentStore);
            CreateIndex<SiteAwareUniqueConstrantIndex>(_documentStore);

            // TODO: I can I do this on the store instead of the session?
            _documentSession.Advanced.WaitForIndexesAfterSaveChanges();

            var pageOverview = new PageOverview("pages/1-A", "sites/1-A", "/page", "title", "data-type-name", DateTime.Now);
            _documentSession.Store(pageOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteDeletedEvent
            {
                Id = "sites/1-A",
            });
            _documentSession.SaveChanges();

            var notFound = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath("sites/1-A", "/page"));
            Assert.Null(notFound);
        }
        
    }

}

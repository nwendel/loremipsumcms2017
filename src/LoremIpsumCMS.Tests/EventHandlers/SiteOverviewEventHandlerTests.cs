﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.EventHandlers;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteOverviewEventHandlerTests : AbstractRavenTests
    {

        private readonly IDocumentStore _documentStore;
        private readonly IDocumentSession _documentSession;
        private readonly SiteOverviewEventHandler _tested;

        /// <summary>
        /// 
        /// </summary>
        public SiteOverviewEventHandlerTests()
        {
            _documentStore = GetDocumentStore();
            _documentSession = _documentStore.OpenSession();
            _tested = new SiteOverviewEventHandler(_documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteCreated()
        {
            _tested.Handle(new SiteCreatedEvent
            {
                Id = "Sites/1-A",
                Name = "Some Site",
                ExtensionProperties = new AbstractSiteExtensionProperties[0],
                Hosts = new [] { new SiteHost("localhost", true)},
                CreatedAt = DateTime.Now
            });
            _documentSession.SaveChanges();

            var loaded = _documentSession.LoadBy<SiteOverview>(x => x.SiteId("Sites/1-A"));
            Assert.NotNull(loaded);
            Assert.Equal("Some Site", loaded.Name);
            Assert.Equal("Sites/1-A", loaded.SiteId);
            Assert.NotNull(loaded.Hosts);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteUpdated()
        {
            var siteOverview = new SiteOverview("Sites/1-A", "Some Site", new[] { new SiteHost("localhost", true) });
            _documentSession.Store(siteOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteUpdatedEvent
            {
                Id = "Sites/1-A",
                Name = "New Name",
                ExtensionProperties = new AbstractSiteExtensionProperties[0],
                Hosts = new[] { new SiteHost("localhost", true) },
                UpdatedAt = DateTime.Now
            });
            _documentSession.SaveChanges();

            var loaded = _documentSession.Load<SiteOverview>(siteOverview.Id);
            Assert.NotNull(loaded);
            Assert.Equal("New Name", loaded.Name);
            Assert.Equal("Sites/1-A", loaded.SiteId);
            Assert.NotNull(loaded.Hosts);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanHandleSiteDeleted()
        {
            var siteOverview = new SiteOverview("Sites/1-A", "Some Site", new[] { new SiteHost("localhost", true) });
            _documentSession.Store(siteOverview);
            _documentSession.SaveChanges();

            _tested.Handle(new SiteDeletedEvent
            {
                Id = siteOverview.SiteId
            });

            var loaded = _documentSession.Load<SiteOverview>(siteOverview.Id);
            Assert.Null(loaded);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using Xunit;
using LoremIpsumCMS.Infrastructure.Collections.Generic;

namespace LoremIpsumCMS.Tests.Infrastructure.Collections.Generic
{

    /// <summary>
    /// 
    /// </summary>
    public class CollectionRemoveRangeExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRemoveRange()
        {
            var tested = new List<string>() { "zxcv", "asdf", "qwerty" };

            tested.RemoveRange(new[] { "asdf", "qwerty" });

            Assert.Single(tested);
            Assert.Contains("zxcv", tested);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullRemoveRange()
        {
            var tested = new List<string>() { "zxcv" };

            Assert.Throws<ArgumentNullException>(() => tested.RemoveRange(null));
        }

    }

}

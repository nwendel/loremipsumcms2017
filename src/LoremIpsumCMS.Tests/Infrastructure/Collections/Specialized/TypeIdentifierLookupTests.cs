﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using Xunit;
using LoremIpsumCMS.Infrastructure.Collections.Specialized;

namespace LoremIpsumCMS.Tests.Infrastructure.Collections.Specialized
{

    /// <summary>
    /// 
    /// </summary>
    public class TypeIdentifierLookupTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRegister()
        {
            var tested = new TypeIdentifierLookup();
            tested.Add(typeof(TypeIdentifierLookupTests));

            var identifier = tested[typeof(TypeIdentifierLookupTests)];
            var type = tested[identifier];

            Assert.NotNull(identifier);
            Assert.Same(typeof(TypeIdentifierLookupTests), type);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnRegisterNullType()
        {
            var tested = new TypeIdentifierLookup();
            Assert.Throws<ArgumentNullException>(() => tested.Add(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnRegisterAbstractType()
        {
            var tested = new TypeIdentifierLookup();
            Assert.Throws<ArgumentException>(() => tested.Add(typeof(AbstractClass)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnRegisterOpenGenericType()
        {
            var tested = new TypeIdentifierLookup();
            Assert.Throws<ArgumentException>(() => tested.Add(typeof(List<>)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnUnregisteredIdentifier()
        {
            var tested = new TypeIdentifierLookup();

            Assert.Throws<KeyNotFoundException>(() => tested["asdf"]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullIdentifier()
        {
            var tested = new TypeIdentifierLookup();

            Assert.Throws<ArgumentNullException>(() => tested[(string)null]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnUnregisteredType()
        {
            var tested = new TypeIdentifierLookup();

            Assert.Throws<KeyNotFoundException>(() => tested[typeof(object)]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullType()
        {
            var tested = new TypeIdentifierLookup();

            Assert.Throws<ArgumentNullException>(() => tested[(Type)null]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnAddTypeTwice()
        {
            var tested = new TypeIdentifierLookup();

            tested.Add(typeof(object));

            Assert.Throws<ArgumentException>(() => tested.Add(typeof(object)));
        }

        /// <summary>
        /// 
        /// </summary>
        private abstract class AbstractClass
        {
        }

    }

}

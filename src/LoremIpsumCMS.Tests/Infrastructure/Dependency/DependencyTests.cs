﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Xunit;
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Tests.Infrastructure.Dependency.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Dependency
{

    /// <summary>
    /// 
    /// </summary>
    public class DependencyTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNonExistantDependency()
        {
            var instances = new[] { new One() };

            var ex = Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
            var expectedMessage = $"{typeof(One).FullName} depends on {typeof(Two).FullName} which cannot be found";
            Assert.Equal(expectedMessage, ex.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderInDependencyOrderCorrectOrder()
        {
            var one = new One();
            var two = new Two();

            var instances = new object[] { one, two };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Same(two, orderedInstances[0]);
            Assert.Same(one, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderInDependencyOrderWrongOrder()
        {
            var one = new One();
            var two = new Two();

            var instances = new object[] { two, one };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Same(two, orderedInstances[0]);
            Assert.Same(one, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanIgnoreOptionalDependency()
        {
            var three = new Three();

            var instances = new object[] { three };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Single(orderedInstances);
            Assert.Same(three, orderedInstances[0]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderOptionalInDependencyOrderCorrectOrder()
        {
            var three = new Three();
            var two = new Two();

            var instances = new object[] { three, two };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Same(two, orderedInstances[0]);
            Assert.Same(three, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanOrderOptionalInDependencyOrderWrongOrder()
        {
            var three = new Three();
            var two = new Two();

            var instances = new object[] { two, three };
            var orderedInstances = instances.OrderByDependencies().ToList();

            Assert.NotNull(orderedInstances);
            Assert.Equal(2, orderedInstances.Count());
            Assert.Same(two, orderedInstances[0]);
            Assert.Same(three, orderedInstances[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCircularDependencyOnSelf()
        {
            var instances = new[] { new Circular() };

            var ex = Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
            var expectedMessage = $"Found dependency circle: {typeof(Circular).FullName} -> {typeof(Circular).FullName}";
            Assert.Equal(expectedMessage, ex.Message);

        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCircularDependencyThree()
        {
            var instances = new object[] { new CircleOne(), new CircleTwo(), new CircleThree() };

            var ex = Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
            var expectedMessage = $"Found dependency circle: {typeof(CircleOne).FullName} -> {typeof(CircleTwo).FullName} -> {typeof(CircleThree).FullName} -> {typeof(CircleOne).FullName}";
            Assert.Equal(expectedMessage, ex.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCircularDependencyFour()
        {
            var instances = new object[] { new CircleFour(), new Circular() };

            var ex = Assert.Throws<DependencyException>(() => instances.OrderByDependencies());
            var expectedMessage = $"Found dependency circle: {typeof(Circular).FullName} -> {typeof(Circular).FullName}";
            Assert.Equal(expectedMessage, ex.Message);
        }

    }

}

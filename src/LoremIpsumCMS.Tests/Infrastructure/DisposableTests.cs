﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Infrastructure;

namespace LoremIpsumCMS.Tests.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public class DisposableTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDispose()
        {
            var state = new DisposableState();
            var tested = new TestDisposable(state);
            tested.Dispose();

            Assert.Equal(1, state.ManagedDisposeCount);
            Assert.Equal(1, state.UnmanagedDisposeCount);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDisposeTwice()
        {
            var state = new DisposableState();
            var tested = new TestDisposable(state);
            tested.Dispose();
            tested.Dispose();

            Assert.Equal(1, state.ManagedDisposeCount);
            Assert.Equal(1, state.UnmanagedDisposeCount);
        }

#if false
        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDestructorDispose()
        {
            var state = new DisposableState();
            var tested = new TestDisposable(state);
            tested = null;

            GC.Collect();
            GC.WaitForPendingFinalizers();

            Assert.Equal(0, state.ManagedDisposeCount);
            Assert.Equal(1, state.UnmanagedDisposeCount);
        }
#endif

        #region Disposable

        /// <summary>
        /// 
        /// </summary>
        public class DisposableState
        {

            /// <summary>
            /// 
            /// </summary>
            public int ManagedDisposeCount { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public int UnmanagedDisposeCount { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class TestDisposable : AbstractDisposable
        {

            private readonly DisposableState _state;

            /// <summary>
            /// 
            /// </summary>
            public TestDisposable(DisposableState state)
            {
                _state = state;
            }

            /// <summary>
            /// 
            /// </summary>
            protected override void ManagedDispose()
            {
                base.ManagedDispose();
                _state.ManagedDisposeCount += 1;
            }

            /// <summary>
            /// 
            /// </summary>
            protected override void UnmanagedDispose()
            {
                base.UnmanagedDispose();
                _state.UnmanagedDisposeCount += 1;
            }

        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Cqrs;
using Moq;
using Raven.Client.Documents.Session;
using Xunit;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Tests.Infrastructure.EventStore.Classes;


namespace LoremIpsumCMS.Tests.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class EventStoreArgumentTests
    {

        [Fact]
        public void ThrowsOnNullAggregateType()
        {
            var tested = new RavenEventStore(null);

            var ex = Assert.Throws<ArgumentNullException>(() => tested.SaveEvents(null, new AbstractEvent[0]));
            Assert.Equal("aggregateType", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnNullEvents()
        {
            var tested = new RavenEventStore(null);

            var ex = Assert.Throws<ArgumentNullException>(() => tested.SaveEvents(typeof(object), null));
            Assert.Equal("events", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullEventId()
        {
            var tested = new RavenEventStore(null);
            var events = new[] { new SomeEvent() };

            var ex = Assert.Throws<InvalidOperationException>(() => tested.SaveEvents(typeof(object), events));
            Assert.Equal("Cannot save event without AggregateId", ex.Message);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateInconsistantEventIds()
        {
            var documentSessionMock = new Mock<IDocumentSession>();
            documentSessionMock.Setup(x => x.Store(It.IsAny<PersistedEvent>()));

            var tested = new RavenEventStore(documentSessionMock.Object);
            var events = new[] { new SomeEvent { Id = "one" }, new SomeEvent { Id = "two" } };

            var ex = Assert.Throws<InvalidOperationException>(() => tested.SaveEvents(typeof(object), events));
            Assert.Equal("Cannot save events with different AggregateIds", ex.Message);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Tests.Infrastructure.EventStore.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class PersistedEventTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var @event = new SomeEvent { Id = "asdf" };
            var tested = new PersistedEvent(typeof(object).Name, @event);

            Assert.Equal("asdf", tested.AggregateId);
            Assert.NotEqual(default(DateTime), tested.At);
            Assert.Equal(typeof(SomeEvent).Name, tested.EventType);
            Assert.Equal(typeof(object).Name, tested.AggregateType);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullAggregateType()
        {
            var @event = new SomeEvent { Id = "asdf" };
            var ex = Assert.Throws<ArgumentNullException>(() => new PersistedEvent(null, @event));
            Assert.Equal("aggregateType", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullEvent()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => new PersistedEvent(typeof(object).Name, null));
            Assert.Equal("event", ex.ParamName);
        }

    }

}

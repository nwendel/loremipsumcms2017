﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using LoremIpsumCMS.Infrastructure.Linq;

namespace LoremIpsumCMS.Tests.Infrastructure.Extensions
{

    /// <summary>
    /// 
    /// </summary>
    public class EnumerableNoneExtensionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNoneTrue()
        {
            var tested = new string[0];

            Assert.True(tested.None());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNoneFalse()
        {
            var tested = new[] { "asdf" };

            Assert.False(tested.None());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNoneWithPredicateTrue()
        {
            var tested = new[] { "asdf" };

            Assert.True(tested.None(x => x != "asdf"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNoneWithPredicateFalse()
        {
            var tested = new[] { "asdf" };

            Assert.False(tested.None(x => x == "asdf"));
        }

    }

}

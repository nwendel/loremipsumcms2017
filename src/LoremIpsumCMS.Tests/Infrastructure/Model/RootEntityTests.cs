﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using LoremIpsumCMS.Tests.Infrastructure.Model.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class RootEntityTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsSameTransientObject()
        {
            var tested = new TestAggregate();

            var isEqual = tested.Equals((object)tested);

            Assert.True(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsDifferentTransientObject()
        {
            var tested = new TestAggregate();
            var other = new TestAggregate();

            var isEqual = tested.Equals(other);

            Assert.False(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsDifferentObjectSameId()
        {
            var tested = new TestAggregate { PublicId = "asdf" };
            var other = new TestAggregate { PublicId = "asdf" };

            var isEqual = tested.Equals(other);

            Assert.True(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsNull()
        {
            var tested = new TestAggregate();

            var isEqual = tested.Equals(null);

            Assert.False(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanEqualsWrongOtherType()
        {
            var tested = new TestAggregate();
            var other = new AnotherTestAggregate();

            var isEqual = tested.Equals(other);

            Assert.False(isEqual);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetTransientHashCode()
        {
            var tested = new TestAggregate();

            var hashCode = tested.GetHashCode();

            Assert.NotEqual(0, hashCode);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetNonTransientHashCode()
        {
            var tested = new TestAggregate { PublicId = "asdf" };

            var hashCode = tested.GetHashCode();

            Assert.NotEqual(0, hashCode);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Cqrs;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven.Classes
{

    /// <summary>
    /// 
    /// </summary>
    public class SomeCommandHandler : 
        IHandleCommand<SomeCreateCommand>,
        IHandleCommand<SomeUpdateCommand>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(SomeCreateCommand command)
        {
            yield return new SomeEvent
            {
                Name = command.Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(SomeUpdateCommand command)
        {
            yield return new SomeEvent
            {
                Id = command.Id,
                Name = command.Name
            };
        }
        
    }

}

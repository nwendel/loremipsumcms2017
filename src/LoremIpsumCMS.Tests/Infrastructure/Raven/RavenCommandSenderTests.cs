﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.EventStore;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Tests.Infrastructure.Raven.Classes;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenCommandSenderTests : AbstractRavenTests
    {

        private readonly RavenCommandSender _tested;

        private readonly Mock<ISupportRequiredService> _supportRequiredServiceMock;
        private readonly Mock<IValidationService> _validationServiceMock;

        private readonly IDocumentSession _documentSession;

        /// <summary>
        /// 
        /// </summary>
        public RavenCommandSenderTests()
        {
            var serviceProviderMock = new Mock<IServiceProvider>();
            _supportRequiredServiceMock = serviceProviderMock.As<ISupportRequiredService>();
            _validationServiceMock = new Mock<IValidationService>();
            var eventStoreMock = new Mock<IEventStore>();

            var store = GetDocumentStore();
            _documentSession = store.OpenSession();

            _tested = new RavenCommandSender(serviceProviderMock.Object, _validationServiceMock.Object, eventStoreMock.Object, _documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnSendNullCommand()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => _tested.Send((AbstractCommand<AbstractAggregate>)null));
            Assert.Equal("command", ex.ParamName); 
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnValidateNullCommand()
        {
            var ex = Assert.Throws<ArgumentNullException>(() => _tested.Validate((AbstractCommand<AbstractAggregate>)null));
            Assert.Equal("command", ex.ParamName);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSendCommand()
        {
            _supportRequiredServiceMock.Setup(x => x.GetRequiredService(typeof(IHandleCommand<SomeCreateCommand>))).Returns(new SomeCommandHandler());

            var command = new SomeCreateCommand {Name = "asdf"};
            var id = _tested.Send(command);

            var aggregate = _documentSession.Load<SomeAggregate>(id);
            Assert.Equal("asdf", aggregate.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSendTwoCommands()
        {
            _supportRequiredServiceMock.Setup(x => x.GetRequiredService(typeof(IHandleCommand<SomeCreateCommand>))).Returns(new SomeCommandHandler());
            _supportRequiredServiceMock.Setup(x => x.GetRequiredService(typeof(IHandleCommand<SomeUpdateCommand>))).Returns(new SomeCommandHandler());

            var createCommand = new SomeCreateCommand { Name = "asdf" };
            var id = _tested.Send(createCommand);

            var updateCommand = new SomeUpdateCommand {Id = id, Name = "qwerty"};
            var sameId = _tested.Send(updateCommand);

            var aggregate = _documentSession.Load<SomeAggregate>(sameId);
            Assert.Equal(id, sameId);
            Assert.Equal("qwerty", aggregate.Name);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnSendCommandWithIdNoAggregate()
        {
            _supportRequiredServiceMock.Setup(x => x.GetRequiredService(typeof(IHandleCommand<SomeUpdateCommand>))).Returns(new SomeCommandHandler());

            var command = new SomeUpdateCommand { Id = "TheId" };
            Assert.Throws<CqrsException>(() => _tested.Send(command));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPublishEventsCommand()
        {
            var eventHandler = new SomeEventHandler();

            _supportRequiredServiceMock.Setup(x => x.GetRequiredService(typeof(IHandleCommand<SomeCreateCommand>))).Returns(new SomeCommandHandler());
            _supportRequiredServiceMock.Setup(x => x.GetRequiredService(typeof(IEnumerable<ISubscribeTo<SomeEvent>>))).Returns(new[] {eventHandler});

            var command = new SomeCreateCommand { Name = "asdf" };
            _tested.Send(command);

            Assert.True(eventHandler.IsInvoked);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCanValidateInvalid()
        {
            var command = new SomeCreateCommand { Name = "asdf" };
            _validationServiceMock.Setup(x => x.Validate(command)).Returns(new[] {new ValidationMessage("asdf", "asdf message")});

            Assert.Throws<ValidationException>(() => _tested.Send(command));
        }
        
    }

}

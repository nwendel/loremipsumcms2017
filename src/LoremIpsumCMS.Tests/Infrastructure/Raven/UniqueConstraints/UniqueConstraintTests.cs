﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Tests.Infrastructure.Raven.UniqueConstraints.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateAndLoad()
        {
            var aggregate = new SomeAggregate {Name = "asdf"};

            using (var store = GetDocumentStore())
            {
                using (var session = store.OpenSession())
                {
                    session.Store(aggregate);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    var loaded = session.LoadByUniqueConstraint<SomeAggregate>(x => x.Name, "asdf");

                    Assert.NotNull(loaded);
                    Assert.Equal("asdf", loaded.Name);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateNullValue()
        {
           var aggregate = new SomeAggregate();

            using (var store = GetDocumentStore())
            {
                using (var session = store.OpenSession())
                {
                    session.Store(aggregate);
                    session.SaveChanges();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUpdate()
        {
            var aggregate = new SomeAggregate { Name = "asdf" };

            using (var store = GetDocumentStore())
            {
                using (var session = store.OpenSession())
                {
                    session.Store(aggregate);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    aggregate.Name = "qwerty";
                    session.Store(aggregate);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    var loadedNotFound = session.LoadByUniqueConstraint<SomeAggregate>(x => x.Name, "asdf");
                    var loaded = session.LoadByUniqueConstraint<SomeAggregate>(x => x.Name, "qwerty");

                    Assert.Null(loadedNotFound);
                    Assert.NotNull(loaded);
                    Assert.Equal("qwerty", loaded.Name);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUpdateToNull()
        {
            var aggregate = new SomeAggregate { Name = "asdf" };

            using (var store = GetDocumentStore())
            {
                using (var session = store.OpenSession())
                {
                    session.Store(aggregate);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    aggregate.Name = null;
                    session.Store(aggregate);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    var loadedNotFound = session.LoadByUniqueConstraint<SomeAggregate>(x => x.Name, "asdf");

                    Assert.Null(loadedNotFound);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanDelete()
        {
            var aggregate = new SomeAggregate { Name = "asdf" };

            using (var store = GetDocumentStore())
            {
                using (var session = store.OpenSession())
                {
                    session.Store(aggregate);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    var todelete = session.Load<SomeAggregate>(aggregate.Id);
                    session.Delete(todelete);
                    session.SaveChanges();
                }

                using (var session = store.OpenSession())
                {
                    var loadedNotFound = session.LoadByUniqueConstraint<SomeAggregate>(x => x.Name, "asdf");

                    Assert.Null(loadedNotFound);
                }
            }
        }
        
    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using Xunit;
using LoremIpsumCMS.Infrastructure.Reflection;
using System.Linq;

namespace LoremIpsumCMS.Tests.Infrastructure.Reflection
{

    /// <summary>
    /// 
    /// </summary>
    public class ReflectionUtilityTests
    {

#if false

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindApplicationAssemblies()
        {
            var assemblies = ReflectionUtility.FindApplicationAssemblies().ToList();
            var entryAssembly = Assembly.GetEntryAssembly();

            Assert.Contains(entryAssembly, assemblies);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindLoremIpsumAssemblies()
        {
            var assemblies = ReflectionUtility.FindLoremIpsumAssemblies();

            Assert.Contains(typeof(LoremIpsumException).GetTypeInfo().Assembly, assemblies);
            Assert.Contains(typeof(ReflectionUtilityTests).GetTypeInfo().Assembly, assemblies);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindApplicationAndLoremIpsumAssemblies()
        {
            var assemblies = ReflectionUtility.FindApplicationAndLoremIpsumAssemblies();
            var distinctAssemblyCount = assemblies.Distinct().Count();
            var entryAssembly = Assembly.GetEntryAssembly();

            Assert.Equal(distinctAssemblyCount, assemblies.Count());
            Assert.Contains(entryAssembly, assemblies);
            Assert.Contains(typeof(LoremIpsumException).GetTypeInfo().Assembly, assemblies);
            Assert.Contains(typeof(ReflectionUtilityTests).GetTypeInfo().Assembly, assemblies);
        }

#endif

    }

}

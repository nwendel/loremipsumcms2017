﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using LoremIpsumCMS.Infrastructure;

namespace LoremIpsumCMS.Tests.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public class StringSlugifyExtensionsTests
    {

        [Fact]
        public void CanRemoveSwedishUmlauts()
        {
            var value = "åäö".Slugify();
            Assert.Equal("aao", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanRemoveDiacritic()
        {
            var value = "é".Slugify();
            Assert.Equal("e", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSquachMultipleSpaces()
        {
            var value = "1   2".Slugify();
            Assert.Equal("1-2", value);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanSlugifyNull()
        {
            string tested = null;
            var value = tested.Slugify();
            Assert.Null(value);
        }

    }

}

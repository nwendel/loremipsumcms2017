﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation.Classes
{

    /// <summary>
    /// 
    /// </summary>
    public class Customer : IContactsAware
    {

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AbstractContact DefaultContact => Contacts.FirstOrDefault();

        /// <summary>
        /// 
        /// </summary>
        public AddressContact MainAddress => (AddressContact)Contacts.FirstOrDefault(x => x.GetType() == typeof(AddressContact));

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<AbstractContact> Contacts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Order> Orders { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Order[] OrdersArray => Orders?.ToArray();

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<int> OrderAmounts => Orders?.Select(x => x.Amount).ToList();

        /// <summary>
        /// 
        /// </summary>
        public int[] OrderAmountsArray => Orders?.Select(x => x.Amount).ToArray();

    }

}

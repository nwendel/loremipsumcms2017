﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class InstanceValidateUsingTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateUsingValidator()
        {
            var tested = new CustomerValidator();
            var customer = new Customer();
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<ContactsAwareValidator>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateUsingValidatorInvalid()
        {
            var tested = new CustomerValidator();
            var customer = new Customer { Contacts = new List<AbstractContact>() };
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<ContactsAwareValidator>();
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Contacts", x.PropertyName);
                Assert.Equal("Contacts must not be set", x.Text);
            });
        }

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        private class CustomerValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            public CustomerValidator()
            {
                ValidateUsing<ContactsAwareValidator>();
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private class ContactsAwareValidator : AbstractValidator<IContactsAware>
        {

            /// <summary>
            /// 
            /// </summary>
            public ContactsAwareValidator()
            {
                RuleFor(x => x.Contacts).Null();
            }

        }

        #endregion

    }

}

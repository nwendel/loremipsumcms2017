﻿//using System;
//using System.Linq;
//using FluentRegistration;
//using Microsoft.Extensions.DependencyInjection;
//using Xunit;
//using LoremIpsumCMS.Infrastructure.Validation;
//using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

//namespace LoremIpsumCMS.Tests.Infrastructure.Validation
//{

//    public class PolymorphicValidateTests
//    {

//        private readonly IServiceProvider _serviceProvider;

//        /// <summary>
//        /// 
//        /// </summary>
//        public PolymorphicValidateTests()
//        {
//            var serviceCollection = new ServiceCollection();
//            serviceCollection.Register(r => r
//                .FromTypes(new[] { typeof(PhoneContactValidator), typeof(AddressContactValidator) })
//                .WithService.AllInterfaces());
//            _serviceProvider = serviceCollection.BuildServiceProvider();
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        [Fact]
//        public void CanValidatePolymorphic()
//        {
//            var tested = new CustomerDefaultContactValidator();
//            var customer = new Customer
//            {
//                Contacts = new AbstractContact[]
//                {
//                    new AddressContact()
//                }
//            };
//            var context = new ValidationContext<Customer>(customer, _serviceProvider);
//            var messages = tested.Validate(context).ToList();

//            Assert.Equal(2, messages.Count);
//            Assert.Contains(messages, x => x.PropertyName == "DefaultContact.Line1");
//            Assert.Contains(messages, x => x.Text == "Line1 must be set");
//            Assert.Contains(messages, x => x.PropertyName == "DefaultContact.Line2");
//            Assert.Contains(messages, x => x.Text == "Line2 must be set");
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        [Fact]
//        public void CanValidateListPolymorphic()
//        {
//            var tested = new CustomerContactsValidator();
//            var customer = new Customer
//            {
//                Contacts = new AbstractContact[]
//                {
//                    new AddressContact(),
//                    new PhoneContact()
//                }
//            };
//            var context = new ValidationContext<Customer>(customer, _serviceProvider);
//            var messages = tested.Validate(context).ToList();

//            Assert.Equal(3, messages.Count);
//            Assert.Contains(messages, x => x.PropertyName == "Contacts[0].Line1");
//            Assert.Contains(messages, x => x.Text == "Line1 must be set");
//            Assert.Contains(messages, x => x.PropertyName == "Contacts[0].Line2");
//            Assert.Contains(messages, x => x.Text == "Line2 must be set");
//            Assert.Contains(messages, x => x.PropertyName == "Contacts[1].Number");
//            Assert.Contains(messages, x => x.Text == "Number must be set");
//        }


//        #region Validators

//        /// <summary>
//        /// 
//        /// </summary>
//        private class CustomerDefaultContactValidator : AbstractValidator<Customer>
//        {

//            /// <summary>
//            /// 
//            /// </summary>
//            public CustomerDefaultContactValidator()
//            {
//                RuleFor(x => x.DefaultContact).PolymorphicValidate();
//            }

//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        private class CustomerContactsValidator : AbstractValidator<Customer>
//        {

//            /// <summary>
//            /// 
//            /// </summary>
//            public CustomerContactsValidator()
//            {
//                RuleFor(x => x.Contacts).All(x => x.PolymorphicValidate());
//            }

//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        private class PhoneContactValidator : AbstractValidator<PhoneContact>
//        {

//            /// <summary>
//            /// 
//            /// </summary>
//            public PhoneContactValidator()
//            {
//                RuleFor(x => x.Number).NotNull();
//            }

//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        private class AddressContactValidator : AbstractValidator<AddressContact>
//        {

//            /// <summary>
//            /// 
//            /// </summary>
//            public AddressContactValidator()
//            {
//                RuleFor(x => x.Line1).NotNull();
//                RuleFor(x => x.Line2).NotNull();
//            }

//        }


//        #endregion

//    }

//}

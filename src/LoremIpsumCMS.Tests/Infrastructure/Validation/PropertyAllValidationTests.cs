﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyAllValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllDefault()
        {
            var tested = new CustomerOrderAmountDefaultValidator(true);
            var customer = new Customer { Orders = new[] { new Order() } };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllDefaultInvalid()
        {
            var tested = new CustomerOrderAmountDefaultValidator(true);
            var customer = new Customer { Orders = new[] { new Order { Amount = 1 } } };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("OrderAmounts[0]", x.PropertyName);
                Assert.Equal("Order Amounts must not be set", x.Text);
            });
        }

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        private class CustomerOrderAmountDefaultValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="default"></param>
            public CustomerOrderAmountDefaultValidator(bool @default)
            {
                if (@default)
                {
                    RuleFor(x => x.OrderAmounts).All(x => x.Default());
                }
                else
                {
                    RuleFor(x => x.OrderAmounts).All(x => x.NotDefault());
                }
            }

        }

        #endregion

    }

}

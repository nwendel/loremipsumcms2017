﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyComparableValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThan()
        {
            var tested = new LessThanValidator();
            var order = new Order { Amount = 99 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThanInvalid()
        {
            var tested = new LessThanValidator();
            var order = new Order { Amount = 100 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal(nameof(Order.Amount), x.PropertyName);
                Assert.Equal("Amount must be less than 100", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThanOrEqualTo()
        {
            var tested = new LessThanOrEqualToValidator();
            var order = new Order { Amount = 100 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateLessThanOrEqualToInvalid()
        {
            var tested = new LessThanOrEqualToValidator();
            var order = new Order { Amount = 101 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal(nameof(Order.Amount), x.PropertyName);
                Assert.Equal("Amount must be less than or equal to 100", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThan()
        {
            var tested = new GreaterThanValidator();
            var order = new Order { Amount = 101 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThanInvalid()
        {
            var tested = new GreaterThanValidator();
            var order = new Order { Amount = 100 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal(nameof(Order.Amount), x.PropertyName);
                Assert.Equal("Amount must be greater than 100", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThanOrEqualTo()
        {
            var tested = new GreaterThanOrEqualToValidator();
            var order = new Order { Amount = 100 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateGreaterThanOrEqualToInvalid()
        {
            var tested = new GreaterThanOrEqualToValidator();
            var order = new Order { Amount = 99 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Order>(order, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal(nameof(Order.Amount), x.PropertyName);
                Assert.Equal("Amount must be greater than or equal to 100", x.Text);
            });
        }

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        public class LessThanValidator : AbstractValidator<Order>
        {

            /// <summary>
            /// 
            /// </summary>
            public LessThanValidator()
            {
                RuleFor(x => x.Amount).LessThan(100);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class LessThanOrEqualToValidator : AbstractValidator<Order>
        {

            /// <summary>
            /// 
            /// </summary>
            public LessThanOrEqualToValidator()
            {
                RuleFor(x => x.Amount).LessThanOrEqualTo(100);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class GreaterThanValidator : AbstractValidator<Order>
        {

            /// <summary>
            /// 
            /// </summary>
            public GreaterThanValidator()
            {
                RuleFor(x => x.Amount).GreaterThan(100);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        public class GreaterThanOrEqualToValidator : AbstractValidator<Order>
        {

            /// <summary>
            /// 
            /// </summary>
            public GreaterThanOrEqualToValidator()
            {
                RuleFor(x => x.Amount).GreaterThanOrEqualTo(100);
            }

        }

        #endregion

    }

}

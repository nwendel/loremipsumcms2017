﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyDefaultValidationTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateDefault()
        {
            var tested = new CustomerAgeDefaultValidator(true);
            var customer = new Customer();
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateDefaultInvalid()
        {
            var tested = new CustomerAgeDefaultValidator(true);
            var customer = new Customer { Age = 1 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must not be set", x.Text);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateNotDefault()
        {
            var tested = new CustomerAgeDefaultValidator(false);
            var customer = new Customer { Age = 1 };
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateNotDefaultInvalid()
        {
            var tested = new CustomerAgeDefaultValidator(false);
            var customer = new Customer();
            var serviceProvider = new ServiceCollection().BuildServiceProvider();
            var context = new ValidationContext<Customer>(customer, serviceProvider);
            var messages = tested.Validate(context).ToList();

            Assert.Single(messages);
            Assert.All(messages, x =>
            {
                Assert.Equal("Age", x.PropertyName);
                Assert.Equal("Age must be set", x.Text);
            });
        }

        #region Validators

        /// <summary>
        /// 
        /// </summary>
        public class CustomerAgeDefaultValidator : AbstractValidator<Customer>
        {

            /// <summary>
            /// 
            /// </summary>
            /// <param name="default"></param>
            public CustomerAgeDefaultValidator(bool @default)
            {
                if (@default)
                {
                    RuleFor(x => x.Age).Default();
                }
                else
                {
                    RuleFor(x => x.Age).NotDefault();
                }
            }

        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class SyntaxTests : AbstractValidator<Customer>
    {

        /// <summary>
        /// 
        /// </summary>
        public SyntaxTests()
        {
            RuleFor(x => x.Name).Null();
            RuleFor(x => x.Age).Default();

            RuleFor(x => x.OrderAmounts).All(x => x.Default());
            RuleFor(x => x.OrderAmountsArray).All(x => x.Default());

            RuleFor(x => x.Orders).All(x =>
            {
                x.RuleFor(o => o.Amount).Default();
            });

            RuleFor(x => x.DefaultContact).ValidateUsing<DefaultContactValidator>();

            ValidateUsing<CustomerAwareValdidator>();
        }

        private IEnumerable<ValidationMessage> Asdf(IValidationContext<Customer> context)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        private class DefaultContactValidator : AbstractValidator<AbstractContact>
        {
        }

        /// <summary>
        /// 
        /// </summary>
        private class CustomerAwareValdidator : AbstractValidator<IContactsAware>
        {
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Tests.Infrastructure.Validation.Classes;

namespace LoremIpsumCMS.Tests.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationMessageTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullPropertyName()
        {
            Assert.Throws<ArgumentNullException>(() => new ValidationMessage(null, "text"));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullText()
        {
            Assert.Throws<ArgumentNullException>(() => new ValidationMessage("proeprty", null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new ValidationMessage<Order>(x => x.Amount, "Some message");

            Assert.Equal(nameof(Order.Amount), tested.PropertyName);
            Assert.Equal("Some message", tested.Text);
        }

    }

}

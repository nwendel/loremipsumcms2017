﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;


namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class HostTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateNoArguments()
        {
            var tested = ActivatorEx.CreateInstance<Host>();

            Assert.NotNull(tested);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new Host("Asdf", "Qwerty", true);

            Assert.Null(tested.Id);
            Assert.Equal("Asdf", tested.Name);
            Assert.Equal("Qwerty", tested.SiteId);
        }

    }

}

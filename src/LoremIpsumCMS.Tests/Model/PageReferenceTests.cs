﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PageReferenceTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateNoArguments()
        {
            var tested = ActivatorEx.CreateInstance<PageReference>();

            Assert.NotNull(tested);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateFromId()
        {
            var tested = new PageReference("Pages/1-A");

            Assert.Equal("Pages/1-A", tested.PageId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateFromIdNullId()
        {
            Assert.Throws<ArgumentNullException>("pageId", () => new PageReference((string)null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateFromPageOverview()
        {
            var pageOverview = new PageOverview("Pages/1-A", "Sites/1-A", "/path", "title", "data-type-name", DateTime.Now);
            var tested = new PageReference(pageOverview);

            Assert.Equal("Pages/1-A", tested.PageId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateFromPageOverviewNullPageOverview()
        {
            Assert.Throws<ArgumentNullException>("pageOverview", () => new PageReference((PageOverview)null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateFromPageOverviewNullPageId()
        {
            var pageOverview = new PageOverview(null, "Sites/1-A", "/path", "title", "data-type-name", DateTime.Now);

            Assert.Throws<ArgumentNullException>("pageOverview", () => new PageReference(pageOverview));
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialReferenceTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateNoArguments()
        {
            var tested = ActivatorEx.CreateInstance<PartialReference>();

            Assert.NotNull(tested);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateFromId()
        {
            var tested = new PartialReference("Partials/1-A");

            Assert.Equal("Partials/1-A", tested.PartialId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateFromIdNullId()
        {
            Assert.Throws<ArgumentNullException>("partialId", () => new PartialReference((string)null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateFromPageOverview()
        {
            var partialOverview = new PartialOverview("Partials/1-A", "Sites/1-A", "name", "data-type-name", DateTime.Now);
            var tested = new PartialReference(partialOverview);

            Assert.Equal("Partials/1-A", tested.PartialId);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateFromPageOverviewNullPageOverview()
        {
            Assert.Throws<ArgumentNullException>("partialOverview", () => new PartialReference((PartialOverview)null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateFromPartialOverviewNullPartialId()
        {
            var partialOverview = new PartialOverview(null, "Sites/1-A", "name", "data-type-name", DateTime.Now);

            Assert.Throws<ArgumentNullException>("partialOverview", () => new PartialReference(partialOverview));
        }

    }

}

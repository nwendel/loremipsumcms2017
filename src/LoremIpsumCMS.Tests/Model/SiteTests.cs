﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanConstruct()
        {
            var tested = new Site();

            Assert.NotNull(tested.ExtensionProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new Site();
            var expectedCreatedAt = DateTime.Now;
            tested.ApplyEvent(new SiteCreatedEvent
            {
                Name = "Some Name",
                Hosts = new SiteHost[0],
                ExtensionProperties = new AbstractSiteExtensionProperties[0],
                CreatedAt = expectedCreatedAt
            });

            Assert.Equal("Some Name", tested.Name);
            Assert.Equal("some-name", tested.NameSlug);
            Assert.NotNull(tested.Hosts);
            Assert.NotNull(tested.ExtensionProperties);
            Assert.Equal(expectedCreatedAt, tested.CreatedAt);
            Assert.Null(tested.LastUpdatedAt);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanUpdate()
        {
            var tested = new Site();
            var expectedUpdatedAt = DateTime.Now;
            tested.ApplyEvent(new SiteUpdatedEvent
            {
                Name = "Some Name",
                Hosts = new SiteHost[0],
                ExtensionProperties = new AbstractSiteExtensionProperties[0],
                UpdatedAt = expectedUpdatedAt
            });

            Assert.Equal("Some Name", tested.Name);
            Assert.Equal("some-name", tested.NameSlug);
            Assert.NotNull(tested.ExtensionProperties);
            Assert.True(tested.LastUpdatedAt.HasValue);
            Assert.Equal(default(DateTime), tested.CreatedAt);
            Assert.Equal(expectedUpdatedAt, tested.LastUpdatedAt.Value);
        }

    }

}

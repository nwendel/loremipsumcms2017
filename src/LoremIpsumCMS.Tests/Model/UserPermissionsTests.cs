﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class UserPermissionsTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullUserId()
        {
            Assert.Throws<ArgumentNullException>("userId", () =>
            {
                var _ = new UserPermissions(null, new[] {"asdf"});
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullPermissionNames()
        {
            Assert.Throws<ArgumentNullException>("permissionNames", () =>
            {
                var _ = new UserPermissions("asdf", null);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNoPermissionNames()
        {
            Assert.Throws<ArgumentNullException>("permissionNames", () =>
            {
                var _ = new UserPermissions("asdf", new string[0]);
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullPermissionName()
        {
            Assert.Throws<ArgumentNullException>("permissionNames", () =>
            {
                var _ = new UserPermissions("asdf", new string[] {null});
            });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateNoArguments()
        {
            var tested = ActivatorEx.CreateInstance<UserPermissions>();

            Assert.NotNull(tested);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreate()
        {
            var tested = new UserPermissions("userId", new[] {"permissionName"});

            Assert.Equal("userId", tested.UserId);
            Assert.Single(tested.PermissionNames);
            Assert.Contains("permissionName", tested.PermissionNames);
        }
        
    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class AssetLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndPathNullSiteId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("siteId", () => session.LoadBy<Asset>(x => x.SiteIdAndPath(null, "path")));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndPathNullPath()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("path", () => session.LoadBy<Asset>(x => x.SiteIdAndPath("siteId", null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndPath()
        {
            var asset = new Asset();
            asset.Apply(new AssetCreatedEvent
            {
                SiteId = "sites/1-A",
                Path = "/"
            });

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(asset);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<Asset>(x => x.SiteIdAndPath(asset.SiteId, asset.Path));

                Assert.Equal(asset.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndPathNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var page = session.LoadBy<Asset>(x => x.SiteIdAndPath("sites/1-A", "/"));

                Assert.Null(page);
            }
        }

    }

}

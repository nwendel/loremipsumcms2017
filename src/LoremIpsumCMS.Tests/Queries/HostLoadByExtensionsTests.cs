﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class HostLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByNameNullName()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("name", () =>  session.LoadBy<Host>(x => x.Name(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByName()
        {
            var host = new Host("asdf", "sites/1-A", true);

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(host);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<Host>(x => x.Name(host.Name));

                Assert.Equal(host.Id, loaded.Id);
            }
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class IdentityUserLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByDisplayNameSlugNullName()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("displayNameSlug", () => session.LoadBy<IdentityUser>(x => x.DisplayNameSlug(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByDisplayNameSlug()
        {
            var identityUser = new IdentityUser();
            identityUser.Apply(new IdentityUserCreatedEvent
            {
                DisplayName = "asdf"
            });

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(identityUser);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<IdentityUser>(x => x.DisplayNameSlug(identityUser.DisplayNameSlug));

                Assert.Equal(identityUser.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByDisplayNameSlugNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var identityUser = session.LoadBy<IdentityUser>(x => x.DisplayNameSlug("asdf"));

                Assert.Null(identityUser);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsLoadByLoginProviderAndKeyOnNullSiteId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("loginProvider", () => session.LoadBy<IdentityUser>(x => x.LoginProviderAndKey(null, "key")));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByLoginProviderAndKeyNullPath()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("loginProviderKey", () => session.LoadBy<IdentityUser>(x => x.LoginProviderAndKey("provider", null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByLoginProviderAndKey()
        {
            var identityUser = new IdentityUser();
            identityUser.Apply(new IdentityUserExternalAccountCreatedEvent()
            {
                LoginProvider = "provider",
                LoginProviderKey = "key"
            });

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(identityUser);
                var externalAccount = new ExternalAccount("provider", "key", identityUser.Id);
                session.Store(externalAccount);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<IdentityUser>(x => x.LoginProviderAndKey("provider", "key"));

                Assert.Equal(identityUser.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByLoginProviderAndKeyNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var identityUser = session.LoadBy<IdentityUser>(x => x.LoginProviderAndKey("provider", "key"));

                Assert.Null(identityUser);
            }
        }

    }

}

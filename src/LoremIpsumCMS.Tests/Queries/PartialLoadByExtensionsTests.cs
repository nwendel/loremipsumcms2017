﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndNameSlugNullSiteId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("siteId", () => session.LoadBy<Partial>(x => x.SiteIdAndNameSlug(null, "nameSlug")));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndNameSlugNullNameSlug()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("nameSlug", () => session.LoadBy<Partial>(x => x.SiteIdAndNameSlug("siteId", null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndNameSlug()
        {
            var partial = new Partial();
            partial.Apply(new PartialCreatedEvent
            {
                SiteId = "sites/1-A",
                Name = "asdf"
            });

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(partial);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<Partial>(x => x.SiteIdAndNameSlug(partial.SiteId, partial.NameSlug));

                Assert.Equal(partial.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndPathNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var page = session.LoadBy<Partial>(x => x.SiteIdAndNameSlug("sites/1-A", "/"));

                Assert.Null(page);
            }
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialOverviewLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndNameSlugNullSiteId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("siteId",
                () => session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug(null, "nameSlug")));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndNameSlugNullNameSlug()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("nameSlug",
                () => session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug("siteId", null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndNameSlug()
        {
            var pageOverview = new PartialOverview(null, "sites/1-A", "asdf", null, DateTime.MinValue);

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(pageOverview);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug("sites/1-A", "asdf"));

                Assert.Equal(pageOverview.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndNameSlugNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var page = session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug("sites/1-A", "asdf"));

                Assert.Null(page);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsLoadBySiteIdAndNameSlugsOnNullSiteId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("siteId",
                () => session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlugs(null, new[] {"nameSlugs"})));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadBySiteIdAndNameSlugsNullNameSlug()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("nameSlugs",
                () => session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlugs("siteId", null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndNameSlugs()
        {
            var pageOverview = new PartialOverview(null, "sites/1-A", "asdf", null, DateTime.MinValue);

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(pageOverview);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlugs("sites/1-A", new[] {"asdf"}));

                Assert.Equal(pageOverview.Id, loaded[0].Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadBySiteIdAndnameSlugNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var page = session.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlugs("sites/1-A", new[] {"asdf"}));

                Assert.Null(page[0]);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByPartialIdNullPartialId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("partialId", () => session.LoadBy<PartialOverview>(x => x.PartialId(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByPartialId()
        {
            var pageOverview = new PartialOverview("partials/1-A", "sites/1-A", "asdf", null, DateTime.MinValue);

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(pageOverview);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<PartialOverview>(x => x.PartialId(pageOverview.PartialId));

                Assert.Equal(pageOverview.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByPartialIdNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var partialOverview = session.LoadBy<PartialOverview>(x => x.PartialId("partials/1-A"));

                Assert.Null(partialOverview);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByPartialIdsNullPartialId()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("partialIds", () => session.LoadBy<PartialOverview>(x => x.PartialIds(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByPartialIds()
        {
            var pageOverviews = new PartialOverview("partials/1-A", "sites/1-A", "asdf", null, DateTime.MinValue);

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(pageOverviews);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<PartialOverview>(x => x.PartialIds(new[] {pageOverviews.PartialId}));

                Assert.Equal(pageOverviews.Id, loaded[0].Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByPartialIdsNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var partialOverviews = session.LoadBy<PartialOverview>(x => x.PartialIds(new []{ "partials/1-A"}));

                Assert.Null(partialOverviews[0]);
            }
        }
    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Events;
using Xunit;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.TestHelper;

namespace LoremIpsumCMS.Tests.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteLoadByExtensionsTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByNameSlugNullNameSlug()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("nameSlug", () => session.LoadBy<Site>(x => x.NameSlug(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByNameSlug()
        {
            var site = new Site();
            site.Apply(new SiteCreatedEvent
            {
                Name = "asdf"
            });

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(site);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<Site>(x => x.NameSlug("asdf"));

                Assert.Equal(site.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByNameSlugNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var site = session.LoadBy<Site>(x => x.NameSlug("asdf"));

                Assert.Null(site);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnLoadByHostnameNullHostname()
        {
            var session = GetDocumentStore().OpenSession();

            Assert.Throws<ArgumentNullException>("hostname", () => session.LoadBy<Site>(x => x.Hostname(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByHostname()
        {
            var site = new Site();
            site.Apply(new SiteCreatedEvent
            {
                Name = "asdf"
            });

            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                session.Store(site);
                var host = new Host(site.Name, site.Id, true);
                session.Store(host);
                session.SaveChanges();
            }

            using (var session = store.OpenSession())
            {
                var loaded = session.LoadBy<Site>(x => x.Hostname("asdf"));

                Assert.Equal(site.Id, loaded.Id);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadByHostnameNotFound()
        {
            var store = GetDocumentStore();
            using (var session = store.OpenSession())
            {
                var site = session.LoadBy<Site>(x => x.Hostname("asdf"));

                Assert.Null(site);
            }
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Moq;
using Xunit;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Tests.Services.Classes;

namespace LoremIpsumCMS.Tests.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentTypeServicePageExtensionTests
    {

        private readonly Mock<IReflectionService> _reflectionServiceMock;

        /// <summary>
        /// 
        /// </summary>
        public ContentTypeServicePageExtensionTests()
        {
            _reflectionServiceMock = new Mock<IReflectionService>();
            _reflectionServiceMock.Setup(x => x.FindApplicationAndLoremIpsumAssemblies()).Returns(new[] { typeof(ContentTypeServicePageExtensionTests).Assembly });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetPageExtensionPropertyTypes()
        {
            var tested = new ContentTypeService(_reflectionServiceMock.Object);
            var pageExtensionProperties = tested.FindPageExtensionPropertyTypes()
                .Select(x => x.Type)
                .ToList();

            Assert.Contains(typeof(SomePageExtensionProperties), pageExtensionProperties);
            Assert.DoesNotContain(typeof(SomeAbstractPageExtensionProperties), pageExtensionProperties);
            Assert.DoesNotContain(typeof(SomeGenericPageExtensionProperties<>), pageExtensionProperties);
        }

    }

}

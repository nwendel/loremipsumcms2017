﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion

using System;
using System.Linq;
using Moq;
using Xunit;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Tests.Services.Classes;

namespace LoremIpsumCMS.Tests.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentTypeServicePartialDataTests
    {

        private readonly Mock<IReflectionService> _reflectionServiceMock;

        /// <summary>
        /// 
        /// </summary>
        public ContentTypeServicePartialDataTests()
        {
            _reflectionServiceMock = new Mock<IReflectionService>();
            _reflectionServiceMock.Setup(x => x.FindApplicationAndLoremIpsumAssemblies()).Returns(new[] { typeof(ContentTypeServicePartialDataTests).Assembly });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetPartialDataTypes()
        {
            var tested = new ContentTypeService(_reflectionServiceMock.Object);
            var partialExtensionProperties = tested.FindPartialDataTypes()
                .Select(x => x.Type)
                .ToList();

            Assert.Contains(typeof(AnotherPartialData), partialExtensionProperties);
            Assert.DoesNotContain(typeof(AnotherAbstractPartialData), partialExtensionProperties);
            Assert.DoesNotContain(typeof(AnotherGenericPartialData<>), partialExtensionProperties);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetPartialDataTypeByNameSlugNullNameSlug()
        {
            var tested = new ContentTypeService(_reflectionServiceMock.Object);

            Assert.Throws<ArgumentNullException>("nameSlug", () => { tested.GetPartialDataTypeByNameSlug(null); });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetPartialDataTypeByNameSlug()
        {
            var tested = new ContentTypeService(_reflectionServiceMock.Object);

            var partialDataType = tested.GetPartialDataTypeByNameSlug("another");

            Assert.Equal(typeof(AnotherPartialData), partialDataType.Type);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetPartialDataTypeByTypeFullNameSlugNullName()
        {
            var tested = new ContentTypeService(_reflectionServiceMock.Object);

            Assert.Throws<ArgumentNullException>("typeFullName", () => { tested.GetPartialDataTypeByTypeFullName(null); });
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetPartialDataTypeByTypeFullName()
        {
            var tested = new ContentTypeService(_reflectionServiceMock.Object);

            var partialDataType = tested.GetPartialDataTypeByTypeFullName(typeof(AnotherPartialData).FullName);

            Assert.Equal(typeof(AnotherPartialData), partialDataType.Type);
        }
        
    }

}

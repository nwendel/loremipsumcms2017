﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.Options;
using Moq;
using Xunit;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Infrastructure.Modules;

namespace LoremIpsumCMS.Tests.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class ReflectionServiceTests
    {

        private readonly IModule _module;
        private readonly LoremIpsumOptions _options;
        private readonly Mock<IOptions<LoremIpsumOptions>> _optionsMock;

        /// <summary>
        /// 
        /// </summary>
        public ReflectionServiceTests()
        {
            _module = new CoreModule();
            _options = new LoremIpsumOptions { ApplicationRootNamespaces = new[] { typeof(ReflectionServiceTests).Assembly.GetRootNamespace() } };
            _optionsMock = new Mock<IOptions<LoremIpsumOptions>>();
            _optionsMock.Setup(x => x.Value).Returns(_options);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindLoremIpsumAssemblies()
        {
            var tested = new ReflectionService(new[] { _module }, null);
            var assemblies = tested.FindLoremIpsumAssemblies();

            Assert.Single(assemblies);
            Assert.Contains(_module.Assembly, assemblies);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindApplicationAssemblies()
        {
            var tested = new ReflectionService(null, _optionsMock.Object);
            var assemblies = tested.FindApplicationAssemblies();

            Assert.Contains(typeof(ReflectionServiceTests).Assembly, assemblies);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanFindApplicationAndLoremIpsumAssemblies()
        {
            var tested = new ReflectionService(new[] { _module }, _optionsMock.Object);
            var assemblies = tested.FindApplicationAndLoremIpsumAssemblies();

            Assert.Contains(_module.Assembly, assemblies);
            Assert.Contains(typeof(ReflectionServiceTests).Assembly, assemblies);
        }

    }

}

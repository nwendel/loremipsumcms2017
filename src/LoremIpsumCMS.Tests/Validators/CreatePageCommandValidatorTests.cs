﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using Xunit;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Validators;

namespace LoremIpsumCMS.Tests.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class CreatePageCommandValidatorTests
    {

        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        public CreatePageCommandValidatorTests()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.Install<ValidatorsServiceInstaller>();
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllNull()
        {
            var command = new CreatePageCommand();
            var context = new ValidationContext<CreatePageCommand>(command, _serviceProvider);
            var tested = new CreatePageCommandValidator();

            var messages = tested.Validate(context).ToList();

            Assert.Equal(6, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePageCommand.SiteId));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePageCommand.Path));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePageCommand.Title));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePageCommand.Data));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePageCommand.ExtensionProperties));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePageCommand.CreatedAt));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateNullExtensionProperty()
        {
            var command = new CreatePageCommand { ExtensionProperties = new AbstractPageExtensionProperties[] { null } };
            var context = new ValidationContext<CreatePageCommand>(command, _serviceProvider);
            var tested = new CreatePageCommandValidator();

            var messages = tested.Validate(context).ToList();

            Assert.Contains(messages, x => x.PropertyName == $"{nameof(CreatePageCommand.ExtensionProperties)}[0]");
        }

    }

}

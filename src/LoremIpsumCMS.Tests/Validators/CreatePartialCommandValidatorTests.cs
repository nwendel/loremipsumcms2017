﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using Xunit;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Validators;

namespace LoremIpsumCMS.Tests.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class CreatePartialCommandValidatorTests
    {

        private readonly IServiceProvider _serviceProvider;

        /// <summary>
        /// 
        /// </summary>
        public CreatePartialCommandValidatorTests()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.Install<ValidatorsServiceInstaller>();
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAllNull()
        {
            var command = new CreatePartialCommand();
            var context = new ValidationContext<CreatePartialCommand>(command, _serviceProvider);
            var tested = new CreatePartialCommandValidator();

            var messages = tested.Validate(context).ToList();

            Assert.Equal(5, messages.Count);
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePartialCommand.SiteId));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePartialCommand.Name));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePartialCommand.Data));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePartialCommand.ExtensionProperties));
            Assert.Contains(messages, x => x.PropertyName == nameof(CreatePartialCommand.CreatedAt));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateNullExtensionProperty()
        {
            var command = new CreatePartialCommand { ExtensionProperties = new AbstractPartialExtensionProperties[] { null } };
            var context = new ValidationContext<CreatePartialCommand>(command, _serviceProvider);
            var tested = new CreatePartialCommandValidator();

            var messages = tested.Validate(context).ToList();

            Assert.Contains(messages, x => x.PropertyName == $"{nameof(CreatePageCommand.ExtensionProperties)}[0]");
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Validators.ValueValidators;
using Xunit;

namespace LoremIpsumCMS.Tests.Validators.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class AbsolutePathValueValidatorTests
    {

        private readonly PropertyChain _propertyChain = new PropertyChain();

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidatePath()
        {
            var tested = new AbsolutePathValueValidator();

            var messages = tested.Validate(_propertyChain, "/asdf", null);

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateAnotherPath()
        {
            var tested = new AbsolutePathValueValidator();

            var messages = tested.Validate(_propertyChain, "/asdf/qwerty", null);

            Assert.Empty(messages);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateRootPath()
        {
            var tested = new AbsolutePathValueValidator();

            var messages = tested.Validate(_propertyChain, "/", null);

            Assert.Empty(messages);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Raven.Client.Documents.Session;
using Raven.Client.Documents;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Blogs.Commands;
using LoremIpsumCMS.Module.Blogs.Model;
using LoremIpsumCMS.Module.Blogs.Queries;
using LoremIpsumCMS.Web.Infrastructure.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.ViewModels.AdminBlog;
using LoremIpsumCMS.Web.Module.Blogs.Authorization;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [Area(BlogAreaNames.AdminBlogs)]
    [LoremIpsumAdminRoute("blog")]
    public class AdminBlogController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminBlogController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IMapper _mapper;
        private readonly ICommandSender _commandSender;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="mapper"></param>
        /// <param name="commandSender"></param>
        public AdminBlogController(
            IDocumentSession documentSession,
            IMapper mapper,
            ICommandSender commandSender)
        {
            _documentSession = documentSession;
            _mapper = mapper;
            _commandSender = commandSender;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        [Authorize(Policy = nameof(BlogPermissions.ManageSiteBlogs))]
        [HttpGet("")]
        public IActionResult Index(SiteReference siteReference)
        {
            var blogs = _documentSession.Query<Blog>()
                .Where(x => x.SiteId == siteReference.SiteId)
                .ToList();

            var viewModel = new IndexViewModel
            {
                Blogs = blogs
                    .MapTo<IndexBlogViewModel>(_mapper)
                    .ToArray()
            };
            return View(viewModel);
        }

        #endregion

        #region Dashboard

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public ValidationResult ValidateDashboard(SiteReference siteReference, string nameSlug)
        {
            var blog = _documentSession.LoadBy<Blog>(x => x.SiteReferenceAndNameSlug(siteReference, nameSlug));
            if (blog == null)
            {
                return ValidationResult.NotFound;
            }

            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        //[AuthorizeResource(Policy = nameof(BlogResourceRequirements.ManageBlog))]
        [HttpGet("{nameSlug}/dashboard")]
        public IActionResult Dashboard(SiteReference siteReference, string nameSlug)
        {
            return View();
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("create")]
        public IActionResult Create()
        {
            var viewModel = new CreateViewModel();
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult CreateSave(SiteReference siteReference, CreateViewModel viewModel)
        {
            var command = new CreateBlogCommand
            {
                SiteId = siteReference.SiteId,
                Name = viewModel.Name,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction("Index");
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Blogs.Model;
using LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.ViewModels.AdminCategory;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(BlogAreaNames.AdminBlogs)]
    [LoremIpsumAdminRoute("blog/categories")]
    public class AdminCategoryController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminCategoryController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="mapper"></param>
        public AdminCategoryController(
            IDocumentSession documentSession,
            IMapper mapper)
        {
            _documentSession = documentSession;
            _mapper = mapper;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        public IActionResult Index(SiteReference siteReference)
        {
            var categories = _documentSession.Query<Category>()
                .Where(x => x.SiteId == siteReference.SiteId)
                .ToList();

            var viewModel = new IndexViewModel
            {
                Categories = categories
                    .MapTo<IndexCategoryViewModel>(_mapper)
                    .ToArray()
            };
            return View(viewModel);
        }

        #endregion

    }

}

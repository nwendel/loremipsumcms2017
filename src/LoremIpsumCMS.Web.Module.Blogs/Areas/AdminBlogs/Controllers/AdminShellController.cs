﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminShell;
using LoremIpsumCMS.Web.Infrastructure.Mvc;

namespace LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(BlogAreaNames.AdminBlogs)]
    public class AdminShellController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminShellController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IHttpContextAccessor _httpContextAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        public AdminShellController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Blog Sidebar

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult Sidebar()
        {
            return PartialView("_Sidebar");
        }

        #endregion

        #region Blog Menu

        /// <summary>
        /// 
        /// </summary>
        [ViewComponentAction]
        public IActionResult SidebarMenu()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            var path = httpContext.Request.Path.ToString();
            var viewModel = CreateBlogMenuModel();

            var menuItems = viewModel.SelectMany(x => x.Items);
            UpdateIsActive(menuItems, path);

            return PartialView("~/Areas/AdminShell/Views/AdminShell/_MainMenu.cshtml", viewModel);
        }

        #endregion

        #region Create Blog Menu Model

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private MenuViewModel[] CreateBlogMenuModel()
        {
            var result = new[]
            {
                new MenuViewModel
                {
                    Title = "Blog",
                    Items = new[]
                    {
                        new MenuItemViewModel
                        {
                            IconClass = "ti-home",
                            Title = "Dashboard",
                            Url = Url.Action("Details", "AdminBlog", new { Area = "AdminBlogs", NameSlug = "asdf" })
                        },
                        new MenuItemViewModel
                        {
                            IconClass = "ti-layout",
                            Title = "All Posts",
                            Url = Url.Action("Index", "AdminPost", new { Area = "AdminBlogs" })
                        },
                        new MenuItemViewModel
                        {
                            IconClass = "ti-layers-alt",
                            Title = "New Post",
                            Url = Url.Action("Create", "AdminPost", new { Area = "AdminBlogs" })
                        },
                    }
                },
                new MenuViewModel
                {
                    Title = "Documentation",
                    Items = new []
                    {
                        new MenuItemViewModel
                        {
                            Title = "Authors",
                            Url = "#"
                        },
                        new MenuItemViewModel
                        {
                            Title = "Developers",
                            Url = "#"
                        },
                        new MenuItemViewModel
                        {
                            Title = "About",
                            Url = "#"
                        }
                    }
                }
            };
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuItems"></param>
        private void UpdateIsActive(IEnumerable<MenuItemViewModel> menuItems, string path)
        {
            if (menuItems == null)
            {
                return;
            }

            var allMenuItems = menuItems
                .Flatten(x => x.Children)
                .OrderByDescending(x => (x.Url ?? "").Length);

            foreach (var menuItem in allMenuItems)
            {
                if (path.StartsWith(menuItem.Url ?? "#"))
                {
                    menuItem.IsActive = true;
                    break;
                }
            }
        }

        #endregion

    }

}

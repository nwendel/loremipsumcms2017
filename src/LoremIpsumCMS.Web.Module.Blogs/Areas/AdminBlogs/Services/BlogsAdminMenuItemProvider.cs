﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Web.Areas.AdminShell.Services;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminShell;
using LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.Controllers;
using LoremIpsumCMS.Web.Module.Blogs.Authorization;

namespace LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class BlogsAdminMenuItemProvider : AbstractAdminMenuItemProvider
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlHelper"></param>
        /// <param name="authorizationService"></param>
        public BlogsAdminMenuItemProvider(IUrlHelper urlHelper, IAuthorizationService authorizationService) : 
            base(urlHelper, authorizationService)
        {
        }

        #endregion

        #region Get Menu Item

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override async Task<MenuItemViewModel> GetMenuItem(ClaimsPrincipal user, string siteId)
        {
            return new MenuItemViewModel
            {
                Title = "Blog",
                Url = "javascript:;",
                Children = new []
                {
                    new MenuItemViewModel
                    {
                        Title = "Blogs",
                        Url = Url.Action(nameof(AdminBlogController.Index), AdminBlogController.Name, new { Area = BlogAreaNames.AdminBlogs }),
                        IsVisible = await GetIsVisible(user, BlogPermissions.ManageSiteBlogs, siteId)

                    },
                    new MenuItemViewModel
                    {
                        Title = "Create Blog",
                        Url = Url.Action(nameof(AdminBlogController.Create), AdminBlogController.Name, new { Area = BlogAreaNames.AdminBlogs }),
                        IsVisible = await GetIsVisible(user, BlogPermissions.ManageSiteBlogs, siteId)
                    }
                }
            };
        }

        #endregion

    }

}

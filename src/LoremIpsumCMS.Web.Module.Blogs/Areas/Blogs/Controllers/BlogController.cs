﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure;

namespace LoremIpsumCMS.Web.Module.Blogs.Areas.Blogs.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(BlogAreaNames.Blogs)]
    [Route("blog")]
    public class BlogController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(BlogController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet("")]
        public IActionResult Index()
        {
            return View();
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Module.Blogs.Model;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Authorization;

namespace LoremIpsumCMS.Web.Module.Blogs.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class BlogPermissions
    {

        #region Fields

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSiteBlogs = new ResourcePermission(nameof(ManageSiteBlogs), nameof(Site), new[] { Permissions.ManageSiteContents });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageBlog = new ResourcePermission(nameof(ManageBlog), nameof(Blog), new[] { ManageSiteBlogs });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageBlogPosts = new ResourcePermission(nameof(ManageBlogPosts), nameof(Blog), new[] { ManageBlog });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageBlogCategories = new ResourcePermission(nameof(ManageBlogCategories), nameof(Blog), new[] { ManageBlog });

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        private BlogPermissions()
        {
        }

        #endregion

    }

}

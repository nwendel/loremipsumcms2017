﻿//#region License
//// Copyright (c) Niklas Wendel 2017-2018
//// 
//// Licensed under the Apache License, Version 2.0 (the "License"); 
//// you may not use this file except in compliance with the License. 
//// You may obtain a copy of the License at 
//// 
//// http://www.apache.org/licenses/LICENSE-2.0 
//// 
//// Unless required by applicable law or agreed to in writing, software 
//// distributed under the License is distributed on an "AS IS" BASIS, 
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//// See the License for the specific language governing permissions and 
//// limitations under the License.
//#endregion
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Microsoft.Extensions.DependencyInjection;
//using Raven.Client.Documents.Session;
//using LoremIpsumCMS.Infrastructure.Raven;
//using LoremIpsumCMS.Model;
//using LoremIpsumCMS.Module.Blogs.Model;
//using LoremIpsumCMS.Module.Blogs.Queries;
//using LoremIpsumCMS.Web.Infrastructure.Authorization;

//namespace LoremIpsumCMS.Web.Module.Blogs.Authorization.ResourceRequirements
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    public class ManageBlogResourceRequirement : AbstractResourceRequirement<Blog>
//    {

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        public ManageBlogResourceRequirement(string name) : base(name)
//        {
//        }

//        #endregion

//        #region Get Resource From Action Arguments

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="arguments"></param>
//        /// <param name="serviceProvider"></param>
//        /// <returns></returns>
//        public override Blog GetResourceFromActionArguments(IDictionary<string, object> arguments, IServiceProvider serviceProvider)
//        {
//            if (!TryGetActionArgument<SiteReference>(arguments, "siteReference", out var siteReference))
//            {
//                throw new LoremIpsumException("TODO");
//            }
//            if (!TryGetActionArgument<string>(arguments, "nameSlug", out var nameSlug))
//            {
//                throw new LoremIpsumException("TODO");
//            }

//            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
//            var blog = documentSession.LoadBy<Blog>(x => x.SiteReferenceAndNameSlug(siteReference, nameSlug));
//            return blog;
//        }

//        #endregion

//    }

//}

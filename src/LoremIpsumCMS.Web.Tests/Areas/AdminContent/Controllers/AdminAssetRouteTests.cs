﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using MvcRouteTester.AspNetCore;
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminContent.Controllers;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminAsset;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminContent.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminAssetRouteTests : IClassFixture<RouteTestFixture>
    {

        private readonly TestServer _server;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeTestFixture"></param>
        public AdminAssetRouteTests(RouteTestFixture routeTestFixture)
        {
            _server = routeTestFixture.Server;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetIndex()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/assets"),
                routeAssert => routeAssert.MapsTo<AdminAssetController>(x => x.Index(null)));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCreate()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/assets/create"),
                routeAssert => routeAssert.MapsTo<AdminAssetController>(x => x.Create(Args.Any<SiteReference>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostCreate()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/assets/create"),
                routeAssert => routeAssert.MapsTo<AdminAssetController>(x => x.Create(Args.Any<SiteReference>(), Args.Any<CreateViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostDelete()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/assets/delete"),
                routeAssert => routeAssert.MapsTo<AdminAssetController>(x => x.Delete(Args.Any<SiteReference>(), Args.Any<DeleteViewModel>())));
        }
        
    }

}

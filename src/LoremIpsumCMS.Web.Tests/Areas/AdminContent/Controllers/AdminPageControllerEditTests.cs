﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.TestHelper;
using LoremIpsumCMS.Web.Areas.AdminContent.Controllers;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPage;
using LoremIpsumCMS.Web.Infrastructure.Validation;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminContent.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminPageControllerEditTests : AbstractRavenTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateEditPostNoSiteReturnsBadRequest()
        {
            var documentSession = GetDocumentStore().OpenSession();
            var tested = new AdminPageController(documentSession, null, null, null);

            var viewModel = new EditViewModel
            {
                OldPath = "/asdf"
            };

            var result = tested.ValidateEdit(null, viewModel);

            Assert.Equal(ValidationResult.BadRequest, result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanValidateEditPostNoPageReturnsBadRequest()
        {
            var documentSession = GetDocumentStore().OpenSession();
            var tested = new AdminPageController(documentSession, null, null, null);

            var siteReferece = new SiteReference("Sites/1-A");
            var viewModel = new EditViewModel
            {
                OldPath = "/asdf"
            };

            var result = tested.ValidateEdit(siteReferece, viewModel);

            Assert.Equal(ValidationResult.BadRequest, result);

        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using MvcRouteTester.AspNetCore;
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminContent.Controllers;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPartial;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminContent.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminPartialRouteTests : IClassFixture<RouteTestFixture>
    {

        private readonly TestServer _server;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeTestFixture"></param>
        public AdminPartialRouteTests(RouteTestFixture routeTestFixture)
        {
            _server = routeTestFixture.Server;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetIndex()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/partials"),
                routeAssert => routeAssert.MapsTo<AdminPartialController>(x => x.Index(Args.Any<SiteReference>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCreate()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/partials/create?partialtypenameslug=partial-type-name-slug"),
                routeAssert => routeAssert.MapsTo<AdminPartialController>(x => x.Create(Args.Any<SiteReference>(), "partial-type-name-slug")));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostCreate()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/partials/create"),
                routeAssert => routeAssert.MapsTo<AdminPartialController>(x => x.Create(Args.Any<SiteReference>(), Args.Any<CreateViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetEdit()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/partials/edit?nameslug=asdf"),
                routeAssert => routeAssert.MapsTo<AdminPartialController>(x => x.Edit(Args.Any<SiteReference>(), "asdf")));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostEdit()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/partials/edit"),
                routeAssert => routeAssert.MapsTo<AdminPartialController>(x => x.Edit(Args.Any<SiteReference>(), Args.Any<EditViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostDelete()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/partials/delete"),
                routeAssert => routeAssert.MapsTo<AdminPartialController>(x => x.Delete(Args.Any<SiteReference>(), Args.Any<DeleteViewModel>())));
        }
        
    }

}

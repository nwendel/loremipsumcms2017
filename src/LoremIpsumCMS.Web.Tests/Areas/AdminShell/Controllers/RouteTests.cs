﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using MvcRouteTester.AspNetCore;
using Xunit;
using LoremIpsumCMS.Web.Areas.AdminShell.Controllers;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminSearch;
using System.Collections.Generic;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteTests : IClassFixture<RouteTestFixture>
    {

        private readonly TestServer _server;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeTestFixture"></param>
        public RouteTests(RouteTestFixture routeTestFixture)
        {
            _server = routeTestFixture.Server;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetDashboard()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin"),
                routeAssert => routeAssert.MapsTo<AdminDashboardController>(x => x.Details()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetModulesIndex()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/modules"),
                routeAssert => routeAssert.MapsTo<AdminModuleController>(x => x.Index()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetSettingsIndex()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/options"),
                routeAssert => routeAssert.MapsTo<AdminOptionsController>(x => x.Details()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostSearch()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithPathAndQuery("/admin/search")
                    .WithMethod(HttpMethod.Post)
                    .WithFormData(new Dictionary<string, string>
                    {
                        { nameof(SearchViewModel.Text), "value" }
                    }),
                routeAssert => routeAssert
                    .MapsTo<AdminSearchController>(x => x.Search(Args.Any<SearchViewModel>()))
                    .ForParameter<SearchViewModel>("viewModel", p => Assert.Equal("value", p.Text)));
        }
        
    }

}

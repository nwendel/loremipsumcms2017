﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using Xunit;
using LoremIpsumCMS.Web.Areas.AdminShell.Mappers.AdminModule;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminModule;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminShell.Mappers.AdminModule
{

    /// <summary>
    /// 
    /// </summary>
    public class AutoMapperProfileTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ConfigurationIsValid()
        {
            var mapperProfile = new MapperProfile();
            var mapperConfiguration = new MapperConfiguration(x => x.AddProfile(mapperProfile));

            mapperConfiguration.AssertConfigurationIsValid();
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanMapModuleToIndexModuleViewModule()
        {
            var mapperProfile = new MapperProfile();
            var mapperConfiguration = new MapperConfiguration(x => x.AddProfile(mapperProfile));
            var tested = mapperConfiguration.CreateMapper();

            var source = new CoreWebModule();
            var result = tested.Map<IndexModuleViewModel>(source);

            Assert.Equal(source.Name, result.Name);
            Assert.Equal(source.Version, result.Version);
            Assert.Equal(source.Assembly.GetName().Name, result.AssemblyName);
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using MvcRouteTester.AspNetCore;
using Xunit;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminSite.Controllers;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteRouteTests : IClassFixture<RouteTestFixture>
    {

        private readonly TestServer _server;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeTestFixture"></param>
        public AdminSiteRouteTests(RouteTestFixture routeTestFixture)
        {
            _server = routeTestFixture.Server;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetIndex()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/site"),
                routeAssert => routeAssert.MapsTo<AdminSiteController>(x => x.Index()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetCreate()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/site/create"),
                routeAssert => routeAssert.MapsTo<AdminSiteController>(x => x.Create()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostCreate()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/site/create"),
                routeAssert => routeAssert.MapsTo<AdminSiteController>(x => x.Create(Args.Any<CreateViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetEdit()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/site/edit"),
                routeAssert => routeAssert.MapsTo<AdminSiteController>(x => x.Edit(Args.Any<Site>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostEdit()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/site/edit"),
                routeAssert => routeAssert.MapsTo<AdminSiteController>(x => x.Edit(Args.Any<EditViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostDelete()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/site/delete"),
                routeAssert => routeAssert.MapsTo<AdminSiteController>(x => x.Delete(Args.Any<DeleteViewModel>())));
        }

    }

}



﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using MvcRouteTester.AspNetCore;
using Xunit;
using LoremIpsumCMS.Web.Areas.AdminUser.Controllers;
using LoremIpsumCMS.Web.Areas.AdminUser.ViewModels.Account;

namespace LoremIpsumCMS.Web.Tests.Areas.AdminUser.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AccountRouteTests : IClassFixture<RouteTestFixture>
    {

        private readonly TestServer _server;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeTestFixture"></param>
        public AccountRouteTests(RouteTestFixture routeTestFixture)
        {
            _server = routeTestFixture.Server;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetLogin()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/login"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.Login(Args.Any<string>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostLogin()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/login"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.Login(Args.Any<LoginViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetRegister()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/login/register"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.Register()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostRegister()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/login/register"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.Register(Args.Any<RegisterViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostLoginExternal()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/login/external"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.LoginExternal(Args.Any<LoginExternalViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetLoginExternalCallback()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/login/external/callback"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.LoginExternalCallback(Args.Any<LoginExternalCallbackViewModel>())));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetRegisterExternal()
        {
            RouteAssert.For(
                _server,
                request => request.WithPathAndQuery("/admin/login/register/external"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.RegisterExternal()));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanPostRegisterExternal()
        {
            RouteAssert.For(
                _server,
                request => request
                    .WithMethod(HttpMethod.Post)
                    .WithPathAndQuery("/admin/login/register/external"),
                routeAssert => routeAssert.MapsTo<AccountController>(x => x.RegisterExternal(Args.Any<RegisterExternalViewModel>())));
        }
        
    }

}

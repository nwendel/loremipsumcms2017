﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Xunit;
using LoremIpsumCMS.Web.Infrastructure.Authorization;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class PermissionTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullName()
        {
            Assert.Throws<ArgumentNullException>(() => new Permission(null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullImpliedBy()
        {
            Assert.Throws<ArgumentNullException>(() => new Permission("asdf", null));
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnCreateNullImpliedByItem()
        {
            Assert.Throws<ArgumentNullException>(() => new Permission("asdf", new Permission[] {null}));
        }

    }

}

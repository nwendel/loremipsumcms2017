﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.Extensions.DependencyInjection;
using AttachedProperties;
using Xunit;
using LoremIpsumCMS.Web.Infrastructure.Modules;
using LoremIpsumCMS.Web.Tests.Infrastructure.Module.Classes;
using LoremIpsumCMS.Infrastructure.Modules;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleLoaderTests
    {

#if false

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanLoadModule()
        {
            var tested = new ModuleLoader();

            var modules = tested.LoadAll();

            Assert.Equal(3, modules.Count());
            Assert.Contains(typeof(TestModule), modules);
            Assert.Contains(typeof(CoreModule), modules);
            Assert.Contains(typeof(CoreWebModule), modules);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void ThrowsOnGetModulesBeforeLoaded()
        {
            var tested = new ServiceCollection();

            Assert.Throws<LoremIpsumException>(() => tested.GetModules());
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanGetModulesTwice()
        {
            var tested = new ServiceCollection();
            tested.AddSingleton<IWebModule, TestModule>();
            tested.SetAttachedValue(AttachedProperty.AreModulesLoaded, true);

            var first = tested.GetModules();
            var second = tested.GetModules();

            Assert.Same(first, second);
        }

#endif
        
    }

}

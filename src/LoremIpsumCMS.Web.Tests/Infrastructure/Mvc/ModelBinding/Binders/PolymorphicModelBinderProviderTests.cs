﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using Moq;
using Xunit;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding.Binders;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class PolymorphicModelBinderProviderTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateBinder()
        {
            var contextMock = new Mock<ModelBinderProviderContext>();
            var modelMetadataMock = new Mock<ModelMetadata>(ModelMetadataIdentity.ForType(typeof(object)));

            contextMock.Setup(x => x.Metadata).Returns(modelMetadataMock.Object);

            var tested = new PolymorphicModelBinderProvider<object>();
            var result = tested.GetBinder(contextMock.Object);

            Assert.NotNull(result);
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanCreateBinderWrongType()
        {
            var contextMock = new Mock<ModelBinderProviderContext>();
            var modelMetadataMock = new Mock<ModelMetadata>(ModelMetadataIdentity.ForType(typeof(int)));

            contextMock.Setup(x => x.Metadata).Returns(modelMetadataMock.Object);

            var tested = new PolymorphicModelBinderProvider<object>();
            var result = tested.GetBinder(contextMock.Object);

            Assert.Null(result);
        }

    }

}

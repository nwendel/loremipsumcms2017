﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Primitives;
using Moq;
using Xunit;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding.Binders;

namespace LoremIpsumCMS.Web.Tests.Infrastructure.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class PolymorphicModelBinderTests
    {

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNotModelBindNonPost()
        {
            var providerContextMock = new Mock<ModelBinderProviderContext>();
            var bindingContextMock = new Mock<ModelBindingContext>();
            var httpContextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();

            bindingContextMock.Setup(x => x.HttpContext).Returns(httpContextMock.Object);
            httpContextMock.Setup(x => x.Request).Returns(requestMock.Object);
            requestMock.Setup(x => x.Method).Returns("GET");

            var tested = new PolymorphicModelBinder(providerContextMock.Object);

            tested.BindModelAsync(bindingContextMock.Object);

            // TODO: Some assert here?
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public void CanNotModelBindPostNoTypeIdentifier()
        {
            var providerContextMock = new Mock<ModelBinderProviderContext>();
            var bindingContextMock = new Mock<ModelBindingContext>();
            var httpContextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var formCollection = new FormCollection(new Dictionary<string, StringValues>());

            bindingContextMock.Setup(x => x.HttpContext).Returns(httpContextMock.Object);
            httpContextMock.Setup(x => x.Request).Returns(requestMock.Object);
            requestMock.Setup(x => x.Method).Returns("POST");
            requestMock.Setup(x => x.Form).Returns(formCollection);

            var tested = new PolymorphicModelBinder(providerContextMock.Object);

            tested.BindModelAsync(bindingContextMock.Object);

            // TODO: Some assert here?
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact]
        public async void CanNotModelBindPostNullTypeIdentifierValue()
        {
            var providerContextMock = new Mock<ModelBinderProviderContext>();
            var bindingContextMock = new Mock<ModelBindingContext>();
            var httpContextMock = new Mock<HttpContext>();
            var requestMock = new Mock<HttpRequest>();
            var formCollection = new FormCollection(new Dictionary<string, StringValues> { { "Asdf.TypeIdentifier", new StringValues() } });

            bindingContextMock.Setup(x => x.HttpContext).Returns(httpContextMock.Object);
            bindingContextMock.Setup(x => x.ModelName).Returns("Asdf");
            httpContextMock.Setup(x => x.Request).Returns(requestMock.Object);
            requestMock.Setup(x => x.Method).Returns("POST");
            requestMock.Setup(x => x.Form).Returns(formCollection);

            var tested = new PolymorphicModelBinder(providerContextMock.Object);

            await Assert.ThrowsAsync<LoremIpsumException>(() => tested.BindModelAsync(bindingContextMock.Object));
        }

    }

}

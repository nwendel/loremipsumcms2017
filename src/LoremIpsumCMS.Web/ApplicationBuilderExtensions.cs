﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Web.Infrastructure.Initialization;
using LoremIpsumCMS.Web.Infrastructure.Modules;

namespace LoremIpsumCMS.Web
{

    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationBuilderExtensions
    {

        #region Use Lorem Ipsum

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static IApplicationBuilder UseLoremIpsum(this IApplicationBuilder self)
        {
            ConfigureModules(self);
            Initialize(self);
            return self;
        }

        #endregion

        #region Configure Modules

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        private static void ConfigureModules(IApplicationBuilder applicationBuilder)
        {
            var serviceProvider = applicationBuilder.ApplicationServices;
            var modules = serviceProvider.GetServices<IWebModule>();
            foreach (var module in modules.OfType<IWebModule>())
            {
                module.Configure(applicationBuilder);
            }
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        private static void Initialize(IApplicationBuilder applicationBuilder)
        {
            var serviceProvider = applicationBuilder.ApplicationServices;
            var initializerService = serviceProvider.GetRequiredService<IInitializerService>();
            //var documentSessionHolder = serviceProvider.GetRequiredService<IDocumentSessionHolder>();

            //using(new DocumentSessionScope(documentSessionHolder))
            //{
            initializerService.ExecuteInitializers();
            initializerService.ExecuteApplicationBuilderInitializers(applicationBuilder);

            //documentSessionHolder.Session.SaveChanges();
            //}
        }

        #endregion

    }

}

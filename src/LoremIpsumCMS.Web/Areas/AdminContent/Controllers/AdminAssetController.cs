﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminAsset;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [AuthorizeResource(Policy = nameof(Permissions.ManageSiteAssets))]
    [Area(AreaNames.AdminContent)]
    [LoremIpsumAdminRoute("assets")]
    public class AdminAssetController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminAssetController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IMapper _mapper;
        private readonly ICommandSender _commandSender;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="mapper"></param>
        /// <param name="commandSender"></param>
        public AdminAssetController(
            IDocumentSession documentSession,
            IMapper mapper,
            ICommandSender commandSender)
        {
            _documentSession = documentSession;
            _mapper = mapper;
            _commandSender = commandSender;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Index(SiteReference siteReference)
        {
            var assets = _documentSession.Query<Asset>()
                .Customize(o => o.WaitForNonStaleResults())
                .Where(x => x.SiteId == siteReference.SiteId)
                .OrderBy(x => x.Path)
                .ToList();

            var viewModel = new IndexViewModel
            {
                Assets = assets.MapTo<IndexAssetViewModel>(_mapper)
            };
            return View(viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        [HttpGet("create")]
        public IActionResult Create(SiteReference siteReference)
        {
            var viewModel = new CreateViewModel();
            return View(viewModel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidCreate(SiteReference siteReference, CreateViewModel viewModel)
        {
            return View("Create", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult Create(SiteReference siteReference, CreateViewModel viewModel)
        {
            var fileBytes = viewModel.File.ToByteArray();
            var command = new CreateAssetCommand
            {
                SiteId = siteReference.SiteId,
                Path = viewModel.Path,
                Bytes = fileBytes,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public IActionResult Delete(SiteReference siteReference, DeleteViewModel viewModel)
        {
            var asset = _documentSession.LoadBy<Asset>(x => x.SiteIdAndPath(siteReference.SiteId, viewModel.Path));

            var command = new DeleteAssetCommand
            {
                Id = asset.Id
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

    }

}

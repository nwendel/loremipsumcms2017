﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Raven.Client.Documents.Session;
using Raven.Client.Documents.Linq;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPage;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [AuthorizeResource(Policy = nameof(Permissions.ManageSitePages))]
    [Area(AreaNames.AdminContent)]
    [LoremIpsumAdminRoute("pages")]
    public class AdminPageController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminPageController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IMapper _mapper;
        private readonly IContentTypeService _contentTypeService;
        private readonly ICommandSender _commandSender;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="mapper"></param>
        /// <param name="contentTypeService"></param>
        /// <param name="commandSender"></param>
        public AdminPageController(
            IDocumentSession documentSession,
            IMapper mapper,
            IContentTypeService contentTypeService,
            ICommandSender commandSender)
        {
            _documentSession = documentSession;
            _mapper = mapper;
            _contentTypeService = contentTypeService;
            _commandSender = commandSender;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        public ValidationResult ValidateIndex(SiteReference siteReference)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Index(SiteReference siteReference)
        {
            var pageDataTypes = _contentTypeService
                .FindPageDataTypes()
                .OrderBy(x => x.Name)
                .ToList();

            var pages = _documentSession.Query<PageOverview>()
                .Customize(o => o.WaitForNonStaleResults())
                .Where(x => x.SiteId == siteReference.SiteId)
                .OrderBy(x => x.Path)
                .ToList();

            var viewModel = new IndexViewModel
            {
                PageTypes = pageDataTypes.MapTo<IndexPageTypeViewModel>(_mapper),
                Pages = pages.MapTo<IndexPageViewModel>(_mapper)
            };
            return View(viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="pageTypeNameSlug"></param>
        /// <returns></returns>
        public ValidationResult ValidateCreate(SiteReference siteReference, string pageTypeNameSlug)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            var pageDataType = _contentTypeService.GetPageDataTypeByNameSlug(pageTypeNameSlug);
            if (pageDataType == null)
            {
                return ValidationResult.NotFound;
            }

            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="pageTypeNameSlug"></param>
        /// <returns></returns>
        [HttpGet("create")]
        public IActionResult Create(SiteReference siteReference, string pageTypeNameSlug)
        {
            var pageDataType = _contentTypeService.GetPageDataTypeByNameSlug(pageTypeNameSlug);
            var pageData = pageDataType.Create();
            var extensionPropertyTypes = _contentTypeService.FindPageExtensionPropertyTypes();
            var extensionProperties = extensionPropertyTypes
                .Select(x => x.Create())
                .ToArray();

            var viewModel = new CreateViewModel
            {
                Data = pageData,
                ExtensionProperties = extensionProperties
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateCreate(SiteReference siteReference, CreateViewModel viewModel)
        {
            if(siteReference == null)
            {
                return ValidationResult.BadRequest;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidCreate(SiteReference siteReference, CreateViewModel viewModel)
        {
            return View(nameof(Create), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult Create(SiteReference siteReference, CreateViewModel viewModel)
        {
            var command = new CreatePageCommand
            {
                SiteId = siteReference.SiteId,
                Path = viewModel.Path,
                Title = viewModel.Title,
                MetaKeywords = viewModel.MetaKeywords,
                MetaDescription = viewModel.MetaDescription,
                Data = viewModel.Data,
                ExtensionProperties = viewModel.ExtensionProperties,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Edit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(SiteReference siteReference, string path)
        {
            if(siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            var found = _documentSession.ExistsBy<Page>(x => x.SiteIdAndPath(siteReference.SiteId, path));
            if(!found)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        [HttpGet("edit")]
        public IActionResult Edit(SiteReference siteReference, string path)
        {
            var page = _documentSession.LoadBy<Page>(x => x.SiteIdAndPath(siteReference.SiteId, path));
            var extensionPropertyTypes = _contentTypeService.FindPageExtensionPropertyTypes();
            var addExtensionPropertyTypes = extensionPropertyTypes
                .Where(x => !x.Type.In(page.ExtensionProperties.Select(p => p.GetType())))
                .ToList();
            var addExtensionProperties = addExtensionPropertyTypes
                .Select(x => x.Create())
                .ToArray();
            var extensionProperties = page.ExtensionProperties
                .Concat(addExtensionProperties)
                .ToArray();

            var viewModel = new EditViewModel
            {
                OldPath = page.Path,
                Path = page.Path,
                Title = page.Title,
                MetaKeywords = page.MetaKeywords,
                MetaDescription = page.MetaDescription,
                Data = page.Data,
                ExtensionProperties = extensionProperties
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(SiteReference siteReference, EditViewModel viewModel)
        {
            if (siteReference == null)
            {
                return ValidationResult.BadRequest;
            }

            var exists = _documentSession.ExistsBy<Page>(x => x.SiteIdAndPath(siteReference.SiteId, viewModel.OldPath));
            if(!exists)
            {
                return ValidationResult.BadRequest;
            }

            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidEdit(SiteReference siteReference, EditViewModel viewModel)
        {
            return View("Edit", viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public IActionResult Edit(SiteReference siteReference, EditViewModel viewModel)
        {
            var page = _documentSession.LoadBy<Page>(x => x.SiteIdAndPath(siteReference.SiteId, viewModel.OldPath));

            var command = new UpdatePageCommand
            {
                Id = page.Id,
                Path = viewModel.Path,
                Title = viewModel.Title,
                MetaKeywords = viewModel.MetaKeywords,
                MetaDescription = viewModel.MetaDescription,
                Data = viewModel.Data,
                ExtensionProperties = viewModel.ExtensionProperties,
                UpdatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateDelete(SiteReference siteReference, DeleteViewModel viewModel)
        {
            if (siteReference == null)
            {
                return ValidationResult.BadRequest;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public IActionResult Delete(SiteReference siteReference, DeleteViewModel viewModel)
        {
            var pageOverview = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath(siteReference.SiteId, viewModel.Path));

            var command = new DeletePageCommand
            {
                Id = pageOverview.PageId
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

    }

}

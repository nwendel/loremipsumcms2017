﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Raven.Client.Documents.Session;
using Raven.Client.Documents.Linq;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPartial;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [AuthorizeResource(Policy = nameof(Permissions.ManageSitePartials))]
    [Area(AreaNames.AdminContent)]
    [LoremIpsumAdminRoute("partials")]
    public class AdminPartialController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminPartialController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IMapper _mapper;
        private readonly IContentTypeService _contentTypeService;
        private readonly ICommandSender _commandSender;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="mapper"></param>
        /// <param name="contentTypeService"></param>
        /// <param name="commandSender"></param>
        public AdminPartialController(
            IDocumentSession documentSession,
            IMapper mapper,
            IContentTypeService contentTypeService,
            ICommandSender commandSender)
        {
            _documentSession = documentSession;
            _mapper = mapper;
            _contentTypeService = contentTypeService;
            _commandSender = commandSender;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        public ValidationResult ValidateIndex(SiteReference siteReference)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Index(SiteReference siteReference)
        {
            var partialDataTypes = _contentTypeService
                .FindPartialDataTypes()
                .OrderBy(x => x.Name)
                .ToList();
            var pages = _documentSession.Query<PartialOverview>()
                .Customize(o => o.WaitForNonStaleResults())
                .Where(x => x.SiteId == siteReference.SiteId)
                .OrderBy(x => x.Name)
                .ToList();

            var viewModel = new IndexViewModel
            {
                PartialTypes = partialDataTypes.MapTo<IndexPartialTypeViewModel>(_mapper),
                Partials = pages.MapTo<IndexPartialViewModel>(_mapper)
            };
            return View(viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="partialTypeNameSlug"></param>
        /// <returns></returns>
        public ValidationResult ValidateCreate(SiteReference siteReference, string partialTypeNameSlug)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            var partialDataType = _contentTypeService.GetPartialDataTypeByNameSlug(partialTypeNameSlug);
            if (partialDataType == null)
            {
                return ValidationResult.NotFound;
            }

            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="partialTypeNameSlug"></param>
        /// <returns></returns>
        [HttpGet("create")]
        public IActionResult Create(SiteReference siteReference, string partialTypeNameSlug)
        {
            var partialDataType = _contentTypeService.GetPartialDataTypeByNameSlug(partialTypeNameSlug);
            var partialData = partialDataType.Create();
            var extensionPropertyTypes = _contentTypeService.FindPartialExtensionPropertyTypes();
            var extensionProperties = extensionPropertyTypes
                .Select(x => x.Create())
                .ToArray();

            var viewModel = new CreateViewModel
            {
                Data = partialData,
                ExtensionProperties = extensionProperties
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateCreate(SiteReference siteReference, CreateViewModel viewModel)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidCreate(SiteReference siteReference, CreateViewModel viewModel)
        {
            return View(nameof(Create), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public IActionResult Create(SiteReference siteReference, CreateViewModel viewModel)
        {
            var command = new CreatePartialCommand
            {
                SiteId = siteReference.SiteId,
                Name = viewModel.Name,
                Data = viewModel.Data,
                ExtensionProperties = viewModel.ExtensionProperties,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Edit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(SiteReference siteReference, string nameSlug)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            var found = _documentSession.ExistsBy<Partial>(x => x.SiteIdAndNameSlug(siteReference.SiteId, nameSlug));
            if (!found)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        [HttpGet("edit")]
        public ActionResult Edit(SiteReference siteReference, string nameSlug)
        {
            var partial = _documentSession.LoadBy<Partial>(x => x.SiteIdAndNameSlug(siteReference.SiteId, nameSlug));
            var extensionPropertyTypes = _contentTypeService.FindPartialExtensionPropertyTypes();
            var addExtensionPropertyTypes = extensionPropertyTypes
                .Where(x => !x.Type.In(partial.ExtensionProperties.Select(p => p.GetType())))
                .ToList();
            var addExtensionProperties = addExtensionPropertyTypes
                .Select(x => x.Create())
                .ToArray();
            var extensionProperties = partial.ExtensionProperties
                .Concat(addExtensionProperties)
                .ToArray();

            var viewModel = new EditViewModel
            {
                OldNameSlug = partial.NameSlug,
                Name = partial.Name,
                Data = partial.Data,
                ExtensionProperties = extensionProperties
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(SiteReference siteReference, EditViewModel viewModel)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidEdit(SiteReference siteReference, EditViewModel viewModel)
        {
            return View(nameof(Edit), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("edit")]
        public ActionResult Edit(SiteReference siteReference, EditViewModel viewModel)
        {
            var partialOverview = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug(siteReference.SiteId, viewModel.OldNameSlug));

            var command = new UpdatePartialCommand
            {
                Id = partialOverview.PartialId,
                Name = viewModel.Name,
                Data = viewModel.Data,
                ExtensionProperties = viewModel.ExtensionProperties,
                UpdatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public ValidationResult ValidateDelete(SiteReference siteReference, DeleteViewModel viewModel)
        {
            if (siteReference == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("delete")]
        public IActionResult Delete(SiteReference siteReference, DeleteViewModel viewModel)
        {
            var partialOverview = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug(siteReference.SiteId, viewModel.NameSlug));

            var command = new DeletePartialCommand
            {
                Id = partialOverview.PartialId
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

    }

}

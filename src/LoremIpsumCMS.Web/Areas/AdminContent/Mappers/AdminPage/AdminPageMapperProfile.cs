﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPage;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Mappers.AdminPage
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminPageMapperProfile : Profile
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AdminPageMapperProfile()
        {
            CreateMap<PageOverview, IndexPageViewModel>()
                .ForMember(d => d.Type, o => o.ResolveUsing<PageOverviewTypeValueResolver>())
                .ForMember(d => d.CreatedAt, o => o.MapFrom(s => s.CreatedAt.ToRelativeString()))
                .ForMember(d => d.LastUpdatedAt, o => o.MapFrom(s => s.LastUpdatedAt.ToRelativeString()));

            CreateMap(typeof(ContentTypeInfo<>), typeof(IndexPageTypeViewModel));
        }

        #endregion

    }

}

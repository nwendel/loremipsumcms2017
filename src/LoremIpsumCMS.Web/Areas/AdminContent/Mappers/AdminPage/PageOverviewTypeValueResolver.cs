﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPage;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Mappers.AdminPage
{

    /// <summary>
    /// 
    /// </summary>
    public class PageOverviewTypeValueResolver : IValueResolver<PageOverview, IndexPageViewModel, string>
    {

        #region Dependencies

        private readonly IContentTypeService _contentTypeService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentTypeService"></param>
        public PageOverviewTypeValueResolver(IContentTypeService contentTypeService)
        {
            _contentTypeService = contentTypeService;
        }

        #endregion

        #region Resolve

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string Resolve(PageOverview source, IndexPageViewModel destination, string destMember, ResolutionContext context)
        {
            var pageDataType = _contentTypeService.GetPageDataTypeByTypeFullName(source.DataTypeFullName);
            return pageDataType.Name;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPage;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Validators.AdminPage
{

    /// <summary>
    /// 
    /// </summary>
    public class EditViewModelValidator : AbstractCreateOrEditViewModelValidator<EditViewModel>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IHostAccessor _hostAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public EditViewModelValidator(
            IDocumentSession documentSession,
            IHostAccessor hostAccessor) : base()
        {
            _documentSession = documentSession;
            _hostAccessor = hostAccessor;

            ValidateUsing(ValidatePathUnique);
        }

        #endregion

        #region Validate Name Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidatePathUnique(IValidationContext<EditViewModel> context)
        {
            var pagePath = context.Instance.Path;
            if (string.IsNullOrWhiteSpace(pagePath))
            {
                yield break;
            }

            var hostname = _hostAccessor.CurrentHost.Host;
            var siteOverview = _documentSession.LoadBy<SiteOverview>(x => x.Hostname(hostname));
            var siteId = siteOverview.SiteId;
            var exists = _documentSession.ExistsBy<PageOverview>(x => x.SiteIdAndPath(siteId, pagePath));
            if (exists)
            {
                var oldPagePath = context.Instance.OldPath;
                var oldPageOverview = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath(siteId, oldPagePath));
                var existingPageOverview = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath(siteId, pagePath));
                if (oldPageOverview.PageId != existingPageOverview.PageId)
                {
                    yield return new ValidationMessage<CreateViewModel>(x => x.Path, "Path must be unique");
                }
            }
        }

        #endregion

    }

}

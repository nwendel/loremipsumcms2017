﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Validators.ValueValidators;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPartial;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Validators.AdminPartial
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractCreateOrEditViewModelValidator<T> : AbstractValidator<T>
        where T : CreateViewModel
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected AbstractCreateOrEditViewModelValidator()
        {
            RuleFor(x => x.Name)
                .NotNullOrWhiteSpace()
                .AbsolutePath();
            RuleFor(x => x.Data)
                .NotNull()
                .PolymorphicValidate();
            RuleFor(x => x.ExtensionProperties)
                .NotNull()
                .All(x => x
                    .NotNull()
                    .PolymorphicValidate());
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPartial;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Infrastructure;

namespace LoremIpsumCMS.Web.Areas.AdminContent.Validators.AdminPartial
{

    /// <summary>
    /// 
    /// </summary>
    public class EditViewModelValidator : AbstractCreateOrEditViewModelValidator<EditViewModel>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IHostAccessor _hostAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public EditViewModelValidator(
            IDocumentSession documentSession,
            IHostAccessor hostAccessor) : base()
        {
            _documentSession = documentSession;
            _hostAccessor = hostAccessor;

            ValidateUsing(ValidateNameUnique);
        }

        #endregion

        #region Validate Name Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateNameUnique(IValidationContext<EditViewModel> context)
        {
            var partialName = context.Instance.Name;
            if (string.IsNullOrWhiteSpace(partialName))
            {
                yield break;
            }

            var hostname = _hostAccessor.CurrentHost.Host;
            var siteOverview = _documentSession.LoadBy<SiteOverview>(x => x.Hostname(hostname));
            var siteId = siteOverview.SiteId;
            var partialNameSlug = partialName.Slugify();
            var exists = _documentSession.ExistsBy<PartialOverview>(x => x.SiteIdAndNameSlug(siteId, partialNameSlug));
            if (exists)
            {
                var oldPartialNameSlug = context.Instance.OldNameSlug;
                var oldPartialOverview = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug(siteId, oldPartialNameSlug));
                var existingPageOverview = _documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug(siteId, partialNameSlug));
                if (oldPartialOverview.PartialId != existingPageOverview.PartialId)
                {
                    yield return new ValidationMessage<CreateViewModel>(x => x.Name, "Name must be unique");
                }
            }
        }

        #endregion

    }

}

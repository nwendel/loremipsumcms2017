﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ViewModels;

namespace LoremIpsumCMS.Web.Areas.AdminContent.ViewModels.AdminPartial
{

    /// <summary>
    /// 
    /// </summary>
    public class CreateViewModel : AbstractViewModel
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AbstractPartialData Data { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public AbstractPartialExtensionProperties[] ExtensionProperties { get; set; } = new AbstractPartialExtensionProperties[0];

        #endregion

    }

}

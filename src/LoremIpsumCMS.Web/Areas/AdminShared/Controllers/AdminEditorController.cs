﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminShared.ViewModels.AdminEditor;

namespace LoremIpsumCMS.Web.Areas.AdminShared.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(AreaNames.AdminShared)]
    public class AdminEditorController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminEditorController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public AdminEditorController(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region For

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="modelState"></param>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult For(string name, object value, ModelStateDictionary modelState)
        {
            // TODO: Rewrite

            foreach(var item in modelState)
            {
                if (!item.Key.StartsWith(name))
                {
                    continue;
                }
                foreach (var error in item.Value.Errors)
                {
                    ViewData.ModelState.AddModelError(item.Key, error.ErrorMessage);
                }
            }

            var viewModel = new ForViewModel
            {
                Name = name,
                Value = value
            };
            return PartialView(viewModel);
        }

        #endregion

        #region Content Area

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contentArea"></param>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult ContentArea(string name, ContentArea contentArea)
        {
            // TODO: Only supports partials as of now

            contentArea = contentArea ?? new ContentArea();
            var partialIds = contentArea.Items
                .OfType<PartialContentAreaItem>()
                .Select(x => x.PartialId)
                .ToArray();
            var partialOverviews = _documentSession.LoadBy<PartialOverview>(x => x.PartialIds(partialIds));
            var items = partialOverviews
                .Select(x =>  new ContentAreaItemViewModel { Name = x.Name })
                .ToList();
            items.Insert(0, new ContentAreaItemViewModel { IsDeleted = true });

            ViewData.TemplateInfo.HtmlFieldPrefix = name;
            var viewModel = new ContentAreaViewModel
            {
                Name = name,
                Items = items.ToArray()
            };
            return PartialView(viewModel);
        }

        #endregion

    }

}

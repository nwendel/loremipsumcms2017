﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using LoremIpsumCMS.Web.Areas.AdminShared.Controllers;
using LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering;

namespace LoremIpsumCMS.Web.Areas.AdminShared
{

    /// <summary>
    /// 
    /// </summary>
    public static class HtmlHelperAdminEditorForExtensions
    {

        #region Admin Editor For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="self"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IHtmlContent AdminEditorFor<TModel, TProperty>(this IHtmlHelper<TModel> self, Expression<Func<TModel, TProperty>> expression)
        {
            var name = ExpressionHelper.GetExpressionText(expression);
            var getValue = expression.Compile();
            var model = self.ViewData.Model;
            var value = getValue(model);

            // TODO: Do I need to do something like this?
            // var name = self.ViewData.TemplateInfo.GetFullHtmlFieldName(name);

            // TODO: I also seems to lose the model errors with this code, needs to be handled

            var viewComponentHelper = self.GetViewComponentHelper();
            return viewComponentHelper.Action(nameof(AdminEditorController.For), AdminEditorController.Name, new { Area = AreaNames.AdminShared, Name = name, Value = value, ModelState = self.ViewData.ModelState });
        }

        #endregion

    }

}
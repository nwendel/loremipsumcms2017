﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminShell.Controllers
{

    // TODO: Which authorize policy to user here?

    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [Area(AreaNames.AdminShell)]
    [LoremIpsumAdminRoute]
    public class AdminDashboardController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminDashboardController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Details

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Details()
        {
            return View();
        }

        #endregion

    }

}

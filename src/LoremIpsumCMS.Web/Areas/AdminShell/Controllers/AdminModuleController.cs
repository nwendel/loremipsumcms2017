﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Modules;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminModule;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Authorize(Policy = nameof(Permissions.ManageModules))]
    [Area(AreaNames.AdminShell)]
    [LoremIpsumAdminRoute("modules")]
    public class AdminModuleController : Controller
    {

        #region Constants

        public static readonly string Name = nameof(AdminModuleController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Fields

        private readonly IEnumerable<IModule> _modules;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modules"></param>
        /// <param name="mapper"></param>
        public AdminModuleController(
            IEnumerable<IModule> modules,
            IMapper mapper)
        {
            _modules = modules;
            _mapper = mapper;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Index()
        {
            var viewModel = _modules
                .MapTo<IndexModuleViewModel>(_mapper)
                .OrderBy(x => x.Name)
                .ToList();

            return View(viewModel);
        }

        #endregion

    }

}

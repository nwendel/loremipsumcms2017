﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminOptions;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Authorize(Policy = nameof(Permissions.ManageOptions))]
    [Area(AreaNames.AdminShell)]
    [LoremIpsumAdminRoute]
    public class AdminOptionsController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminOptionsController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public AdminOptionsController(IOptions<LoremIpsumOptions> options)
        {
            _options = options;
        }

        #endregion

        #region Details

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("options")]
        public IActionResult Details()
        {
            // TODO: More here, just some stuff as an example for now

            var optionValues = new List<OptionValueViewModel>
            {
                new OptionValueViewModel {Name = "Database Name", Value = _options.Value.RavenDatabase},
                new OptionValueViewModel {Name = "Database Url", Value = _options.Value.RavenUrl},
                new OptionValueViewModel {Name = "Ignore Page Content for Paths with Extensions", Value = string.Join("; ", _options.Value.IgnorePageRouteForExtensions)}
            };

            var viewModel = new DetailsViewModel
            {
                OptionsValues = optionValues
                    .OrderBy(x => x.Name)
                    .ToList()
            };

            return View(viewModel);
        }

        #endregion

    }

}

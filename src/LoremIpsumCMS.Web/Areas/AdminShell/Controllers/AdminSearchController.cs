﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminSearch;
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [Area(AreaNames.AdminShell)]
    [LoremIpsumAdminRoute]
    public class AdminSearchController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminSearchController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Search

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult Search()
        {
            return PartialView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("search")]
        public IActionResult Search(SearchViewModel viewModel)
        {
            return null;
        }

        #endregion

    }

}

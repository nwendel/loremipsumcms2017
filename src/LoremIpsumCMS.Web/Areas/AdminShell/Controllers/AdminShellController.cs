﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Web.Areas.AdminContent.Controllers;
using LoremIpsumCMS.Web.Areas.AdminShell.Services;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminShell;
using LoremIpsumCMS.Web.Areas.AdminSite.Controllers;
using LoremIpsumCMS.Web.Areas.AdminUser.Controllers;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Mvc;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminShell.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(AreaNames.AdminShell)]
    [LoremIpsumAdminRoute]
    public class AdminShellController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminShellController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IEnumerable<IAdminMenuItemProvider> _moduleMenuItemProviders;
        private readonly IAuthorizationService _authorizationService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="moduleMenuItemProviders"></param>
        /// <param name="authorizationService"></param>
        public AdminShellController(
            IHttpContextAccessor httpContextAccessor,
            IEnumerable<IAdminMenuItemProvider> moduleMenuItemProviders, 
            IAuthorizationService authorizationService)
        {
            _httpContextAccessor = httpContextAccessor;
            _moduleMenuItemProviders = moduleMenuItemProviders;
            _authorizationService = authorizationService;
        }

        #endregion

        #region Main Menu

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public async Task<IActionResult> MainMenu()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            var user = httpContext.User;
            // TODO: Need to get site id here...
            var siteId = string.Empty;


            var viewModel = await CreateMainMenuModel(user, siteId);

            var menuItems = viewModel.SelectMany(x => x.Items);
            var path = httpContext.Request.Path.ToString();
            UpdateIsActive(menuItems, path);

            return PartialView("_MainMenu", viewModel);
        }

        #endregion

        #region Create Main Menu Model

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task<MenuViewModel[]> CreateMainMenuModel(ClaimsPrincipal user, string siteId)
        {
            var result = new[]
            {
                new MenuViewModel
                {
                    Title = "Content",
                    Items = new[]
                    {
                        new MenuItemViewModel
                        {
                            IconClass = "ti-home",
                            Title = "Dashboard",
                            Url = Url.Action(nameof(AdminDashboardController.Details), AdminDashboardController.Name, new {Area = AreaNames.AdminShell})
                        },
                        new MenuItemViewModel
                        {
                            IconClass = "ti-layout",
                            Title = "Site",
                            // TODO: Something else here?
                            Url = Url.Action(nameof(AdminSiteController.Edit), AdminSiteController.Name, new {Area = AreaNames.AdminSite})
                        },
                        new MenuItemViewModel
                        {
                            IconClass = "ti-layers-alt",
                            Title = "Pages",
                            Url = Url.Action(nameof(AdminPageController.Index), AdminPageController.Name, new {Area = AreaNames.AdminContent}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageSitePages, siteId)
                        },
                        new MenuItemViewModel
                        {
                            IconClass = "ti-layers-alt",
                            Title = "Partials",
                            Url = Url.Action(nameof(AdminPartialController.Index), AdminPartialController.Name, new {Area = AreaNames.AdminContent}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageSitePartials, siteId)
                        },
                        new MenuItemViewModel
                        {
                            IconClass = "ti-files",
                            Title = "Assets",
                            Url = Url.Action(nameof(AdminAssetController.Index), AdminAssetController.Name, new {Area = AreaNames.AdminContent}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageSiteAssets, siteId)
                        }
                    }
                },
                new MenuViewModel
                {
                    Title = "Modules",
                    Items = new[]
                    {
                        new MenuItemViewModel
                        {
                            Title = "Manage",
                            Url = Url.Action(nameof(AdminModuleController.Index), AdminModuleController.Name, new {Area = AreaNames.AdminShell}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageModules)
                        }
                    }.Concat(CreateModelMenuItems(user, siteId)).ToArray()
                },
                new MenuViewModel
                {
                    Title = "Manage",
                    Items = new[]
                    {
                        new MenuItemViewModel
                        {
                            Title = "Sites",
                            Url = Url.Action(nameof(AdminSiteController.Index), AdminSiteController.Name, new {Area = AreaNames.AdminSite}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageSites)
                        },
                        new MenuItemViewModel
                        {
                            Title = "Users",
                            Url = Url.Action(nameof(AdminUserController.Index), AdminUserController.Name, new {Area = AreaNames.AdminUser}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageUsers)
                        },
                        new MenuItemViewModel
                        {
                            Title = "Options",
                            Url = Url.Action(nameof(AdminOptionsController.Details), AdminOptionsController.Name, new {Area = AreaNames.AdminShell}),
                            IsVisible = await GetIsVisible(user, Permissions.ManageOptions)
                        }
                    }
                },
                new MenuViewModel
                {
                    Title = "Documentation",
                    Items = new[]
                    {
                        new MenuItemViewModel
                        {
                            Title = "Authors",
                            Url = "#"
                        },
                        new MenuItemViewModel
                        {
                            Title = "Developers",
                            Url = "#"
                        },
                        new MenuItemViewModel
                        {
                            Title = "About",
                            Url = "#"
                        }
                    }
                }

            };
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private MenuItemViewModel[] CreateModelMenuItems(ClaimsPrincipal user, string siteId)
        {
            // TODO: Avoid using Result and instead async / await, but how?
            var menuItems = _moduleMenuItemProviders
                .Select(x => x.GetMenuItem(user, siteId).Result)
                .OrderBy(x => x.Title)
                .ToArray();
            return menuItems;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuItems"></param>
        /// <param name="path"></param>
        private void UpdateIsActive(IEnumerable<MenuItemViewModel> menuItems, string path)
        {
            if (menuItems == null)
            {
                return;
            }

            var allMenuItems = menuItems
                .Flatten(x => x.Children)
                .OrderByDescending(x => (x.Url ?? "").Length);

            foreach (var menuItem in allMenuItems)
            {
                if (path.StartsWith(menuItem.Url ?? "#"))
                {
                    menuItem.IsActive = true;
                    break;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <returns></returns>
        private async Task<bool> GetIsVisible(ClaimsPrincipal user, Permission permission)
        {
            var authorizationResult = await _authorizationService.AuthorizeAsync(user, permission.Name);
            return authorizationResult == AuthorizationResult.Success();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        private async Task<bool> GetIsVisible(ClaimsPrincipal user, Permission permission, string siteId)
        {
            var authorizationResult = await _authorizationService.AuthorizeAsync(user, siteId, permission.Name);
            return authorizationResult == AuthorizationResult.Success();
        }

        #endregion

    }

}

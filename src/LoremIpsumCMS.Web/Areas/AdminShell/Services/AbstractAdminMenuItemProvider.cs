﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminShell;
using LoremIpsumCMS.Web.Infrastructure.Authorization;

namespace LoremIpsumCMS.Web.Areas.AdminShell.Services
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractAdminMenuItemProvider : IAdminMenuItemProvider
    {

        #region Dependencies

        private readonly IAuthorizationService _authorizationService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="urlHelper"></param>
        /// <param name="authorizationService"></param>
        protected AbstractAdminMenuItemProvider(
            IUrlHelper urlHelper,
            IAuthorizationService authorizationService)
        {
            Url = urlHelper;
            _authorizationService = authorizationService;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        protected IUrlHelper Url { get; }

        #endregion

        #region Get Menu Item

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public abstract Task<MenuItemViewModel> GetMenuItem(ClaimsPrincipal user, string siteId);

        #endregion

        #region Get Is Visible

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <returns></returns>
        protected async Task<bool> GetIsVisible(ClaimsPrincipal user, Permission permission)
        {
            var authorizationResult = await _authorizationService.AuthorizeAsync(user, permission.Name);
            return authorizationResult == AuthorizationResult.Success();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="permission"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        protected async Task<bool> GetIsVisible(ClaimsPrincipal user, Permission permission, string siteId)
        {
            var authorizationResult = await _authorizationService.AuthorizeAsync(user, siteId, permission.Name);
            return authorizationResult == AuthorizationResult.Success();
        }

        #endregion

    }

}

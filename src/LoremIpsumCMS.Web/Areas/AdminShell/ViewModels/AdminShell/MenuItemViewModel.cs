﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ViewModels;

namespace LoremIpsumCMS.Web.Areas.AdminShell.ViewModels.AdminShell
{

    /// <summary>
    /// 
    /// </summary>
    public class MenuItemViewModel : AbstractViewModel
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string IconClass { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsOpen
        {
            get
            {
                var isOpen = Children != null && Children.Any(x => x.IsActive);
                return isOpen;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasChildren => Children != null && Children.Any();

        /// <summary>
        /// 
        /// </summary>
        public MenuItemViewModel[] Children { get; set; }

        #endregion

    }

}

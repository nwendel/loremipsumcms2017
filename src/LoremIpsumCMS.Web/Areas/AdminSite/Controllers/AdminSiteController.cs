﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using AutoMapper;
using Raven.Client.Documents.Session;
using Raven.Client.Documents.Linq;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Authorization;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.Infrastructure.Validation;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Area(AreaNames.AdminSite)]
    [LoremIpsumAdminRoute("site")]
    public class AdminSiteController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminSiteController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IHostAccessor _hostAccessor;
        private readonly IContentTypeService _contentTypeService;
        private readonly IMapper _mapper;
        private readonly ICommandSender _commandSender;
        private readonly IDocumentSession _documentSession;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOptions<LoremIpsumOptions> _options;
        private readonly IOptions<AuthorizationOptions> _authorizationOptions;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        /// <param name="contentTypeService"></param>
        /// <param name="mapper"></param>
        /// <param name="commandSender"></param>
        /// <param name="documentSession"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="options"></param>
        /// <param name="authorizationOptions"></param>
        public AdminSiteController(
            IHostAccessor hostAccessor,
            IContentTypeService contentTypeService,
            IMapper mapper,
            ICommandSender commandSender,
            IDocumentSession documentSession,
            IHttpContextAccessor httpContextAccessor,
            IOptions<LoremIpsumOptions> options,
            IOptions<AuthorizationOptions> authorizationOptions)
        {
            _hostAccessor = hostAccessor;
            _contentTypeService = contentTypeService;
            _mapper = mapper;
            _commandSender = commandSender;
            _documentSession = documentSession;
            _httpContextAccessor = httpContextAccessor;
            _options = options;
            _authorizationOptions = authorizationOptions;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = nameof(Permissions.ManageSites))]
        [HttpGet("")]
        public IActionResult Index()
        {
            var sites = _documentSession.Query<Site>()
                .Customize(o => o.WaitForNonStaleResults())
                .ToList();

            var viewModel = new IndexViewModel
            {
                Sites = sites
                    .MapTo<IndexSiteViewModel>(_mapper)
                    .ToArray()
            };

            return View(viewModel);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        [Authorize(Policy = nameof(Permissions.ManageSites))]
        [HttpGet("create")]
        public IActionResult Create()
        {
            var host = _hostAccessor.CurrentHost.Host;
            var extensionPropertyTypes = _contentTypeService.FindSiteExtensionPropertyTypes();
            var extensionProperties = extensionPropertyTypes
                .OrderBy(x => x.Name)
                .Select(x => x.Create())
                .ToArray();

            var sitePermissions = _authorizationOptions.Value.GetResourcePermissions(nameof(Site));
            var sitePermissionNames = sitePermissions
                .Select(x => x.Name)
                .ToArray();

            var viewModel = new CreateViewModel
            {
                Hosts = new[]
                {
                    new SiteHostViewModel
                    {
                        Name = host,
                        IsAdmin = true
                    }
                },
                ExtensionProperties = extensionProperties,
                SitePermissionNames = sitePermissionNames,
                UserPermissions = new[]
                {
                    new UserPermissionsViewModel { UserName = "Niklas", UserNameSlug = "niklas"}
                }
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidCreate(CreateViewModel viewModel)
        {
            return View(nameof(Create), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Authorize(Policy = nameof(Permissions.ManageSites))]
        [HttpPost("create")]
        public IActionResult Create(CreateViewModel viewModel)
        {
            var hosts = viewModel.Hosts
                .Where(x => !x.IsDeleted)
                .MapTo<SiteHost>(_mapper)
                .ToList();

            var command = new CreateSiteCommand
            {
                Name = viewModel.Name,
                Hosts = hosts,
                ExtensionProperties = viewModel.ExtensionProperties,
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Edit

        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        public ValidationResult ValidateEdit(Site site)
        {
            if (site == null)
            {
                return ValidationResult.NotFound;
            }
            return ValidationResult.Success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        [AuthorizeResource(Policy = nameof(Permissions.ManageSite))]
        [HttpGet("edit")]
        public IActionResult Edit(Site site)
        {
            var extensionPropertyTypes = _contentTypeService.FindSiteExtensionPropertyTypes();
            var addExtensionPropertyTypes = extensionPropertyTypes
                .Where(x => !x.Type.In(site.ExtensionProperties.Select(p => p.GetType())))
                .ToList();
            var addExtensionProperties = addExtensionPropertyTypes
                .Select(x => x.Create())
                .ToArray();
            var extensionProperties = site.ExtensionProperties
                .Concat(addExtensionProperties)
                .OrderBy(x => x.GetName())
                .ToArray();

            var sitePermissions = _authorizationOptions.Value.GetResourcePermissions(nameof(Site));
            var sitePermissionNames = sitePermissions
                .Select(x => x.Name)
                .ToArray();

            var viewModel = new EditViewModel
            {
                Name = site.Name,
                OldNameSlug = site.NameSlug,
                Hosts = site.Hosts
                    .MapTo<SiteHostViewModel>(_mapper)
                    .ToArray(),
                ExtensionProperties = extensionProperties,
                SitePermissionNames = sitePermissionNames,
                UserPermissions = new[]
                {
                    new UserPermissionsViewModel { UserName = "Niklas", UserNameSlug = "niklas"}
                }
            };
            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidEdit(EditViewModel viewModel)
        {
            return View(nameof(Edit), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [AuthorizeResource(Policy = nameof(Permissions.ManageSite))]
        [HttpPost("edit")]
        public IActionResult Edit(EditViewModel viewModel)
        {
            var site = _documentSession.LoadBy<Site>(x => x.NameSlug(viewModel.OldNameSlug));
            var hosts = viewModel.Hosts
                .Where(x => !x.IsDeleted)
                .MapTo<SiteHost>(_mapper)
                .ToList();

            var command = new UpdateSiteCommand
            {
                Id = site.Id,
                Name = viewModel.Name,
                Hosts = hosts,
                ExtensionProperties = viewModel.ExtensionProperties,
                UpdatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [Authorize(Policy = nameof(Permissions.ManageSites))]
        [HttpPost("delete")]
        public IActionResult Delete(DeleteViewModel viewModel)
        {
            var site = _documentSession.LoadBy<Site>(x => x.NameSlug(viewModel.NameSlug));

            var command = new DeleteSiteCommand
            {
                Id = site.Id
            };
            _commandSender.Send(command);

            return RedirectToAction(nameof(Index));
        }

        #endregion

        #region Navigate To Site

        // TODO: Authorize this somehow?

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        [HttpGet("navigate-to-site/{nameSlug}")]
        public IActionResult NavigateToSite(string nameSlug)
        {
            var site = _documentSession.LoadBy<Site>(x => x.NameSlug(nameSlug));
            var adminHost = site.Hosts.First(x => x.IsAdmin);

            var request = _httpContextAccessor.HttpContext.Request;
            var path = Url.Action(nameof(Index));

            if (_hostAccessor.HasOverride)
            {
                var queryParameterName = _options.Value.HostnameOverrideQueryParameterName;
                return Redirect($"{request.Scheme}://{request.Host}{path}?{queryParameterName}={adminHost.Name}");
            }

            return Redirect($"{request.Scheme}://{adminHost.Name}{path}");
        }

        #endregion

    }

}

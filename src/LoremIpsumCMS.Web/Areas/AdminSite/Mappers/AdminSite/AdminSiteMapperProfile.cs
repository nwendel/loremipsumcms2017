﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Mappers.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public class AdminSiteMapperProfile : Profile
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AdminSiteMapperProfile()
        {
            CreateMap<Site, IndexSiteViewModel>()
                .ForMember(d => d.IsCurrent, o => o.ResolveUsing<IndexSiteViewModelIsCurrentValueResolver>())
                .ForMember(d => d.CreatedAt, o => o.MapFrom(s => s.CreatedAt.ToRelativeString()))
                .ForMember(d => d.LastUpdatedAt, o => o.MapFrom(s => s.LastUpdatedAt.ToRelativeString()));

            CreateMap<SiteHostViewModel, SiteHost>();

            CreateMap<SiteHost, SiteHostViewModel>()
                .ForMember(d => d.IsDeleted, o => o.Ignore());
        }

        #endregion

    }

}

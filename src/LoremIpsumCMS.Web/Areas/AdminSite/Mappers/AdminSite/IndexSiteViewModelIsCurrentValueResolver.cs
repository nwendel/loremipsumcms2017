﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using AutoMapper;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Mappers.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public class IndexSiteViewModelIsCurrentValueResolver : IValueResolver<Site, IndexSiteViewModel, bool>
    {

        #region Dependencies

        private readonly IHostAccessor _hostAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        public IndexSiteViewModelIsCurrentValueResolver(IHostAccessor hostAccessor)
        {
            _hostAccessor = hostAccessor;
        }

        #endregion

        #region Resolve

        /// <summary>
        /// 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="destination"></param>
        /// <param name="destMember"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public bool Resolve(Site source, IndexSiteViewModel destination, bool destMember, ResolutionContext context)
        {
            var host = _hostAccessor.CurrentHost.Host;
            var isCurrent = source.Hosts.Any(x => x.Name == host);
            return isCurrent;
        }

        #endregion

    }

}

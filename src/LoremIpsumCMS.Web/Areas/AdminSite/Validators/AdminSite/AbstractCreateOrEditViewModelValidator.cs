﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Validators.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractCreateOrEditViewModelValidator<T> : AbstractValidator<T>
        where T : CreateViewModel
    {

        #region Dependencies

        /// <summary>
        /// 
        /// </summary>
        private readonly IHostAccessor _hostAccessor;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        protected AbstractCreateOrEditViewModelValidator(IHostAccessor hostAccessor)
        {
            _hostAccessor = hostAccessor;

            RuleFor(x => x.Name)
                .NotNull()
                .WithMessage("Name is required");
            RuleFor(x => x.Hosts)
                .NotNull();
            RuleFor(x => x.ExtensionProperties)
                .All(x => x.PolymorphicValidate());

            ValidateUsing(ValidateHosts);
        }

        #endregion

        #region Validate Hostnames

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateHosts(IValidationContext<T> context)
        {
            var hosts = context.Instance.Hosts;
            if(hosts == null)
            {
                yield break;
            }
            hosts = hosts
                .Where(x => !x.IsDeleted)
                .ToArray();

            if (hosts.None())
            {
                yield return new ValidationMessage<T>(x => x.Hosts, "Must have at least one host");
            }
            if (hosts.Any(x => x.Name == null))
            {
                yield return new ValidationMessage<T>(x => x.Hosts, "Hostname is required");
            }

            // Check that current request host is defined as admin
            var currentRequestHostIsAdmin = false;
            var currentRequestHostname = _hostAccessor.CurrentHost.Host;
            foreach (var host in hosts)
            {
                if (host.Name == null)
                {
                    continue;
                }
                if (host.Name == currentRequestHostname && host.IsAdmin)
                {
                    currentRequestHostIsAdmin = true;
                }
            }
            if (!currentRequestHostIsAdmin)
            {
                yield return new ValidationMessage<T>(x => x.Hosts, "Current host is not defined as admin");
            }
        }

        #endregion

    }

}

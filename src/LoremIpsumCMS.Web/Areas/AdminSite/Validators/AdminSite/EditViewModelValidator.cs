﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminSite.ViewModels.AdminSite;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Areas.AdminSite.Validators.AdminSite
{

    /// <summary>
    /// 
    /// </summary>
    public class EditViewModelValidator : AbstractCreateOrEditViewModelValidator<EditViewModel>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        /// <param name="documentSession"></param>
        public EditViewModelValidator(
            IHostAccessor hostAccessor,
            IDocumentSession documentSession)
            : base(hostAccessor)
        {
            _documentSession = documentSession;

            ValidateUsing(ValidateNameUnique);
            ValidateUsing(ValidateHostsUnique);
        }

        #endregion

        #region Validate Name Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateNameUnique(IValidationContext<EditViewModel> context)
        {
            var siteName = context.Instance.Name;
            if (string.IsNullOrWhiteSpace(siteName))
            {
                yield break;
            }

            var siteNameSlug = siteName.Slugify();
            var exists = _documentSession.ExistsBy<Site>(x => x.NameSlug(siteNameSlug));
            if (exists)
            {
                var oldSite = _documentSession.LoadBy<Site>(x => x.NameSlug(context.Instance.OldNameSlug));
                var existingSite = _documentSession.LoadBy<Site>(x => x.NameSlug(siteNameSlug));
                if (oldSite.Id != existingSite.Id)
                {
                    yield return new ValidationMessage("Name", "Name must be unique");
                }
            }
        }

        #endregion

        #region Validate Hosts Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateHostsUnique(IValidationContext<EditViewModel> context)
        {
            var site = _documentSession.LoadBy<Site>(x => x.NameSlug(context.Instance.OldNameSlug));
            var hosts = context.Instance.Hosts
                .Where(x => !x.IsDeleted)
                .ToList();
            foreach (var host in hosts)
            {
                if(host.Name == null)
                {
                    continue;
                }
                var otherHost = _documentSession.LoadBy<Host>(x => x.Name(host.Name));
                if (otherHost != null && otherHost.SiteId != site.Id)
                {
                    yield return new ValidationMessage("Hosts", "Hostnames must be unique");
                }
            }

            yield break;
        }

        #endregion

    }

}

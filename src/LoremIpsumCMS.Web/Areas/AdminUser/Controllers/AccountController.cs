﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminShell.Controllers;
using LoremIpsumCMS.Web.Areas.AdminUser.ViewModels.Account;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Identity;
using LoremIpsumCMS.Web.Infrastructure.Mvc.Filters;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminUser.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [AllowAnonymous]
    [KeepTempData(_returnUrl)]
    [Area(AreaNames.AdminUser)]
    [LoremIpsumLoginRoute]
    public class AccountController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AccountController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly ISignInManager _signInManager;
        private readonly ICommandSender _commandSender;
        private readonly IDocumentSession _documentSession;
        private readonly IPasswordHasher _passwordHasher;

        #endregion

        #region Fields

        private const string _returnUrl = "ReturnUrl";

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="signInManager"></param>
        /// <param name="commandSender"></param>
        /// <param name="documentSession"></param>
        /// <param name="passwordHasher"></param>
        public AccountController(
            ISignInManager signInManager,
            ICommandSender commandSender,
            IDocumentSession documentSession,
            IPasswordHasher passwordHasher)
        {
            _signInManager = signInManager;
            _commandSender = commandSender;
            _documentSession = documentSession;
            _passwordHasher = passwordHasher;
        }

        #endregion

        #region Login

        /// <summary>
        /// 
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Login(string returnUrl)
        {
            if(returnUrl != null)
            {
                TempData[_returnUrl] = returnUrl;
                return RedirectToAction(nameof(Login));
            }

            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            var externalAuthenticationSchemes = await _signInManager.GetExternalAuthenticationSchemesAsync();
            var viewModel = new LoginViewModel
            {
                ExternalAuthentcationSchemes = externalAuthenticationSchemes,
            };

            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public async Task<IActionResult> InvalidLogin(LoginViewModel viewModel)
        {
            var externalAuthenticationSchemes = await _signInManager.GetExternalAuthenticationSchemesAsync();
            viewModel.ExternalAuthentcationSchemes = externalAuthenticationSchemes;

            return View(nameof(Login), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            var result = await _signInManager.PasswordSignInAsync(viewModel.DisplayName, viewModel.Password);
            if(result.Succeeded)
            {
                var user = _documentSession.LoadBy<IdentityUser>(x => x.DisplayNameSlug(viewModel.DisplayName.Slugify()));
                var command = new LoginIdentityUserCommand
                {
                    Id = user.Id,
                    At = DateTime.Now
                };
                _commandSender.Send(command);

                var returnUrl = GetReturnUrl();
                return Redirect(returnUrl);
            }

            // User/Password problem, which should not be checked by validators. Show login again
            var externalAuthenticationSchemes = await _signInManager.GetExternalAuthenticationSchemesAsync();
            viewModel.ExternalAuthentcationSchemes = externalAuthenticationSchemes;

            return View(nameof(Login), viewModel);

            //ViewData["ReturnUrl"] = returnUrl;
            //if (ModelState.IsValid)
            //{
            //    // This doesn't count login failures towards account lockout
            //    // To enable password failures to trigger account lockout, set lockoutOnFailure: true
            //    var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
            //    if (result.Succeeded)
            //    {
            //        _logger.LogInformation("User logged in.");
            //        return RedirectToLocal(returnUrl);
            //    }
            //    if (result.RequiresTwoFactor)
            //    {
            //        return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
            //    }
            //    if (result.IsLockedOut)
            //    {
            //        _logger.LogWarning("User account locked out.");
            //        return RedirectToAction(nameof(Lockout));
            //    }
            //    else
            //    {
            //        ModelState.AddModelError(string.Empty, "Invalid login attempt.");
            //        return View(model);
            //    }
            //}

            //// If we got this far, something failed, redisplay form
            //return View(model);

        }

        #endregion

        #region Register

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("register")]
        public IActionResult Register()
        {
            var viewModel = new RegisterViewModel();

            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidRegister(RegisterViewModel viewModel)
        {
            return View(nameof(Register), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("register")]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {
            var passwordHash = _passwordHasher.HashPassword(viewModel.Password);
            var command = new CreateIdentityUserCommand
            {
                DisplayName = viewModel.DisplayName,
                FullName = viewModel.Name,
                PasswordHash = passwordHash,
                Email = viewModel.Email,
                PermissionNames = viewModel.DisplayName == "Administrator"
                    ? new [] { Permissions.GlobalAdministrator.Name }
                    : new string[0],
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            var displayNameSlug = viewModel.DisplayName.Slugify();
            var identityUser = _documentSession.LoadBy<IdentityUser>(x => x.DisplayNameSlug(displayNameSlug));
            await _signInManager.SignInAsync(identityUser, new AuthenticationProperties());

            if (identityUser.DisplayName == "Administrator")
            {
                // TODO: Update CreatedByUserId
            }


            var returnUrl = GetReturnUrl();
            return Redirect(returnUrl);
        }

        #endregion

        #region Login External

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("external")]
        public IActionResult LoginExternal(LoginExternalViewModel viewModel)
        {
            var providerName = viewModel.ProviderName;
            var returnUrl = GetReturnUrl();
            var redirectUri = Url.Action(nameof(LoginExternalCallback), new {ReturnUrl = returnUrl});
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(providerName, redirectUri);

            return Challenge(properties, providerName);
        }

        #endregion

        #region Login External Callback

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpGet("external/callback")]
        public async Task<IActionResult> LoginExternalCallback(LoginExternalCallbackViewModel viewModel)
        {
            //if (remoteError != null)
            //{
            //    ModelState.AddModelError(string.Empty, $"Error from external provider: {remoteError}");
            //    return View(nameof(Login));
            //}
            var externalLoginInfo = await _signInManager.GetExternalLoginInfoAsync();
            if (externalLoginInfo == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Try to sign in with external provider, if login exists
            var result = await _signInManager.ExternalLoginSignInAsync(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey, false, false);
            if (result.Succeeded)
            {
                //    _logger.LogInformation(5, "User logged in with {Name} provider.", info.LoginProvider);
                var user = _documentSession.LoadBy<IdentityUser>(x => x.LoginProviderAndKey(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey));
                var command = new LoginIdentityUserCommand
                {
                    Id = user.Id,
                    At = DateTime.Now
                };
                _commandSender.Send(command);

                return Redirect(viewModel.ReturnUrl);
            }
            //if (result.RequiresTwoFactor)
            //{
            //    return RedirectToAction(nameof(SendCode), new { ReturnUrl = returnUrl });
            //}
            //if (result.IsLockedOut)
            //{
            //    return View("Lockout");
            //}
            //else
            //{

            // No account, create
            return RedirectToAction(nameof(RegisterExternal));
        }

        #endregion

        #region Register External

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("register/external")]
        public async Task<IActionResult> RegisterExternal()
        {
            var externalLoginInfo = await _signInManager.GetExternalLoginInfoAsync();
            var viewModel = new RegisterExternalViewModel
            {
                Name = externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Name),
                Email = externalLoginInfo.Principal.FindFirstValue(ClaimTypes.Email),
            };

            return View(viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        public IActionResult InvalidRegisterExternal(RegisterExternalViewModel viewModel)
        {
            return View(nameof(RegisterExternal), viewModel);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost("register/external")]
        public async Task<IActionResult> RegisterExternal(RegisterExternalViewModel viewModel)
        {
            var externalLoginInfo = await _signInManager.GetExternalLoginInfoAsync();

            var command = new CreateIdentityUserExternalAccountCommand
            {
                DisplayName = viewModel.DisplayName,
                FullName = viewModel.Name,
                Email = viewModel.Email,
                LoginProvider = externalLoginInfo.LoginProvider,
                LoginProviderKey = externalLoginInfo.ProviderKey,
                PermissionNames = viewModel.DisplayName == "Administrator"
                    ? new[] { Permissions.GlobalAdministrator.Name }
                    : new string[0],
                CreatedAt = DateTime.Now
            };
            _commandSender.Send(command);

            var identityUser = _documentSession.LoadBy<IdentityUser>(x => x.LoginProviderAndKey(externalLoginInfo.LoginProvider, externalLoginInfo.ProviderKey));
            await _signInManager.SignInAsync(identityUser, new AuthenticationProperties());

            if (identityUser.DisplayName == "Administrator")
            {
                // TODO: Update CreatedByUserId
            }

            var returnUrl = GetReturnUrl();
            return Redirect(returnUrl);
        }

        #endregion

        #region Get Referrer Or Default

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private string GetReturnUrl()
        {
            if(TempData.ContainsKey(_returnUrl))
            {
                var returnUrl = (string)TempData[_returnUrl];
                TempData.Remove(_returnUrl);
                return returnUrl;
            }
            return Url.Action(nameof(AdminDashboardController.Details), AdminDashboardController.Name, new { Area = AreaNames.AdminShell });
        }

        #endregion

    }

}

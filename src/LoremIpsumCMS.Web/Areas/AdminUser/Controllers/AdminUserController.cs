﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.AutoMapper;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Areas.AdminUser.ViewModels.AdminUser;
using LoremIpsumCMS.Web.Authorization;
using LoremIpsumCMS.Web.Mvc;

namespace LoremIpsumCMS.Web.Areas.AdminUser.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    [Authorize(Policy = nameof(Permissions.ManageUsers))]
    [Area(AreaNames.AdminUser)]
    [LoremIpsumAdminRoute]
    public class AdminUserController : Controller
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public static readonly string Name = nameof(AdminUserController).TrimEndsWith(nameof(Controller));

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="mapper"></param>
        public AdminUserController(
            IDocumentSession documentSession,
            IMapper mapper)
        {
            _documentSession = documentSession;
            _mapper = mapper;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("user")]
        public IActionResult Index()
        {
            var identityUsers = _documentSession.Query<IdentityUser>()
                .ToList();

            var viewModel = new IndexViewModel
            {
                Users = identityUsers.MapTo<IndexUserViewModel>(_mapper)
            };

            return View(viewModel);
        }

        #endregion

    }

}

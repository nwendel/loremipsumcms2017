﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Areas.AdminUser.ViewModels.Account;

namespace LoremIpsumCMS.Web.Areas.AdminUser.Validators.Account
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractRegisterViewModelValidator<T> : AbstractValidator<T>
        where T : RegisterExternalViewModel
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        protected AbstractRegisterViewModelValidator(IDocumentSession documentSession)
        {
            _documentSession = documentSession;

            RuleFor(x => x.DisplayName)
                .NotNullOrWhiteSpace();

            ValidateUsing(ValidateDisplayNameUnique);
        }

        #endregion

        #region Validate Display Name Unique

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateDisplayNameUnique(IValidationContext<RegisterExternalViewModel> context)
        {
            var displayName = context.Instance.DisplayName;
            if (string.IsNullOrWhiteSpace(displayName))
            {
                yield break;
            }

            var displayNameSlug = displayName.Slugify();
            var exists = _documentSession.ExistsBy<IdentityUser>(x => x.DisplayNameSlug(displayNameSlug));
            if (exists)
            {
                yield return new ValidationMessage<RegisterExternalViewModel>(x => x.DisplayName, "Display name must be unique");
            }
        }

        #endregion

    }

}

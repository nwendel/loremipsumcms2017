﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Areas.AdminUser.ViewModels.Account;

namespace LoremIpsumCMS.Web.Areas.AdminUser.Validators.Account
{

    /// <summary>
    /// 
    /// </summary>
    public class RegisterViewModelValidator : AbstractRegisterViewModelValidator<RegisterViewModel>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public RegisterViewModelValidator(IDocumentSession documentSession)
            : base(documentSession)
        {
            RuleFor(x => x.Name)
                .NotNullOrWhiteSpace();

            // TODO: Check valid email
            RuleFor(x => x.Email)
                .NotNullOrWhiteSpace();

            // TODO: Check valid password
        }

        #endregion

    }

}

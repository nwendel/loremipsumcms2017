﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Infrastructure.Authorization;

namespace LoremIpsumCMS.Web.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class Permissions
    {

        #region Fields

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission GlobalAdministrator = new Permission(nameof(GlobalAdministrator));

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSites = new Permission(nameof(ManageSites), new[] { GlobalAdministrator });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageModules = new Permission(nameof(ManageModules), new[] { GlobalAdministrator });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageUsers = new Permission(nameof(ManageUsers), new[] { GlobalAdministrator });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageOptions = new Permission(nameof(ManageOptions), new[] { GlobalAdministrator });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSite = new ResourcePermission(nameof(ManageSite), nameof(Site), new[] { ManageSites });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSiteContents = new ResourcePermission(nameof(ManageSiteContents), nameof(Site), new[] { ManageSite });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSitePages = new ResourcePermission(nameof(ManageSitePages), nameof(Site), new[] { ManageSiteContents });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSitePartials = new ResourcePermission(nameof(ManageSitePartials), nameof(Site), new[] { ManageSiteContents });

        /// <summary>
        /// 
        /// </summary>
        public static readonly Permission ManageSiteAssets = new ResourcePermission(nameof(ManageSiteAssets), nameof(Site), new[] { ManageSiteContents });
        
        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        private Permissions()
        {
        }

        #endregion

    }

}

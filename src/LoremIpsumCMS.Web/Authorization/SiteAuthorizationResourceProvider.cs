﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Infrastructure.Authorization;

namespace LoremIpsumCMS.Web.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteAuthorizationResourceProvider : AbstractAuthorizationResourceProvider
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public SiteAuthorizationResourceProvider() : base(nameof(Site))
        {
        }

        #endregion

        #region Find Resource Id From Action Arguments

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionArguments"></param>
        /// <returns></returns>
        public override string GetResourceIdFromActionArguments(IDictionary<string, object> actionArguments)
        {
            if (TryGetActionArgument<SiteReference>(actionArguments, "siteReference", out var siteReference))
            {
                return siteReference.SiteId;
            }
            if (TryGetActionArgument<Site>(actionArguments, "site", out var site))
            {
                return site.Id;
            }

            return null;
        }

        #endregion

    }

}

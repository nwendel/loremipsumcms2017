﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.Web.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AssetController : Controller
    {

        #region Dependencies

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOptions<LoremIpsumOptions> _options;
        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="options"></param>
        /// <param name="documentSession"></param>
        public AssetController(
            IHttpContextAccessor httpContextAccessor,
            IOptions<LoremIpsumOptions> options,
            IDocumentSession documentSession)
        {
            _httpContextAccessor = httpContextAccessor;
            _options = options;
            _documentSession = documentSession;
        }

        #endregion

        #region Index

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteReference"></param>
        /// <returns></returns>
        public IActionResult Index(SiteReference siteReference)
        {
            TempData.Keep();

            var httpContext = _httpContextAccessor.HttpContext;
            var fullPath = httpContext.Request.Path.ToString();
            var path = fullPath.Substring(_options.Value.AssetRootPath.Length + 1);

            var asset = _documentSession.LoadBy<Asset>(x => x.SiteIdAndPath(siteReference.SiteId, path));
            var attachmentResult = _documentSession.Advanced.Attachments.Get(asset, nameof(Asset));
            var stream = attachmentResult.Stream;
            var response = File(stream, "application/octet-stream");
            return response;
        }

        #endregion

    }

}

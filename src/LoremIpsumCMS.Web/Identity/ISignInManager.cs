﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Web.Identity
{

    /// <summary>
    /// 
    /// </summary>
    public interface ISignInManager
    {

        #region Password Sign In Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task<SignInResult> PasswordSignInAsync(string displayName, string password);

        #endregion

        #region Get External Authentication Schemes Async

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<AuthenticationScheme>> GetExternalAuthenticationSchemesAsync();

        #endregion

        #region Configure External Authentication Properties

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="redirectUri"></param>
        /// <returns></returns>
        AuthenticationProperties ConfigureExternalAuthenticationProperties(string providerName, string redirectUri);

        #endregion

        #region Get External Login Info Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expectedXsrf"></param>
        /// <returns></returns>
        Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string expectedXsrf = null);

        #endregion

        #region External Login Sign In Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="providerKey"></param>
        /// <param name="isPersistent"></param>
        /// <param name="bypassTwoFactor"></param>
        /// <returns></returns>
        Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string providerKey, bool isPersistent, bool bypassTwoFactor);

        #endregion

        #region Sign In Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="authenticationProperties"></param>
        /// <param name="authenticationMethod"></param>
        /// <returns></returns>
        Task SignInAsync(IdentityUser user, AuthenticationProperties authenticationProperties, string authenticationMethod = null);

        #endregion

    }

}

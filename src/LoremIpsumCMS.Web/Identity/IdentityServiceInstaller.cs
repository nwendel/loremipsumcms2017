﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion

using System.Threading.Tasks;
using FluentRegistration;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Web.Identity
{

    /// <summary>
    /// 
    /// </summary>
    public class IdentityServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Register(r => r
                .FromThisAssembly()
                .Where(c => c.InThisNamespace())
                .WithServices.AllInterfaces());

            serviceCollection
                .AddAuthentication(o =>
                {
                    o.DefaultAuthenticateScheme = IdentityConstants.ApplicationScheme;
                    o.DefaultChallengeScheme = IdentityConstants.ApplicationScheme;
                    o.DefaultSignInScheme = IdentityConstants.ExternalScheme;
                })
                .AddCookie(IdentityConstants.ExternalScheme, o =>
                {
                    // TODO: Is LoginPath needed here?
                    o.LoginPath = "/admin/login";
                    o.Cookie.Name = IdentityConstants.ExternalScheme;
                })
                .AddCookie(IdentityConstants.ApplicationScheme, o =>
                {
                    o.LoginPath = "/admin/login";
                    o.Cookie.Name = IdentityConstants.ApplicationScheme;
                    // TODO: Also override login path?
                    o.Events.OnRedirectToAccessDenied = c =>
                    {
                        c.Response.StatusCode = StatusCodes.Status403Forbidden;
                        return Task.CompletedTask;
                    };
                });
        }

        #endregion

    }

}

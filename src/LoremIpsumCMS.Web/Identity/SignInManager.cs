﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Infrastructure.Authorization;

namespace LoremIpsumCMS.Web.Identity
{

    /// <summary>
    /// 
    /// </summary>
    public class SignInManager : ISignInManager
    {

        #region Constants

        private const string _loginProviderKey = "LoginProvider";

        #endregion

        #region Dependencies

        private readonly IAuthenticationSchemeProvider _authenticationSchemeProvider;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDocumentSession _documentSession;
        private readonly IPasswordHasher _passwordHasher;
        private readonly IEnumerable<IIdentityClaimProvider> _identityClaimProviders;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationSchemeProvider"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="documentSession"></param>
        /// <param name="passwordHasher"></param>
        public SignInManager(
            IAuthenticationSchemeProvider authenticationSchemeProvider,
            IHttpContextAccessor httpContextAccessor,
            IDocumentSession documentSession,
            IPasswordHasher passwordHasher,
            IEnumerable<IIdentityClaimProvider> identityClaimProviders)
        {
            _authenticationSchemeProvider = authenticationSchemeProvider;
            _httpContextAccessor = httpContextAccessor;
            _documentSession = documentSession;
            _passwordHasher = passwordHasher;
            _identityClaimProviders = identityClaimProviders;
        }

        #endregion

        #region Password Sign In Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public async Task<SignInResult> PasswordSignInAsync(string displayName, string password)
        {
            var displayNameSlug = displayName.Slugify();
            var identityUser = _documentSession.LoadBy<IdentityUser>(x => x.DisplayNameSlug(displayNameSlug));
            if (identityUser?.Account == null)
            {
                return SignInResult.Failed;
            }
            if (!_passwordHasher.VerifyHashedPassword(identityUser.Account.PasswordHash, password))
            {
                return SignInResult.Failed;
            }

            var httpContext = _httpContextAccessor.HttpContext;
            await httpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            var authenticationProperties = new AuthenticationProperties { IsPersistent = false };
            await SignInAsync(identityUser, authenticationProperties);
            return SignInResult.Success;
        }

        #endregion

        #region Get External Authentication Schemes Async

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AuthenticationScheme>> GetExternalAuthenticationSchemesAsync()
        {
            var schemes = await _authenticationSchemeProvider.GetAllSchemesAsync();
            var externalSchemes = schemes.Where(s => !string.IsNullOrEmpty(s.DisplayName));
            return externalSchemes;
        }

        #endregion

        #region Configure External Authentication Properties

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerName"></param>
        /// <param name="redirectUri"></param>
        /// <returns></returns>
        public AuthenticationProperties ConfigureExternalAuthenticationProperties(string providerName, string redirectUri)
        {
            var properties = new AuthenticationProperties
            {
                RedirectUri = redirectUri,
                Items = {{_loginProviderKey, providerName}}
            };
            return properties;
        }

        #endregion

        #region Get External Login Info Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="expectedXsrf"></param>
        /// <returns></returns>
        public async Task<ExternalLoginInfo> GetExternalLoginInfoAsync(string expectedXsrf = null)
        {
            var httpContext = _httpContextAccessor.HttpContext;
            var authenticateResult = await httpContext.AuthenticateAsync(IdentityConstants.ExternalScheme);

            var items = authenticateResult?.Properties?.Items;
            if(authenticateResult?.Principal == null || items == null || !items.ContainsKey(_loginProviderKey))
            {
                return null;
            }

            var providerKey = authenticateResult.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
            var provider = items[_loginProviderKey];
            if(providerKey == null | provider == null)
            {
                return null;
            }

            return new ExternalLoginInfo(authenticateResult.Principal, provider, providerKey, provider)
            {
                AuthenticationTokens = authenticateResult.Properties.GetTokens()
            };



            //var auth = await Context.AuthenticateAsync(IdentityConstants.ExternalScheme);
            //var items = auth?.Properties?.Items;
            //if (auth?.Principal == null || items == null || !items.ContainsKey(LoginProviderKey))
            //{
            //    return null;
            //}

            //if (expectedXsrf != null)
            //{
            //    if (!items.ContainsKey(XsrfKey))
            //    {
            //        return null;
            //    }
            //    var userId = items[XsrfKey] as string;
            //    if (userId != expectedXsrf)
            //    {
            //        return null;
            //    }
            //}

            //var providerKey = auth.Principal.FindFirstValue(ClaimTypes.NameIdentifier);
            //var provider = items[LoginProviderKey] as string;
            //if (providerKey == null || provider == null)
            //{
            //    return null;
            //}
            //// TODO: display name gone?.  Add [] indexer for Authproperties
            //return new ExternalLoginInfo(auth.Principal, provider, providerKey, provider)
            //{
            //    AuthenticationTokens = auth.Properties.GetTokens()
            //};
        }

        #endregion

        #region External Login Sign In Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="loginProviderKey"></param>
        /// <param name="isPersistent"></param>
        /// <param name="bypassTwoFactor"></param>
        /// <returns></returns>
        public async Task<SignInResult> ExternalLoginSignInAsync(string loginProvider, string loginProviderKey, bool isPersistent, bool bypassTwoFactor)
        {
            var identityUser = _documentSession.LoadBy<IdentityUser>(x => x.LoginProviderAndKey(loginProvider, loginProviderKey));
            if (identityUser == null)
            {
                return SignInResult.Failed;
            }

            //var user = await UserManager.FindByLoginAsync(loginProvider, providerKey);
            //if (user == null)
            //{
            //    return SignInResult.Failed;
            //}

            //var error = await PreSignInCheck(user);
            //if (error != null)
            //{
            //    return error;
            //}
            //return await SignInOrTwoFactorAsync(user, isPersistent, loginProvider, bypassTwoFactor);


            // Cleanup external cookie
            if (loginProvider != null)
            {
                var httpContext = _httpContextAccessor.HttpContext;
                await httpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            }

            var authenticationProperties = new AuthenticationProperties { IsPersistent = isPersistent };
            await SignInAsync(identityUser, authenticationProperties, loginProvider);
            return SignInResult.Success;
        }

        #endregion

        #region Sign In Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="authenticationProperties"></param>
        /// <param name="authenticationMethod"></param>
        /// <returns></returns>
        public async Task SignInAsync(IdentityUser user, AuthenticationProperties authenticationProperties, string authenticationMethod = null)
        {
            //var userPrincipal = await CreateUserPrincipalAsync(user);

            //var userId = await UserManager.GetUserIdAsync(user);
            //var userName = await UserManager.GetUserNameAsync(user);
            //var id = new ClaimsIdentity("Identity.Application", // REVIEW: Used to match Application scheme
            //    Options.ClaimsIdentity.UserNameClaimType,
            //    Options.ClaimsIdentity.RoleClaimType);
            //id.AddClaim(new Claim(Options.ClaimsIdentity.UserIdClaimType, userId));
            //id.AddClaim(new Claim(Options.ClaimsIdentity.UserNameClaimType, userName));
            //if (UserManager.SupportsUserSecurityStamp)
            //{
            //    id.AddClaim(new Claim(Options.ClaimsIdentity.SecurityStampClaimType,
            //        await UserManager.GetSecurityStampAsync(user)));
            //}
            //if (UserManager.SupportsUserClaim)
            //{
            //    id.AddClaims(await UserManager.GetClaimsAsync(user));
            //}
            //return id;

            var identity = new ClaimsIdentity(IdentityConstants.ApplicationScheme);
            identity.AddClaims(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.DisplayNameSlug),
                new Claim(ClaimTypes.Name, user.FullName),
                // TODO: Use constant
                new Claim("UserId", user.Id)
            });
            foreach (var permissionName in user.PermissionNames)
            {
                identity.AddClaim(new Claim(Permission.ClaimType, permissionName));
            }

            var resourceIdentityClaims = _identityClaimProviders.SelectMany(x => x.FindClaims(user.Id));
            identity.AddClaims(resourceIdentityClaims);
            // TODO: Load from separate document in database
            //foreach (var resourcePermission in user.ResourcePermissions)
            //{
            //    foreach (var name in resourcePermission.Names)
            //    {
            //        identity.AddClaim(new Claim(ResourcePermission.ClaimType, $"{resourcePermission.ResourceId}:{name}"));
            //    }
            //}

            var userPrincipal = new ClaimsPrincipal(identity);

            //// Review: should we guard against CreateUserPrincipal returning null?
            //if (authenticationMethod != null)
            //{
            //    userPrincipal.Identities.First().AddClaim(new Claim(ClaimTypes.AuthenticationMethod, authenticationMethod));
            //}

            var httpContext = _httpContextAccessor.HttpContext;
            await httpContext.SignInAsync(IdentityConstants.ApplicationScheme, userPrincipal, authenticationProperties ?? new AuthenticationProperties());
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractAuthorizationResourceProvider : IAuthorizationResourceProvider
    {

        #region Fields

        private readonly string _name;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        protected AbstractAuthorizationResourceProvider(string name)
        {
            _name = name;
        }

        #endregion

        #region Can Provide Resource

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool CanProvideResource(string name)
        {
            return _name == name;
        }

        #endregion

        #region Get Resource Id From Action Arguments

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionArguments"></param>
        /// <returns></returns>
        public abstract string GetResourceIdFromActionArguments(IDictionary<string, object> actionArguments);

        #endregion

        #region Try Get Action Argument

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TArgument"></typeparam>
        /// <param name="arguments"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected bool TryGetActionArgument<TArgument>(IDictionary<string, object> arguments, string name, out TArgument value)
        {
            if (arguments.ContainsKey(name) && arguments[name].GetType() == typeof(TArgument))
            {
                value = (TArgument)arguments[name];
                return true;
            }

            value = default(TArgument);
            return false;
        }

        #endregion

    }

}

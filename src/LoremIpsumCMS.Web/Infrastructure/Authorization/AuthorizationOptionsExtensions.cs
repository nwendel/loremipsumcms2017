﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Authorization;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public static class AuthorizationOptionsExtensions
    {

        #region Fields

        // TODO: Should I use an attached property for this instead?

        private static readonly IList<Permission> _allPermissions = new List<Permission>();

        #endregion

        #region Register Permission Policies For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        public static void RegisterPermissionPoliciesFor<T>(this AuthorizationOptions self)
        {
            var fieldInfos = typeof(T)
                .GetFields(BindingFlags.Public | BindingFlags.Static)
                .Where(x => typeof(Permission).IsAssignableFrom(x.FieldType))
                .ToList();
            foreach (var fieldInfo in fieldInfos)
            {
                var permission = (Permission)fieldInfo.GetValue(null);
                _allPermissions.Add(permission);
                self.AddPolicy(permission.Name, p => p.AddRequirements(new PermissionRequirement(permission)));
            }
        }

        #endregion

        #region Get Permissions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        // ReSharper disable once UnusedParameter.Global
        public static IEnumerable<Permission> GetPermissions(this AuthorizationOptions self)
        {
            var permissions = _allPermissions
                // ReSharper disable once RedundantEnumerableCastCall
                .OfType<Permission>()
                .ToList();
            return permissions;
        }

        #endregion

        #region Get Resource Permissions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="resourceName"></param>
        /// <returns></returns>
        // ReSharper disable once UnusedParameter.Global
        public static IEnumerable<ResourcePermission> GetResourcePermissions(this AuthorizationOptions self, string resourceName)
        {
            var permissions = _allPermissions
                .OfType<ResourcePermission>()
                .Where(x => x.ResourceName == resourceName)
                .ToList();
            return permissions;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class AuthorizeResourceAttribute : ActionFilterAttribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public AuthorizeResourceAttribute()
        {
            // TODO: Needs to run after ValidationActionFilterAttribute
            Order = int.MaxValue;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Policy { get; set; }

        #endregion

        #region On Action Execution Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="next"></param>
        /// <returns></returns>
        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var requestServices = context.HttpContext.RequestServices;
            var authorizationService = requestServices.GetRequiredService<IAuthorizationService>();

            if (!context.HttpContext.User.Identity.IsAuthenticated)
            {
                // TODO: Shouldn't be hardcoded
                context.Result = new RedirectResult("/admin/login");
                return;
            }

            var authorizationResult = await authorizationService.AuthorizeAsync(context.HttpContext.User, context.ActionArguments, Policy);
            if (!authorizationResult.Succeeded)
            {
                // TODO: Is this correct, shouldn't it use the authorization options?
                context.Result = new ForbidResult();
                return;
            }

            await base.OnActionExecutionAsync(context, next);
        }

        #endregion

    }

}

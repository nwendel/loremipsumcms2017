﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Linq;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class Permission
    {

        #region Constants

        /// <summary>
        /// 
        /// </summary>
        public const string ClaimType = nameof(Permission);

        #endregion

        #region Fields

        private IEnumerable<string> _allPermissionNames;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        public Permission(string name)
            : this(name, new List<Permission>())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="impliedBy"></param>
        public Permission(string name, IEnumerable<Permission> impliedBy)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            if (impliedBy == null)
            {
                throw new ArgumentNullException(nameof(impliedBy));
            }
            impliedBy = impliedBy.ToList();
            if (impliedBy.Any(x => x == null))
            {
                throw new ArgumentNullException(nameof(impliedBy));
            }

            Name = name;
            ImpliedBy = impliedBy;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<Permission> ImpliedBy { get; }

        #endregion

        #region Get All Permission Names

        /// <summary>
        /// 
        /// </summary>
        public virtual IEnumerable<string> GetAllPermissionNames()
        {
            if (_allPermissionNames == null)
            {
                var allPermissions = ImpliedBy
                    .Flatten(x => x.ImpliedBy)
                    .ToList();
                allPermissions.Insert(0, this);
                _allPermissionNames = allPermissions
                    .Select(x => x.Name)
                    .ToList();
            }
            return _allPermissionNames;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Raven.Client.Documents.Linq;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class PermissionRequirementHandler : AuthorizationHandler<PermissionRequirement>
    {

        #region Dependencies

        private readonly IResourceAuthorizationService _resourceAuthorizationService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceAuthorizationService"></param>
        public PermissionRequirementHandler(IResourceAuthorizationService resourceAuthorizationService)
        {
            _resourceAuthorizationService = resourceAuthorizationService;
        }

        #endregion

        #region Handle Requirment Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="requirement"></param>
        /// <returns></returns>
        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
        {
            if (requirement.Permission is ResourcePermission resourcePermission)
            {
                var success = await HandleResourcePermissionRequirementAsync(context, resourcePermission);
                if (success)
                {
                    context.Succeed(requirement);
                }
            }
            else
            {
                var success = await HandlePermissionRequirementAsync(context, requirement.Permission);
                if (success)
                {
                    context.Succeed(requirement);
                }
            }
        }

        #endregion

        #region Handle Permission Requirement Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="permission"></param>
        /// <returns></returns>
        private Task<bool> HandlePermissionRequirementAsync(AuthorizationHandlerContext context, Permission permission)
        {
            var claims = context.User.FindAll(x => x.Type == Permission.ClaimType);
            var allPermissionNames = permission.GetAllPermissionNames();
            var success = claims.Any(c => c.Value.In(allPermissionNames));

            return Task.FromResult(success);
        }

        #endregion

        #region Handle Resource Permission Requirement Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="resourcePermission"></param>
        /// <returns></returns>
        private Task<bool> HandleResourcePermissionRequirementAsync(AuthorizationHandlerContext context, ResourcePermission resourcePermission)
        {
            var allPermissionNames = resourcePermission.GetAllPermissionNames();
            var claims = context.User
                .FindAll(x => x.Type == Permission.ClaimType)
                .ToList();
            var success = claims.Any(c => c.Value.In(allPermissionNames));
            if (success)
            {
                return Task.FromResult(true);
            }

            var resourceName = resourcePermission.ResourceName;
            var resource = context.Resource;
            var resourceId = _resourceAuthorizationService.GetResourceIdFromResource(resourceName, resource);
            var allSameResourcePermissionNames = resourcePermission.GetAllSameResourcePermissionNames();
            var allSameResourceClaimValues = allSameResourcePermissionNames
                .Select(x => $"{resourceId}:{x}")
                .ToList();

            var resourceClaims = context.User
                .FindAll(x => x.Type == ResourcePermission.ClaimType)
                .ToList();
            var sameResourceSuccess = resourceClaims.Any(c => c.Value.In(allSameResourceClaimValues));
            if (sameResourceSuccess)
            {
                return Task.FromResult(true);
            }


            // TODO: Fallback to different resource...


            return Task.FromResult(false);
        }

        #endregion

    }

}

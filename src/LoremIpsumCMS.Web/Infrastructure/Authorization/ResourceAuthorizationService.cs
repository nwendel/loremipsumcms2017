﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class ResourceAuthorizationService : IResourceAuthorizationService
    {

        #region Dependencies

        private readonly IEnumerable<IAuthorizationResourceProvider> _authorizationResourceProviders;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authorizationResourceProviders"></param>
        public ResourceAuthorizationService(IEnumerable<IAuthorizationResourceProvider> authorizationResourceProviders)
        {
            _authorizationResourceProviders = authorizationResourceProviders;
        }

        #endregion

        #region Get Resource Id From Resource

        /// <summary>
        /// 
        /// </summary>
        /// <param name="resourceName"></param>
        /// <param name="resource"></param>
        /// <returns></returns>
        public string GetResourceIdFromResource(string resourceName, object resource)
        {
            if (string.IsNullOrEmpty(resourceName))
            {
                throw new ArgumentNullException(nameof(resourceName));
            }
            if (resource == null)
            {
                throw new ArgumentNullException(nameof(resource));
            }

            if (resource is string resoruceId)
            {
                // TODO: Also an assumption, which might be dangerous...
                return resoruceId;
            }
            if (resource is AbstractAggregate aggregate)
            {
                // TODO: Assume aggregate is of right type seems dangerous?
                return aggregate.Id;
            }
            if (resource is IDictionary<string, object> actionArguments)
            {
                var provider = _authorizationResourceProviders.Single(x => x.CanProvideResource(resourceName));
                var resourceId = provider.GetResourceIdFromActionArguments(actionArguments);
                return resourceId;
            }

            return null;
        }

        #endregion

    }

}

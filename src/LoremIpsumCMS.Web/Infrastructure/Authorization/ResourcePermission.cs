﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Linq;

namespace LoremIpsumCMS.Web.Infrastructure.Authorization
{

    /// <summary>
    /// 
    /// </summary>
    public class ResourcePermission : Permission
    {

        #region Constants

        // TODO: Is this how I want it with the new keyword here?

        /// <summary>
        /// 
        /// </summary>
        public new const string ClaimType = nameof(ResourcePermission);

        #endregion

        #region Fields

        private IEnumerable<string> _allSameResourcePermissionNames;
        private IEnumerable<ResourcePermission> _allOtherResourcePermissions;
        private IEnumerable<string> _allPermissionNames;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="resourceName"></param>
        public ResourcePermission(string name, string resourceName)
            : this(name, resourceName, new List<Permission>())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="resourceName"></param>
        /// <param name="impliedBy"></param>
        public ResourcePermission(string name, string resourceName, IEnumerable<Permission> impliedBy)
            : base(name, impliedBy)
        {
            ResourceName = resourceName;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string ResourceName { get; }

        #endregion

        #region Get All Same Resource Permission Names

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> GetAllSameResourcePermissionNames()
        {
            if (_allSameResourcePermissionNames == null)
            {
                var allSameResourcePermissions = ImpliedBy
                    .Flatten(x => x.ImpliedBy)
                    .OfType<ResourcePermission>()
                    .Where(x => x.ResourceName == ResourceName)
                    .ToList();
                allSameResourcePermissions.Insert(0, this);
                _allSameResourcePermissionNames = allSameResourcePermissions
                    .Select(x => x.Name)
                    .ToList();
            }
            return _allSameResourcePermissionNames;
        }

        #endregion

        #region Get All Other Resource Permissions

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ResourcePermission> GetAllOtherResourcePermissions()
        {
            if (_allOtherResourcePermissions == null)
            {
                var allOtherResourcePermissions = ImpliedBy
                .Flatten(x => x.ImpliedBy)
                .OfType<ResourcePermission>()
                .Where(x => x.ResourceName != ResourceName)
                .ToList();
                _allOtherResourcePermissions = allOtherResourcePermissions;
            }
            return _allOtherResourcePermissions;
        }

        #endregion

        #region Get All Permission Names

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override IEnumerable<string> GetAllPermissionNames()
        {
            if (_allPermissionNames == null)
            {
                var allPermissionNames = ImpliedBy
                .Flatten(x => x.ImpliedBy)
                .OfType<Permission>()
                .Select(x => x.Name)
                .ToList();
                _allPermissionNames = allPermissionNames;
            }
            return _allPermissionNames;
        }

        #endregion

    }

}

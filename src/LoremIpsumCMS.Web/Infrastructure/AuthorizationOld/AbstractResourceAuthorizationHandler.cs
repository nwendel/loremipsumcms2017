﻿//#region License
//// Copyright (c) Niklas Wendel 2017-2018
//// 
//// Licensed under the Apache License, Version 2.0 (the "License"); 
//// you may not use this file except in compliance with the License. 
//// You may obtain a copy of the License at 
//// 
//// http://www.apache.org/licenses/LICENSE-2.0 
//// 
//// Unless required by applicable law or agreed to in writing, software 
//// distributed under the License is distributed on an "AS IS" BASIS, 
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//// See the License for the specific language governing permissions and 
//// limitations under the License.
//#endregion
//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;

//namespace LoremIpsumCMS.Web.Infrastructure.Authorization
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    public abstract class AbstractResourceAuthorizationHandler<TRequirement, TResource> : AuthorizationHandler<TRequirement>
//        where TRequirement : AbstractResourceRequirement<TResource>
//    {

//        #region Dependencies

//        private readonly IServiceProvider _serviceProvider;

//        #endregion

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="serviceProvider"></param>
//        protected AbstractResourceAuthorizationHandler(IServiceProvider serviceProvider)
//        {
//            _serviceProvider = serviceProvider;
//        }

//        #endregion

//        #region Handle Requirement Async

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="context"></param>
//        /// <param name="requirement"></param>
//        /// <returns></returns>
//        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement)
//        {
//            var actionArguments = (IDictionary<string, object>) context.Resource;
//            var resource = requirement.GetResourceFromActionArguments(actionArguments, _serviceProvider);

//            if(resource == null)
//            {
//                // TODO: Is this correct?  
//                //       The validation should make this into a NotFound response instead?  
//                //       All not found resources are always allowed?
//                context.Succeed(requirement);
//                return;
//            }

//            await HandleResourceRequirementAsync(context, requirement, resource);
//        }

//        #endregion

//        #region Handle Resource Requirement Async

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="context"></param>
//        /// <param name="requirement"></param>
//        /// <param name="resource"></param>
//        /// <returns></returns>
//        protected abstract Task HandleResourceRequirementAsync(AuthorizationHandlerContext context, TRequirement requirement, TResource resource);

//        #endregion

//    }

//}

﻿//#region License
//// Copyright (c) Niklas Wendel 2017-2018
//// 
//// Licensed under the Apache License, Version 2.0 (the "License"); 
//// you may not use this file except in compliance with the License. 
//// You may obtain a copy of the License at 
//// 
//// http://www.apache.org/licenses/LICENSE-2.0 
//// 
//// Unless required by applicable law or agreed to in writing, software 
//// distributed under the License is distributed on an "AS IS" BASIS, 
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//// See the License for the specific language governing permissions and 
//// limitations under the License.
//#endregion
//using System;
//using System.Collections.Generic;
//using Microsoft.AspNetCore.Authorization;

//namespace LoremIpsumCMS.Web.Infrastructure.Authorization
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    public abstract class AbstractResourceRequirement : IAuthorizationRequirement
//    {

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        protected AbstractResourceRequirement(string name)
//        {
//            Name = name;
//        }

//        #endregion

//        #region Properties

//        /// <summary>
//        /// 
//        /// </summary>
//        public string Name { get; }

//        #endregion

//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    public abstract class AbstractResourceRequirement<TResource> : AbstractResourceRequirement
//    {

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        protected AbstractResourceRequirement(string name) 
//            : base(name)
//        {
//        }

//        #endregion

//        #region Get Resource From Action Arguments

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="arguments"></param>
//        /// <param name="serviceProvider"></param>
//        /// <returns></returns>
//        public abstract TResource GetResourceFromActionArguments(IDictionary<string, object> arguments, IServiceProvider serviceProvider);

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <typeparam name="TArgument"></typeparam>
//        /// <param name="arguments"></param>
//        /// <param name="name"></param>
//        /// <param name="value"></param>
//        /// <returns></returns>
//        protected bool TryGetActionArgument<TArgument>(IDictionary<string, object> arguments, string name, out TArgument value)
//        {
//            if (arguments.ContainsKey(name) && arguments[name].GetType() == typeof(TArgument))
//            {
//                value = (TArgument)arguments[name];
//                return true;
//            }

//            value = default(TArgument);
//            return false;
//        }
//        #endregion

//    }

//}

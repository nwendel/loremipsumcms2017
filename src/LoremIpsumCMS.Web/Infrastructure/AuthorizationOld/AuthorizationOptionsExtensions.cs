﻿//#region License
//// Copyright (c) Niklas Wendel 2017-2018
//// 
//// Licensed under the Apache License, Version 2.0 (the "License"); 
//// you may not use this file except in compliance with the License. 
//// You may obtain a copy of the License at 
//// 
//// http://www.apache.org/licenses/LICENSE-2.0 
//// 
//// Unless required by applicable law or agreed to in writing, software 
//// distributed under the License is distributed on an "AS IS" BASIS, 
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//// See the License for the specific language governing permissions and 
//// limitations under the License.
//#endregion
//using System.Linq;
//using System.Reflection;
//using Microsoft.AspNetCore.Authorization;

//namespace LoremIpsumCMS.Web.Infrastructure.Authorization
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    public static class AuthorizationOptionsExtensions
//    {

//        #region Register Permission Policies For

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="self"></param>
//        public static void RegisterPermissionPoliciesFor<T>(this AuthorizationOptions self)
//        {
//            var fieldInfos = typeof(T)
//                .GetFields(BindingFlags.Public | BindingFlags.Static)
//                .Where(x => x.FieldType == typeof(Permission))
//                .ToList();
//            foreach (var fieldInfo in fieldInfos)
//            {
//                var permission = (Permission)fieldInfo.GetValue(null);
//                // TODO: This is probably overkill I can use RequireClaim instead?
//                self.AddPolicy(permission.Name, p => p.AddRequirements(new PermissionRequirement(permission)));
//            }
//        }

//        #endregion

//        #region Register Permission Policies For

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="self"></param>
//        public static void RegisterResourceRequirementPoliciesFor<T>(this AuthorizationOptions self)
//        {
//            var fieldInfos = typeof(T)
//                .GetFields(BindingFlags.Public | BindingFlags.Static)
//                .Where(x => typeof(AbstractResourceRequirement).IsAssignableFrom(x.FieldType))
//                .ToList();
//            foreach (var fieldInfo in fieldInfos)
//            {
//                var resourceRequirement = (AbstractResourceRequirement) fieldInfo.GetValue(null);
//                self.AddPolicy(resourceRequirement.Name, p => p.AddRequirements(resourceRequirement));
//            }
//        }

//        #endregion
        
//    }

//}

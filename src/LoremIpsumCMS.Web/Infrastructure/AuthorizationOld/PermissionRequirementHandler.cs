﻿//#region License
//// Copyright (c) Niklas Wendel 2017-2018
//// 
//// Licensed under the Apache License, Version 2.0 (the "License"); 
//// you may not use this file except in compliance with the License. 
//// You may obtain a copy of the License at 
//// 
//// http://www.apache.org/licenses/LICENSE-2.0 
//// 
//// Unless required by applicable law or agreed to in writing, software 
//// distributed under the License is distributed on an "AS IS" BASIS, 
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//// See the License for the specific language governing permissions and 
//// limitations under the License.
//#endregion
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Authorization;
//using Raven.Client.Documents.Linq;

//namespace LoremIpsumCMS.Web.Infrastructure.Authorization
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    public class PermissionRequirementHandler : AuthorizationHandler<PermissionRequirement>
//    {

//        #region Handle Requirment Async

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="context"></param>
//        /// <param name="requirement"></param>
//        /// <returns></returns>
//        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, PermissionRequirement requirement)
//        {
//            var claims = context.User.FindAll(x => x.Type == Permission.ClaimType);
//            var allPermissionNames = requirement.Permission.GetAllPermissionNames();
//            if (claims.Any(c => c.Value.In(allPermissionNames)))
//            {
//                context.Succeed(requirement);
//            }

//            return Task.CompletedTask;
//        }

//        #endregion

//    }

//}

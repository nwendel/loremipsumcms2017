﻿//#region License
//// Copyright (c) Niklas Wendel 2017-2018
//// 
//// Licensed under the Apache License, Version 2.0 (the "License"); 
//// you may not use this file except in compliance with the License. 
//// You may obtain a copy of the License at 
//// 
//// http://www.apache.org/licenses/LICENSE-2.0 
//// 
//// Unless required by applicable law or agreed to in writing, software 
//// distributed under the License is distributed on an "AS IS" BASIS, 
//// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//// See the License for the specific language governing permissions and 
//// limitations under the License.
//#endregion
//using System.Collections.Generic;

//namespace LoremIpsumCMS.Web.Infrastructure.Authorization
//{

//    /// <summary>
//    /// 
//    /// </summary>
//    public abstract class AbstractResourcePermission : Permission
//    {

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        public AbstractResourcePermission(string name) : base(name)
//        {
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        /// <param name="impliedBy"></param>
//        public AbstractResourcePermission(string name, IEnumerable<Permission> impliedBy) : base(name, impliedBy)
//        {
//        }

//        #endregion

//    }

//    /// <summary>
//    /// 
//    /// </summary>
//    public class ResourcePermission<TResource> : AbstractResourcePermission
//    {

//        #region Constructor

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        public ResourcePermission(string name) : base(name)
//        {
//        }

//        /// <summary>
//        /// 
//        /// </summary>
//        /// <param name="name"></param>
//        /// <param name="impliedBy"></param>
//        public ResourcePermission(string name, IEnumerable<Permission> impliedBy) : base(name, impliedBy)
//        {
//        }

//        #endregion

//    }

//}

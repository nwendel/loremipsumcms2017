﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace LoremIpsumCMS.Web.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public class HostAccessor : IHostAccessor
    {

        // TODO: Overrides depends on session being configured, detect when initializing?

        #region Dependencies

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="options"></param>
        public HostAccessor(
            IHttpContextAccessor httpContextAccessor,
            IOptions<LoremIpsumOptions> options)
        {
            _httpContextAccessor = httpContextAccessor;
            _options = options;
        }

        #endregion

        #region Current Host

        /// <summary>
        /// 
        /// </summary>
        public HostString CurrentHost
        {
            get
            {
                var httpContext = _httpContextAccessor.HttpContext;
                var request = httpContext.Request;
                var requestHost = request.Host;

                if (_options.Value.IsHostnameOverrideEnabled && IsLocalhost) 
                {
                    var hostnameOverride = httpContext.Session.GetHostnameOverride();
                    if(!string.IsNullOrEmpty(hostnameOverride))
                    {
                        requestHost = requestHost.Port.HasValue
                            ? new HostString(hostnameOverride, requestHost.Port.Value)
                            : new HostString(hostnameOverride);
                    }
                }

                return requestHost;
            }
        }

        #endregion

        #region Is Localhost

        /// <summary>
        /// 
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        public bool IsLocalhost
        {
            get
            {
                var host = _httpContextAccessor.HttpContext.Request.Host.Host;
                return host == "localhost" || host == "::1";
            }
        }

        #endregion

        #region Has Override

        /// <summary>
        /// 
        /// </summary>
        public bool HasOverride
        {
            get
            {
                var httpContext = _httpContextAccessor.HttpContext;
                var hostnameOverride = httpContext.Session.GetHostnameOverride();
                return !string.IsNullOrEmpty(hostnameOverride);
            }
        }

        #endregion

        #region Override Host

        /// <summary>
        /// 
        /// </summary>
        public string OverrideHost
        {
            get
            {
                var httpContext = _httpContextAccessor.HttpContext;
                var hostnameOverride = httpContext.Session.GetHostnameOverride();
                return hostnameOverride;
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace LoremIpsumCMS.Web.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public class HostOverrideActionFilterAttribute : ActionFilterAttribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validationService"></param>
        public HostOverrideActionFilterAttribute()
        {
            // TODO: Needs to run early?  But what if Order is explicitly set should I still override it?
            Order = int.MinValue;
        }

        #endregion

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var httpContext = context.HttpContext;
            var request = httpContext.Request;
            var requestHost = request.Host;
            var serviceProvider = httpContext.RequestServices;
            var options = serviceProvider.GetRequiredService<IOptions<LoremIpsumOptions>>();
            var hostAccessor = serviceProvider.GetRequiredService<IHostAccessor>();

            if (options.Value.IsHostnameOverrideEnabled && hostAccessor.IsLocalhost)
            {
                var requestQuery = request.Query;
                if (requestQuery.TryGetValue(options.Value.HostnameOverrideQueryParameterName, out var value))
                {
                    httpContext.Session.SetHostnameOverride(value.ToString());
                }
            }

            base.OnActionExecuting(context);
        }

        #endregion

    }

}

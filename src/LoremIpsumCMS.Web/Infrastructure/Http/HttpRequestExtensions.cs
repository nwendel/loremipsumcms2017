﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;

namespace LoremIpsumCMS.Web.Infrastructure.Http
{

    /// <summary>
    /// 
    /// </summary>
    public static class HttpRequestExtensions
    {

        #region Fields

        private const string _headersReferrerKey = "Referer";

        #endregion

        #region Get Referrer

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetReferrer(this HttpRequest self)
        {
            var headers = self.Headers;
            if(!headers.ContainsKey(_headersReferrerKey))
            {
                return null;
            }
            var referrer = headers[_headersReferrerKey];
            return referrer.ToString();
        }

        #endregion

    }

}

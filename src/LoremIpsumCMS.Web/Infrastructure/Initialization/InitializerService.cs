﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS.Infrastructure.Dependency;
using LoremIpsumCMS.Infrastructure.Initialization;
using LoremIpsumCMS.Infrastructure.Logging;

namespace LoremIpsumCMS.Web.Infrastructure.Initialization
{

    /// <summary>
    /// 
    /// </summary>
    public class InitializerService : IInitializerService
    {

        #region Fields

        private readonly ILogger _logger = LogManager.CreateLogger<InitializerService>();

        #endregion

        #region Dependencies

        private readonly IEnumerable<IInitializer> _initializers;
        private readonly IEnumerable<IApplicationBuilderInitializer> _applicationBuilderInitializers;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="initializers"></param>
        /// <param name="applicationBuilderInitializers"></param>
        public InitializerService(
            IEnumerable<IInitializer> initializers,
            IEnumerable<IApplicationBuilderInitializer> applicationBuilderInitializers)
        {
            _initializers = initializers;
            _applicationBuilderInitializers = applicationBuilderInitializers;
        }

        #endregion

        #region Excute Initializers

        /// <summary>
        /// 
        /// </summary>
        public void ExecuteInitializers()
        {
            var initializers = _initializers
                .OrderByDependencies()
                .ToList();

            foreach(var initializer in initializers)
            {
                _logger.LogDebug("Before initialize {0}", initializer.GetType().FullName);
                initializer.Initialize();
                _logger.LogDebug("After initialize {0}", initializer.GetType().FullName);
            }
        }

        #endregion

        #region Excute Application Builder Initializers

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationBuilder"></param>
        public void ExecuteApplicationBuilderInitializers(IApplicationBuilder applicationBuilder)
        {
            var initializers = _applicationBuilderInitializers
                .OrderByDependencies()
                .ToList();

            foreach (var initializer in initializers)
            {
                _logger.LogDebug("Before initialize {0}", initializer.GetType().FullName);
                initializer.Initialize(applicationBuilder);
                _logger.LogDebug("After initialize {0}", initializer.GetType().FullName);
            }
        }

        #endregion

    }

}

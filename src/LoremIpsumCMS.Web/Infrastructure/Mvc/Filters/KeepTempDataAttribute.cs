﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Filters
{

    /// <summary>
    /// 
    /// </summary>
    public class KeepTempDataAttribute : ActionFilterAttribute
    {

        #region Fields

        private readonly string _key;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public KeepTempDataAttribute()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public KeepTempDataAttribute(string key)
        {
            _key = key;
        }

        #endregion

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            base.OnActionExecuting(context);

            var controller = context.Controller as Controller;
            if (controller == null)
            {
                return;
            }

            var tempData = controller.TempData;
            if(_key == null)
            {
                tempData.Keep();
            }
            else
            {
                if(tempData.ContainsKey(_key))
                {
                    tempData.Keep(_key);
                }
            }
        }

        #endregion

    }

}

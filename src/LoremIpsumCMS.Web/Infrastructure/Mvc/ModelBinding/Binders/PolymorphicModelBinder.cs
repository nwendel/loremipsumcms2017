﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Collections.Specialized;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class PolymorphicModelBinder : IModelBinder
    {

        #region Dependencies

        private readonly ModelBinderProviderContext _providerContext;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerContext"></param>
        public PolymorphicModelBinder(ModelBinderProviderContext providerContext)
        {
            _providerContext = providerContext;
        }

        #endregion

        #region Bind Model Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request;
            if (request.Method != "POST")
            {
                return Task.CompletedTask;
            }

            if (request.Form.Keys.None(x => x.StartsWith(bindingContext.ModelName)))
            {
                return Task.CompletedTask;
            }

            var formKey = $"{bindingContext.ModelName}.TypeIdentifier";
            var typeIdentifier = request.Form[formKey];
            if (string.IsNullOrEmpty(typeIdentifier))
            {
                throw new LoremIpsumException($"No value for {formKey} posted");
            }

            var typeIdentifierLookup = bindingContext.HttpContext.RequestServices.GetRequiredService<TypeIdentifierLookup>();
            var type = typeIdentifierLookup[typeIdentifier];
            var modelType = bindingContext.ModelMetadata.ModelType;
            if (!modelType.GetTypeInfo().IsAssignableFrom(type))
            {
                throw new LoremIpsumException($"{type.FullName} is not a valid type, must inherit from {modelType.FullName}");
            }

            var metadata = _providerContext.MetadataProvider.GetMetadataForType(type);
            bindingContext.ModelMetadata = metadata;
            var binder = _providerContext.CreateBinder(metadata);
            var result = binder.BindModelAsync(bindingContext);
            bindingContext.ValidationState[bindingContext.Model] = new ValidationStateEntry { Metadata = metadata };

            return result;
        }

        #endregion

    }

}

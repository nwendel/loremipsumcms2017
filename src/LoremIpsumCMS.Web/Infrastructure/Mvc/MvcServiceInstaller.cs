﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc
{

    /// <summary>
    /// 
    /// </summary>
    public class MvcServiceInstaller : IServiceInstaller
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Register(c => c
                .For<IActionContextAccessor>()
                .ImplementedBy<ActionContextAccessor>()
                .Lifetime.Scoped());

            serviceCollection.Register(c => c
                .For<IUrlHelper>()
                .UsingFactoryMethod(serviceProvider =>
                {
                    var actionContextAccessor = serviceProvider.GetRequiredService<IActionContextAccessor>();
                    var actionContext = actionContextAccessor.ActionContext;
                    return new UrlHelper(actionContext);
                }));
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class HtmlHelperGetViewComponentExtensions
    {

        #region Get View Component Helper

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IViewComponentHelper GetViewComponentHelper(this IHtmlHelper self)
        {
            var viewContext = self.ViewContext;
            var serviceProvider = viewContext.HttpContext.RequestServices;
            var viewComponentHelper = serviceProvider.GetRequiredService<IViewComponentHelper>();
            var viewContextAware = viewComponentHelper as IViewContextAware;
            viewContextAware.Contextualize(viewContext);
            return viewComponentHelper;
        }

        #endregion

    }

}

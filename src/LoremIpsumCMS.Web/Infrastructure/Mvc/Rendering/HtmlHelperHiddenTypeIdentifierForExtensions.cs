﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Collections.Specialized;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class HtmlHelperHiddenTypeIdentifierForExtensions
    {

        #region Assembly Qualified Type Name For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TProperty"></typeparam>
        /// <param name="self"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IHtmlContent HiddenTypeIdentifierFor<TModel, TProperty>(this IHtmlHelper<TModel> self, Expression<Func<TModel, TProperty>> expression)
        {
            var serviceProvider = self.ViewContext.HttpContext.RequestServices;
            var typeIdentifierLookup = serviceProvider.GetRequiredService<TypeIdentifierLookup>();

            var getValue = expression.Compile();
            var model = self.ViewData.Model;
            var value = getValue(model);
            var typeIdentifier = typeIdentifierLookup[value?.GetType()];

            var propertyName = ExpressionHelper.GetExpressionText(expression);
            var html = self.Hidden($"{propertyName}.TypeIdentifier", typeIdentifier);
            return html;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;
using LoremIpsumCMS.Web.Mvc.Routing;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class ActionViewComponent : ViewComponent
    {

        #region Dependencies

        private readonly IActionDescriptorCollectionProvider _actionDescriptorCollectionProvider;
        private readonly IActionSelector _actionSelector;
        private readonly IActionInvokerFactory _actionInvokerFactory;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="actionSelector"></param>
        /// <param name="actionInvokerFactory"></param>
        public ActionViewComponent(
            IActionDescriptorCollectionProvider actionDescriptorCollectionProvider,
            IActionSelector actionSelector,
            IActionInvokerFactory actionInvokerFactory)
        {
            _actionDescriptorCollectionProvider = actionDescriptorCollectionProvider;
            _actionSelector = actionSelector;
            _actionInvokerFactory = actionInvokerFactory;
        }

        #endregion

        #region Invoke Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public async Task<IHtmlContent> InvokeAsync(string controllerName, string actionName, object routeValues)
        {
            // TODO: Instead of all of this, can I use the MvcRouteHandler?  Similar to the PageRouteHandler

            var httpContext = CreateHttpContext();

            var routeContext = new RouteContext(httpContext);
            var routeData = routeContext.RouteData;
            routeData.PushState(null, new RouteValueDictionary(new { controller = controllerName, action = actionName }), null);
            routeData.PushState(null, new RouteValueDictionary(routeValues), null);
            routeData.UpdateDataTokens(ViewContext.RouteData.DataTokens);

            var actionDescriptor = FindActionDescriptor(routeContext);
            if (actionDescriptor == null)
            {
                // TODO: More information here...
                throw new LoremIpsumException("ViewComponent Action not found");
            }

            var actionContext = new ActionContext(httpContext, routeData, actionDescriptor)
            {
                // TODO: Support filters?  Filters are removed here since the TempData one generates duplicate key exception
                //ActionDescriptor = { FilterDescriptors = new FilterDescriptor[0] };
                ActionDescriptor =
                {
                    FilterDescriptors = new[] {new FilterDescriptor(new RouteDataActionFilterAttribute(), FilterScope.Action)}
                }
            };
            var invoker = _actionInvokerFactory.CreateInvoker(actionContext);

            var content = await InvokeActionAsync(httpContext, invoker);
            return new HtmlString(content);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private HttpContext CreateHttpContext()
        {
            // Cannot use HttpConetxtFactory here since that would change the HttpContext returned by HttpContextAccessor which I don't want.
            // I cannot use the same feature collection since httpcontext is just a wrapper around the feature collection, instead I need to copy each feature.
            var httpContext = new DefaultHttpContext();
            foreach (var feature in ViewContext.HttpContext.Features)
            {
                // TODO: Is this the only one which should not be copied?
                if (feature.Key != typeof(IHttpResponseFeature))
                {
                    httpContext.Features[feature.Key] = feature.Value;
                }
                else
                {
                    httpContext.Features.Set<IHttpResponseFeature>(new HttpResponseFeature());
                }
            }
            return httpContext;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeContext"></param>
        /// <returns></returns>
        private ActionDescriptor FindActionDescriptor(RouteContext routeContext)
        {
            var routeData = routeContext.RouteData;
        
            // TODO: This didn't work if I have attribute routes on the controller or action
            //var candidates = _actionSelector.SelectCandidates(routeContext);
            var candidates = _actionDescriptorCollectionProvider.ActionDescriptors.Items;
            candidates = candidates
                .OfType<ControllerActionDescriptor>()
                .Where(x => x.MethodInfo.CustomAttributes.Any(a => typeof(ViewComponentActionAttribute).IsAssignableFrom(a.AttributeType)))
                .ToList();
            // TODO: This filtering must be rewritten
            candidates = candidates
                .Where(x => x.RouteValues.All(r => 
                    (routeData.Values.ContainsKey(r.Key) || r.Value == null) && 
                    (r.Value == null || r.Value == (string)routeData.Values[r.Key])))
                .ToList();

            var actionDescriptor = _actionSelector.SelectBestCandidate(routeContext, candidates);
            return actionDescriptor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="actionInvoker"></param>
        /// <returns></returns>
        private async Task<string> InvokeActionAsync(HttpContext httpContext, IActionInvoker actionInvoker)
        {
            string content;
            var memoryStream = new MemoryStream();
            httpContext.Response.Body = memoryStream;

            await actionInvoker.InvokeAsync();
            httpContext.Response.Body.Position = 0;
            using (var reader = new StreamReader(httpContext.Response.Body))
            {
                content = reader.ReadToEnd();
            }

            //await actionInvoker.InvokeAsync().ContinueWith(t =>
            //{
            //    if (t.IsFaulted)
            //    {
            //        // TODO: Throw exception instead?
            //        content = t.Exception.Message;
            //    }
            //    else if (t.IsCompleted)
            //    {
            //        httpContext.Response.Body.Position = 0;
            //        using (var reader = new StreamReader(httpContext.Response.Body))
            //        {
            //            content = reader.ReadToEnd();
            //        }
            //    }
            //});

            return content;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc.Filters;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class RouteDataActionFilterAttribute : ActionFilterAttribute
    {

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var actionArguments = context.ActionArguments;
            var routeDataValues = context.RouteData.Values;
            foreach (var parameter in context.ActionDescriptor.Parameters)
            {
                if(routeDataValues.ContainsKey(parameter.Name))
                {
                    if(parameter.ParameterType.IsAssignableFrom(routeDataValues[parameter.Name].GetType()))
                    {
                        actionArguments[parameter.Name] = routeDataValues[parameter.Name];
                    }
                }
            }
        }

        #endregion

    }

}

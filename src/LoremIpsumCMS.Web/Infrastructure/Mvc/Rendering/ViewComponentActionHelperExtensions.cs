﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering.Internal;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class ViewComponentActionHelperExtensions
    {

        #region Action

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="actionName"></param>
        /// <param name="controllerName"></param>
        /// <returns></returns>
        public static IHtmlContent Action(this IViewComponentHelper self, string actionName, string controllerName)
        {
            return Action(self, actionName, controllerName, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="controllerName"></param>
        /// <param name="actionName"></param>
        /// <param name="routeValues"></param>
        /// <returns></returns>
        public static IHtmlContent Action(this IViewComponentHelper self, string actionName, string controllerName, object routeValues)
        {
            var contentTask = self.InvokeAsync(typeof(ActionViewComponent), new { ControllerName = controllerName, ActionName = actionName, RouteValues = routeValues });
            contentTask.Wait();
            var content = contentTask.Result;
            return content;
        }

        #endregion

    }

}

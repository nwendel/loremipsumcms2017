﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace LoremIpsumCMS.Web.Infrastructure.Mvc.TagHelpers
{

    // TODO: This needs to be rewritten

    /// <summary>
    /// 
    /// </summary>
    [HtmlTargetElement("loremipsum-validation-indicator-item", ParentTag = "loremipsum-validation-indicator")]
    public class LoremIpsumValidationIndicatorItemTagHelper : TagHelper
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ModelExpression For { get; set; }

        #endregion

        #region Process Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public override Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
        {
            var asdf = (LoremIpsumValidationIndicatorTagHelper)context.Items[typeof(LoremIpsumValidationIndicatorTagHelper)];
            asdf.ForAll.Add(For);
            output.SuppressOutput();

            // var fullName = NameAndIdProvider.GetFullHtmlFieldName(viewContext, expression);
            //var formContext = viewContext.ClientValidationEnabled ? viewContext.FormContext : null;
            //if (!viewContext.ViewData.ModelState.ContainsKey(fullName) && formContext == null)

            return Task.CompletedTask;
            //return base.ProcessAsync(context, output);
        }

        #endregion

    }

}

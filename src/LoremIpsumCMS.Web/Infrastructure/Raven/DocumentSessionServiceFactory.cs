﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using FluentRegistration;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Proxy;
using LoremIpsumCMS.Infrastructure.Raven;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentSessionServiceFactory :
        IServiceFactory<IDocumentSession> //,
        //IServiceFactory<IAsyncDocumentSession>
    {

        #region Dependencies

        private readonly IDocumentStore _documentStore;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IDocumentSessionHolder _documentSessionHolder;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Fields

        private readonly IDocumentSession _documentSession;
        //private readonly IAsyncDocumentSession _asyncDocumentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="documentSessionHolder"></param>
        /// <param name="options"></param>
        public DocumentSessionServiceFactory(
            IDocumentStore documentStore,
            IHttpContextAccessor httpContextAccessor,
            IDocumentSessionHolder documentSessionHolder,
            IOptions<LoremIpsumOptions> options)
        {
            _documentStore = documentStore;
            _httpContextAccessor = httpContextAccessor;
            _documentSessionHolder = documentSessionHolder;
            _options = options;

            _documentSession = CreateDocumentSession();
            //_asyncDocumentSession = CreateAsyncDocumentSession();
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IDocumentSession IServiceFactory<IDocumentSession>.Create()
        {
            return _documentSession;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //IAsyncDocumentSession IServiceFactory<IAsyncDocumentSession>.Create()
        //{
        //    return _asyncDocumentSession;
        //}

        #endregion

        #region Create Document Session

        /// <summary>
        /// 
        /// </summary>
        public IDocumentSession CreateDocumentSession()
        {
            return ProxyFactory.Create(() =>
            {
                var httpContext = _httpContextAccessor.HttpContext;
                if (httpContext == null)
                {
                    return _documentSessionHolder.DocumentSession;
                }

                var documentSession = httpContext.GetDocumentSession();
                if (documentSession == null)
                {
                    documentSession = _documentStore.OpenSession(_options.Value.RavenDatabase);
                    httpContext.SetDocumentSession(documentSession);
                }
                return documentSession;
            });
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public IAsyncDocumentSession CreateAsyncDocumentSession()
        //{
        //    return ProxyFactory.Create(() =>
        //    {
        //        var httpContext = _httpContextAccessor.HttpContext;
        //        if (httpContext == null)
        //        {
        //            return _ravenSessionHolder.AsyncDocumentSession;
        //        }

        //        var asyncDocumentSession = httpContext.GetAsyncDocumentSession();
        //        if (asyncDocumentSession == null)
        //        {
        //            asyncDocumentSession = _documentStore.OpenAsyncSession(_options.Value.RavenDatabase);
        //            httpContext.SetAsyncDocumentSession(asyncDocumentSession);
        //        }
        //        return asyncDocumentSession;
        //    });
        //}

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;
using FluentRegistration;
using Raven.Client.Documents;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentStoreServiceFactory : IServiceFactory<IDocumentStore>
    {

        #region Dependencies

        private readonly IOptions<LoremIpsumOptions> _options;
        private readonly IApplicationLifetime _applicationLifetime;
        private readonly UniqueConstraintsListener _uniqueConstraintsListener;


        #endregion

        #region Fields

        private readonly Lazy<IDocumentStore> _documentStore;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <param name="applicationLifetime"></param>
        /// <param name="uniqueConstraintsListener"></param>
        public DocumentStoreServiceFactory(
            IOptions<LoremIpsumOptions> options,
            IApplicationLifetime applicationLifetime,
            UniqueConstraintsListener uniqueConstraintsListener)
        {
            _options = options;
            _applicationLifetime = applicationLifetime;
            _uniqueConstraintsListener = uniqueConstraintsListener;

            _documentStore = new Lazy<IDocumentStore>(CreateInternal);
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IDocumentStore Create()
        {
            return _documentStore.Value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IDocumentStore CreateInternal()
        {
            var documentStore = new DocumentStore
            {
                Urls = new [] { _options.Value.RavenUrl },
                Database = _options.Value.RavenDatabase
            };

            // TODO: Raven4
            //documentStore.RegisterListener(new UniqueConstraintsStoreListener());
            documentStore.OnBeforeStore += _uniqueConstraintsListener.OnBeforeStore;
            documentStore.OnBeforeDelete += _uniqueConstraintsListener.OnBeforeDelete;

            documentStore.Initialize();

            // TODO: Raven4
            //documentStore.DatabaseCommands.GlobalAdmin.EnsureDatabaseExists(_options.Value.RavenDatabase);

            _applicationLifetime.ApplicationStopped.Register(() => DisposeDocumentStore());

            return documentStore;
        }

        #endregion

        #region Close

        /// <summary>
        /// 
        /// </summary>
        private void DisposeDocumentStore()
        {
            _documentStore.Value.Dispose();
        }

        #endregion

    }

}

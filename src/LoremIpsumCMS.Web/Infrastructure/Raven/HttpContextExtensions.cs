﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Http;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public static class HttpContextExtensions
    {

        #region Fields

        private const string _documentSessionKey = "LoremIpsumCMS:DocumentSession";
        private const string _asyncDocumentSessionKey = "LoremIpsumCMS:AsyncDocumentSession";

        #endregion

        #region Get / Set Document Session

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static IDocumentSession GetDocumentSession(this HttpContext self)
        {
            var documentSession = (IDocumentSession)self.Items[_documentSessionKey];
            return documentSession;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="documentSession"></param>
        public static void SetDocumentSession(this HttpContext self, IDocumentSession documentSession)
        {
            self.Items[_documentSessionKey] = documentSession;
        }

        #endregion

        #region Get / Set Async Document Session

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="self"></param>
        ///// <returns></returns>
        //public static IAsyncDocumentSession GetAsyncDocumentSession(this HttpContext self)
        //{
        //    var asyncDocumentSession = (IAsyncDocumentSession)self.Items[_asyncDocumentSessionKey];
        //    return asyncDocumentSession;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="self"></param>
        ///// <param name="asyncDocumentSession"></param>
        //public static void SetAsyncDocumentSession(this HttpContext self, IAsyncDocumentSession asyncDocumentSession)
        //{
        //    self.Items[_asyncDocumentSessionKey] = asyncDocumentSession;
        //}

        #endregion


    }

}

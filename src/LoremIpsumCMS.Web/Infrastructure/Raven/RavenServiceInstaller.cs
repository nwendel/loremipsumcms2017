﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Register(r => r
                .FromThisAssembly()
                .Where(c => c.InThisNamespace() && c.AssignableTo(typeof(IServiceFactory<>)))
                .WithServices.AllInterfaces());

            serviceCollection.Register(c => c
                .For<IDocumentStore>()
                .UsingFactory());

            serviceCollection.Register(c => c
                .For<IDocumentSession>()
                .UsingFactory());

            serviceCollection.Register(c => c
                .For<IAsyncDocumentSession>()
                .UsingFactory());
        }

        #endregion

    }

}

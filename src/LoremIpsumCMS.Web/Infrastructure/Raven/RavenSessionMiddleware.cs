﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace LoremIpsumCMS.Web.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class RavenSessionMiddleware
    {

        #region Dependencies

        private readonly RequestDelegate _next;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        public RavenSessionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        #endregion

        #region Invoke

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext httpContext)
        {
            await _next(httpContext);
            CloseSession(httpContext);
        }

        #endregion

        #region Close Sessions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        private static void CloseSession(HttpContext httpContext)
        {
            var documentSession = httpContext.GetDocumentSession();
            documentSession?.Dispose();

            //var asyncDocumentSession = httpContext.GetAsyncDocumentSession();
            //asyncDocumentSession?.Dispose();
        }

        #endregion

    }

}

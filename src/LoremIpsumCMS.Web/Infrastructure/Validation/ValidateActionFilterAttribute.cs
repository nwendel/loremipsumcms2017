﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding;

namespace LoremIpsumCMS.Web.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateActionFilterAttribute : ActionFilterAttribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public ValidateActionFilterAttribute()
        {
            // TODO: Needs to run last?  But what if Order is explicitly set should I still override it?
            // TODO: Needs to run before AuthorizeResourceAttribute
            Order = int.MaxValue - 1000;
        }

        #endregion

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Validate(context);
            base.OnActionExecuting(context);
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void Validate(ActionExecutingContext context)
        {
            var actionDescriptor = context.ActionDescriptor;
            var actionName = actionDescriptor.RouteValues["action"];
            var actionParameters = actionDescriptor.Parameters;
            var actionParameterTypes = actionParameters.Select(x => x.ParameterType).ToArray();
            var actionArgumentValues = context.ActionArguments.Values.ToArray();
            var controller = (Controller)context.Controller;
            var controllerType = controller.GetType();
            var serviceProvider = context.HttpContext.RequestServices;

            ValidateArguments(controller, actionArgumentValues, serviceProvider);
            var result = InvokeControllerValidate(controller, controllerType, actionName, actionParameterTypes, actionArgumentValues);
            if (result.IsRedirect)
            {
                context.Result = result.RedirectResult;
            }
            else if (!controller.ModelState.IsValid)
            {
                InvokeControllerInvalid(context, controller, controllerType, actionName, actionParameterTypes, actionArgumentValues);
            }
        }

        #endregion

        #region Validate Arguments

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="actionArgumentValues"></param>
        /// <param name="serviceProvider"></param>
        private void ValidateArguments(Controller controller, object[] actionArgumentValues, IServiceProvider serviceProvider)
        {
            var validationService = serviceProvider.GetRequiredService<IValidationService>();
            foreach (var argument in actionArgumentValues.Where(x => x != null))
            {
                var messages = validationService.Validate(argument).ToList();
                controller.ModelState.AddModelErrors(messages);
            }
        }

        #endregion

        #region Invoke Controller Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="controllerType"></param>
        /// <param name="actionName"></param>
        /// <param name="actionParameterTypes"></param>
        /// <param name="actionArgumentValues"></param>
        /// <returns></returns>
        private ValidationResult InvokeControllerValidate(
            Controller controller, Type controllerType,
            string actionName, Type[] actionParameterTypes, object[] actionArgumentValues)
        {
            var validateMethodInfo = controllerType.GetMethod("Validate" + actionName, actionParameterTypes);
            if (validateMethodInfo == null)
            {
                return ValidationResult.Success;
            }

            var result = InvokeControllerValidate(validateMethodInfo, controller, actionArgumentValues);
            if (result.HasMessages)
            {
                controller.ModelState.AddModelErrors(result.Messages);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="controller"></param>
        /// <param name="actionArgumentValues"></param>
        /// <returns></returns>
        private ValidationResult InvokeControllerValidate(MethodInfo methodInfo, Controller controller, object[] actionArgumentValues)
        {
            if (methodInfo.ReturnType == typeof(ValidationResult))
            {
                var validationResult = (ValidationResult)methodInfo.InvokeAndUnwrapException(controller, actionArgumentValues);
                return validationResult;
            }
            if (methodInfo.ReturnType == typeof(Task<ValidationResult>))
            {
                // TODO: Use await here instead?
                var validationResultTask = (Task<ValidationResult>)methodInfo.InvokeAndUnwrapException(controller, actionArgumentValues);
                var validationResult = validationResultTask.GetAwaiter().GetResult();
                return validationResult;
            }

            throw new ValidationMethodException($"Method {methodInfo.Name} has invalid return type");
        }

        #endregion

        #region Invoke Controller Invalid

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="controller"></param>
        /// <param name="controllerType"></param>
        /// <param name="actionName"></param>
        /// <param name="actionParameterTypes"></param>
        /// <param name="actionArgumentValues"></param>
        private void InvokeControllerInvalid(
            ActionExecutingContext context, Controller controller, Type controllerType,
            string actionName, Type[] actionParameterTypes, object[] actionArgumentValues)
        {
            var invalidMethodInfo = controllerType.GetMethod("Invalid" + actionName, actionParameterTypes);
            if (invalidMethodInfo == null)
            {
                throw new ValidationMethodException($"Method Invalid{actionName} is missing from controller {controllerType.FullName}");
            }

            var actionResult = InvokeControllerInvalid(invalidMethodInfo, controller, actionArgumentValues);
            if (actionResult == null)
            {
                throw new ValidationMethodException($"Method Invalid{actionName} cannot return null");
            }
            context.Result = actionResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="controller"></param>
        /// <param name="actionArgumentValues"></param>
        /// <returns></returns>
        private IActionResult InvokeControllerInvalid(MethodInfo methodInfo, Controller controller, object[] actionArgumentValues)
        {
            if (methodInfo.ReturnType == typeof(IActionResult))
            {
                var actionResult = (IActionResult)methodInfo.InvokeAndUnwrapException(controller, actionArgumentValues);
                return actionResult;
            }
            if (methodInfo.ReturnType == typeof(Task<IActionResult>))
            {
                // TODO: Use await here instead?
                var actionResultTask = (Task<IActionResult>)methodInfo.InvokeAndUnwrapException(controller, actionArgumentValues);
                var actionResult = actionResultTask.GetAwaiter().GetResult();
                return actionResult;
            }

            throw new ValidationMethodException($"Method {methodInfo.Name} has invalid return type");
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation;

namespace LoremIpsumCMS.Web.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidationResult
    {

        #region Fields

        private readonly List<ValidationMessage> _messages = new List<ValidationMessage>();

        #endregion

        #region Static Results

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult Success = new ValidationResult();

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult NotFound = new ValidationResult { RedirectResult = new NotFoundResult() };

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult Forbidden = new ValidationResult { RedirectResult = new ForbidResult() };

        /// <summary>
        /// 
        /// </summary>
        public static readonly ValidationResult BadRequest = new ValidationResult { RedirectResult = new BadRequestResult() };

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public bool IsSuccess => _messages.None();

        /// <summary>
        /// 
        /// </summary>
        public bool HasMessages => _messages.Any();

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<ValidationMessage> Messages => _messages.AsReadOnly();

        /// <summary>
        /// 
        /// </summary>
        public bool IsRedirect => RedirectResult != null;

        /// <summary>
        /// 
        /// </summary>
        public IActionResult RedirectResult { get; private set; }

        #endregion

    }

}

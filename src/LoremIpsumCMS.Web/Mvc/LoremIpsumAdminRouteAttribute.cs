﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Areas;
using LoremIpsumCMS.Web.Areas.AdminUser.Controllers;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Mvc
{

    // TODO: Base off IRouteTemplateProvider instead?

    /// <summary>
    /// 
    /// </summary>
    public class LoremIpsumAdminRouteAttribute : RouteAttribute, IActionFilter
    {

        private bool _isFirstRun = true;

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public LoremIpsumAdminRouteAttribute() :
            base("admin")
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="template"></param>
        public LoremIpsumAdminRouteAttribute(string template) : 
            base($"admin/{template}")
        {
        }

        #endregion

        #region On Action Executing

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var serviceProvider = context.HttpContext.RequestServices;
            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
            var hostAccessor = serviceProvider.GetRequiredService<IHostAccessor>();

            var hostname = hostAccessor.CurrentHost.Host;
            var host = documentSession.LoadBy<Host>(x => x.Name(hostname));

            if (host != null && !host.IsAdmin)
            {
                context.Result = new NotFoundResult();
            }
            else if (host == null)
            {
                if (_isFirstRun)
                {
                    var setupService = serviceProvider.GetRequiredService<ISetupService>();
                    var created = setupService.Setup(hostname);
                    if (created)
                    {
                        context.Result = new RedirectToActionResult(nameof(AccountController.Login), AccountController.Name, new {Area = AreaNames.AdminUser});
                    }
                    _isFirstRun = false;
                }
                else
                {
                    context.Result = new NotFoundResult();
                }
            }
        }

        #endregion

        #region On Action Executed

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using LoremIpsumCMS.Model;
using System.Linq;
using Raven.Client.Documents.Session;
using Microsoft.Extensions.DependencyInjection;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Infrastructure;

namespace LoremIpsumCMS.Web.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentAreaModelBinder : IModelBinder
    {

        #region Bind Model Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext.Request;
            if (request.Method != "POST")
            {
                return Task.CompletedTask;
            }

            var partialNameSlugs = new List<string>();
            var index = 0;
            while(true)
            {
                var isDeletedformKey = $"{bindingContext.ModelName}.Items[{index}].IsDeleted";
                var nameformKey = $"{bindingContext.ModelName}.Items[{index}].Name";
                if (!request.Form.ContainsKey(isDeletedformKey))
                {
                    break;
                }

                var isDeleted = request.Form[isDeletedformKey].Any(x => x == "true");
                var name = request.Form[nameformKey];

                if(!isDeleted)
                {
                    partialNameSlugs.Add(name[0].Slugify());
                }

                index += 1;
            }

            var serviceProvider = bindingContext.HttpContext.RequestServices;
            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
            var hostAccessor = serviceProvider.GetRequiredService<IHostAccessor>();
            var currentHost = hostAccessor.CurrentHost;
            var siteOverview = documentSession.LoadBy<SiteOverview>(x => x.Hostname(currentHost.Host));
            var partialOverviews = documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlugs(siteOverview.SiteId, partialNameSlugs.ToArray()));

            var contentArea = new ContentArea();
            foreach(var partialOverview in partialOverviews)
            {
                var item = new PartialContentAreaItem { PartialId = partialOverview.PartialId };
                contentArea.Add(item);
            }

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, contentArea, contentArea.ToString());
            bindingContext.Result = ModelBindingResult.Success(contentArea);

            return Task.CompletedTask;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Infrastructure.Mvc.ModelBinding.Binders;

namespace LoremIpsumCMS.Web.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class ModelBindersServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Configure<MvcOptions>(o =>
            {
                o.ModelBinderProviders.Insert(0, new SimpleModelBinderProvider<Site, SiteModelBinder>());
                o.ModelBinderProviders.Insert(0, new SimpleModelBinderProvider<SiteReference, SiteRefenceModelBinder>());

                o.ModelBinderProviders.Insert(0, new PolymorphicModelBinderProvider<AbstractSiteExtensionProperties>());

                o.ModelBinderProviders.Insert(0, new PolymorphicModelBinderProvider<AbstractPageData>());
                o.ModelBinderProviders.Insert(0, new PolymorphicModelBinderProvider<AbstractPageExtensionProperties>());

                o.ModelBinderProviders.Insert(0, new PolymorphicModelBinderProvider<AbstractPartialData>());
                o.ModelBinderProviders.Insert(0, new PolymorphicModelBinderProvider<AbstractPartialExtensionProperties>());

                o.ModelBinderProviders.Insert(0, new SimpleModelBinderProvider<ContentArea, ContentAreaModelBinder>());
            });
        }

        #endregion

    }

}

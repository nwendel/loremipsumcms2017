﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using LoremIpsumCMS.Infrastructure.Collections.Specialized;
using LoremIpsumCMS.Infrastructure.Initialization;
using LoremIpsumCMS.Services;

namespace LoremIpsumCMS.Web.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class PolymorphicModelBinderInitializer : IInitializer
    {

        #region Dependencies

        private readonly IContentTypeService _contentTypeService;
        private readonly TypeIdentifierLookup _typeIdentifierLookup;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentTypeService"></param>
        /// <param name="typeIdentifierLookup"></param>
        public PolymorphicModelBinderInitializer(
            IContentTypeService contentTypeService,
            TypeIdentifierLookup typeIdentifierLookup)
        {
            _contentTypeService = contentTypeService;
            _typeIdentifierLookup = typeIdentifierLookup;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            _typeIdentifierLookup.AddRange(_contentTypeService.FindSiteExtensionPropertyTypes().Select(x => x.Type));

            _typeIdentifierLookup.AddRange(_contentTypeService.FindPageDataTypes().Select(x => x.Type));
            _typeIdentifierLookup.AddRange(_contentTypeService.FindPageExtensionPropertyTypes().Select(x => x.Type));

            _typeIdentifierLookup.AddRange(_contentTypeService.FindPartialDataTypes().Select(x => x.Type));
            _typeIdentifierLookup.AddRange(_contentTypeService.FindPartialExtensionPropertyTypes().Select(x => x.Type));
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Mvc.ModelBinding.Binders
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteModelBinder : IModelBinder
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var serviceProvider = bindingContext.HttpContext.RequestServices;
            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
            var hostAccessor = serviceProvider.GetRequiredService<IHostAccessor>();

            var host = hostAccessor.CurrentHost.Host;
            var site = host != null
                ? documentSession.LoadBy<Site>(x => x.Hostname(host))
                : null;

            bindingContext.ModelState.SetModelValue(bindingContext.ModelName, site, site?.ToString());
            bindingContext.Result = ModelBindingResult.Success(site);

            return Task.CompletedTask;
        }

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Mvc.Rendering;
using LoremIpsumCMS.Web.Mvc.ViewFeatures;

namespace LoremIpsumCMS.Web.Mvc.Razor
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class LoremIpsumRazorPage<TContentModel, TModel> : RazorPage<TModel>
        where TContentModel : AbstractContentData
    {

        #region Fields

        private readonly Lazy<ILoremIpsumHelper<TContentModel>> _lazyLoremIpsumHelper;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected LoremIpsumRazorPage()
        {
            _lazyLoremIpsumHelper = new Lazy<ILoremIpsumHelper<TContentModel>>(CreateLoremIpsumHelper);
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public ILoremIpsumHelper<TContentModel> LoremIpsum
        {
            get
            {
                return _lazyLoremIpsumHelper.Value;
            }
        }

        #endregion

        #region Create Lorem Ipsum Helper

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private ILoremIpsumHelper<TContentModel> CreateLoremIpsumHelper()
        {
            var serviceProvider = ViewContext.HttpContext.RequestServices;
            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
            var htmlHelper = serviceProvider.GetRequiredService<IHtmlHelper<TContentModel>>();
            var viewContextAware = htmlHelper as IViewContextAware;
            viewContextAware.Contextualize(ViewContext);

            var loremIpsumHelper = new LoremIpsumHelper<TContentModel>(documentSession, htmlHelper);
            return loremIpsumHelper;
        }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TContentModel"></typeparam>
    public abstract class LoremIpsumRazorPage<TContentModel> : LoremIpsumRazorPage<TContentModel, TContentModel>
        where TContentModel : AbstractContentData
    {
    }

}

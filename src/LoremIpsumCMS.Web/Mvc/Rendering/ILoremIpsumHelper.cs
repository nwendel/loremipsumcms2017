﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Web.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public interface ILoremIpsumHelper
    {

        #region Helper Properties

        /// <summary>
        /// 
        /// </summary>
        IHtmlHelper Html { get; }

        /// <summary>
        /// 
        /// </summary>
        IViewComponentHelper Component { get; }

        #endregion

        #region Content Properties

        /// <summary>
        /// 
        /// </summary>
        Site Site { get; }

        /// <summary>
        /// 
        /// </summary>
        IAbstractContent<AbstractContentData, AbstractExtensionProperties> Content { get; }

        /// <summary>
        /// 
        /// </summary>
        AbstractContentData ContentData { get; }

        /// <summary>
        /// 
        /// </summary>
        Page Page { get; }

        /// <summary>
        /// 
        /// </summary>
        AbstractPageData PageData { get; }

        /// <summary>
        /// 
        /// </summary>
        Partial Partial { get; }

        /// <summary>
        /// 
        /// </summary>
        AbstractPartialData PartialData { get; }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    public interface ILoremIpsumHelper<TModel> : ILoremIpsumHelper
        where TModel : AbstractContentData
    {

        #region Helper Properties

        /// <summary>
        /// 
        /// </summary>
        new IHtmlHelper<TModel> Html { get; }

        #endregion

    }

}

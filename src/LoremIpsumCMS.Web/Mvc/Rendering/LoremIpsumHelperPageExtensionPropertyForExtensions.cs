﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Html;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Web.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class LoremIpsumHelperPageExtensionPropertyForExtensions
    {

        #region Page Extension Property For

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public static IHtmlContent PageExtensionPropertyFor<TExtention>(this ILoremIpsumHelper self, Expression<Func<TExtention, string>> propertyExpression)
            where TExtention : AbstractPageExtensionProperties
        {
            var page = self.Page;
            var property = propertyExpression.Compile();
            var value = page.GetExtensionProperty(propertyExpression);
            return new HtmlString(value);
        }

        #endregion

    }

}

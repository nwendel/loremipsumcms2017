﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Infrastructure.Http;
using LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering;
using LoremIpsumCMS.Web.Mvc.Routing;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;

namespace LoremIpsumCMS.Web.Mvc.Rendering
{

    /// <summary>
    /// 
    /// </summary>
    public static class LoremIpsumHelperPartialExtensions
    {

        #region Partial

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IHtmlContent Partial(this ILoremIpsumHelper self, string name)
        {
            var htmlHelper = self.Html;
            var viewComponentHelper = htmlHelper.GetViewComponentHelper();
            var serviceProvider = htmlHelper.ViewContext.HttpContext.RequestServices;
            var hostAccessor = serviceProvider.GetRequiredService<IHostAccessor>();
            var documentSession = serviceProvider.GetRequiredService<IDocumentSession>();
            var contentRouteInfoCache = serviceProvider.GetRequiredService<IContentRouteInfoCache>();

            var hostname = hostAccessor.CurrentHost.Host;
            var siteOverview = documentSession.LoadBy<SiteOverview>(x => x.Hostname(hostname));
            if(siteOverview == null)
            {
                // TODO: Probably should not throw here, instead return some content about not found
                throw new LoremIpsumException("No site");
            }
            var partialOverview = documentSession.LoadBy<PartialOverview>(x => x.SiteIdAndNameSlug(siteOverview.SiteId, name.Slugify()));
            if(partialOverview == null)
            {
                // TODO: Probably should not throw here, instead return some content about not found
                throw new LoremIpsumException("No partial");
            }

            var routeData = htmlHelper.ViewContext.RouteData;
            var partialReference = new PartialReference(partialOverview);
            routeData.UpdateDataTokens(partialReference);

            var contentRouteInfo = contentRouteInfoCache.GetRouteInfo(partialOverview.DataTypeFullName);
            var rvd = new RouteValueDictionary();
            foreach(var parameter in contentRouteInfo.Parameters)
            {
                rvd.Add(parameter.Name, parameter.GetValue(siteOverview.SiteId, partialOverview.PartialId));
            }

            var content = viewComponentHelper.Action(contentRouteInfo.ActionName, contentRouteInfo.ControllerName, rvd);
            return content;
        }

        #endregion

    }

}

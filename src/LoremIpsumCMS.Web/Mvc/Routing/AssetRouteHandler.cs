﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Infrastructure.Http;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class AssetRouteHandler : IRouter
    {

        #region Dependencies

        private readonly IHostAccessor _hostAccessor;
        private readonly IDocumentSession _documentSession;
        private readonly MvcRouteHandler _mvcRouteHandler;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostAccessor"></param>
        /// <param name="documentSession"></param>
        /// <param name="mvcRouteHandler"></param>
        public AssetRouteHandler(
            IHostAccessor hostAccessor,
            IDocumentSession documentSession,
            MvcRouteHandler mvcRouteHandler,
            IOptions<LoremIpsumOptions> options)
        {
            _hostAccessor = hostAccessor;
            _documentSession = documentSession;
            _mvcRouteHandler = mvcRouteHandler;
            _options = options;
        }

        #endregion

        #region Route Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task RouteAsync(RouteContext context)
        {
            var hostname = _hostAccessor.CurrentHost.Host;
            var fullPath = context.HttpContext.Request.Path.ToString();

            if(!fullPath.StartsWith($"/{_options.Value.AssetRootPath}/"))
            {
                return Task.CompletedTask;
            }

            var siteOverview = _documentSession.LoadBy<SiteOverview>(x => x.Hostname(hostname));
            if (siteOverview == null)
            {
                return Task.CompletedTask;
            }
            var path = fullPath.Substring(_options.Value.AssetRootPath.Length + 1);
            var asset = _documentSession.LoadBy<Asset>(x => x.SiteIdAndPath(siteOverview.SiteId, path));
            if (asset == null)
            {
                return Task.CompletedTask;
            }

            context.RouteData.Values["controller"] = "Asset";
            context.RouteData.Values["action"] = "Index";

            return _mvcRouteHandler.RouteAsync(context);
        }

        #endregion

        #region Get Virtual Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            // TODO: Anything here?
            return null;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Initialization;
using LoremIpsumCMS.Infrastructure.Logging;
using LoremIpsumCMS.Services;
using LoremIpsumCMS.Web.Mvc.Routing.Internal.Parameters;

namespace LoremIpsumCMS.Web.Mvc.Routing.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentRouteInfoCache :
        IContentRouteInfoCache,
        IInitializer
    {

        #region Dependencies

        private readonly IReflectionService _reflectionService;
        private readonly IEnumerable<IRouteParameterFactory> _routeParameterFactories;

        #endregion

        #region Fields

        private readonly ILogger _logger = LogManager.CreateLogger<ContentRouteInfoCache>();
        private readonly IDictionary<string, ContentRouteInfo> _contentRouteInfos = new Dictionary<string, ContentRouteInfo>();
        private bool _isInitialized;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reflectionService"></param>
        public ContentRouteInfoCache(
            IReflectionService reflectionService,
            IEnumerable<IRouteParameterFactory> routeParameterFactories)
        {
            _reflectionService = reflectionService;
            _routeParameterFactories = routeParameterFactories;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            var allTypes = _reflectionService.FindApplicationAndLoremIpsumAssemblies()
                .SelectMany(x => x.GetTypes())
                .ToList();
            var controllerTypes = allTypes
                .Where(x => !x.GetTypeInfo().IsAbstract && typeof(Controller).IsAssignableFrom(x))
                .ToList();

            Initialize<PageRouteForAttribute>(controllerTypes);
            Initialize<PartialRouteForAttribute>(controllerTypes);

            _isInitialized = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TController"></typeparam>
        /// <param name="controllerTypes"></param>
        private void Initialize<TController>(IEnumerable<Type> controllerTypes)
            where TController : Attribute, IContentRouteForAttribute
        {
            foreach (var controllerType in controllerTypes)
            {
                foreach (var methodInfo in controllerType.GetMethods())
                {
                    if(IsContentRoute<TController>(methodInfo))
                    {
                        var dataType = methodInfo.GetCustomAttribute<TController>().DataType;
                        var actionName = methodInfo.Name;
                        var routeParameters = AnalyzeRouteParameters(methodInfo, dataType);
                        // TODO: Area?

                        _logger.LogDebug("Found controller action");
                        _logger.LogDebug("DataType: {0}", dataType.Name);
                        _logger.LogDebug("ControllerType: {0}", controllerType.Name);
                        _logger.LogDebug("ActionName: {0}", actionName);

                        _contentRouteInfos[dataType.FullName] = new ContentRouteInfo
                        {
                            ControllerName = controllerType.Name.TrimEndsWith(nameof(Controller)),
                            ActionName = methodInfo.Name,
                            Parameters = routeParameters
                        };
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodInfo"></param>
        /// <returns></returns>
        private static bool IsContentRoute<T>(MethodInfo methodInfo)
            where T : Attribute
        {
            var isContentRoute = 
                methodInfo.IsPublic &&
                typeof(IActionResult).IsAssignableFrom((methodInfo.ReturnParameter.ParameterType)) &&
                methodInfo.GetCustomAttribute<T>() != null;
            return isContentRoute;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="dataType"></param>
        private IEnumerable<RouteParameter> AnalyzeRouteParameters(MethodInfo methodInfo, Type dataType)
        {
            var parameters = new List<RouteParameter>();
            var methodParameters = methodInfo.GetParameters();
            foreach (var methodParameter in methodParameters)
            {
                var methodParameterType = methodParameter.ParameterType;
                var factory = _routeParameterFactories.SingleOrDefault(x => x.CanCreate(methodParameterType, dataType));
                if (factory != null)
                {
                    var parameter = factory.Create(methodParameter.Name, methodParameterType, dataType);
                    parameters.Add(parameter);
                }
            }
            return parameters;
        }

        #endregion

        #region Get Route Info

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public ContentRouteInfo GetRouteInfo(Type dataType)
        {
            if (dataType == null)
            {
                throw new ArgumentNullException(nameof(dataType));
            }

            return GetRouteInfo(dataType.FullName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTypeFullName"></param>
        /// <returns></returns>
        public ContentRouteInfo GetRouteInfo(string dataTypeFullName)
        {
            if (dataTypeFullName == null)
            {
                throw new ArgumentNullException(nameof(dataTypeFullName));
            }
            if (!_isInitialized)
            {
                throw new InvalidOperationException($"{nameof(ContentRouteInfoCache)} is not initialized");
            }

            var found = _contentRouteInfos.TryGetValue(dataTypeFullName, out ContentRouteInfo contentRouteInfo);
            if (!found)
            {
                throw new ArgumentException($"No content route found for {dataTypeFullName}");
            }
            return contentRouteInfo;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Web.Mvc.Routing.Internal.Parameters
{

    /// <summary>
    /// 
    /// </summary>
    public interface IRouteParameterFactory
    {

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterType"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        bool CanCreate(Type parameterType, Type dataType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameterType"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        RouteParameter Create(string name, Type parameterType, Type dataType);

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Web.Mvc.Routing.Internal.Parameters
{

    /// <summary>
    /// 
    /// </summary>
    public class PageDataRouteParameterFactory : IRouteParameterFactory
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public PageDataRouteParameterFactory(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameterType"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public bool CanCreate(Type parameterType, Type dataType)
        {
            return 
                parameterType == dataType &&
                typeof(AbstractPageData).IsAssignableFrom(dataType);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parameterType"></param>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public RouteParameter Create(string name, Type parameterType, Type dataType)
        {
            return new RouteParameter
            {
                Name = name,
                GetValueAction = (s, c) =>
                {
                    var page = _documentSession.Load<Page>(c);
                    var data = page.Data;
                    return data;
                }
            };
        }

        #endregion

    }

}

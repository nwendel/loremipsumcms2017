﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.Filters;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
    public class PageRouteForAttribute : ActionFilterAttribute, IContentRouteForAttribute
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataType"></param>
        public PageRouteForAttribute(Type dataType)
        {
            if (dataType == null)
            {
                throw new ArgumentNullException(nameof(dataType));
            }
            if (!typeof(AbstractPageData).GetTypeInfo().IsAssignableFrom(dataType))
            {
                throw new ArgumentException($"{dataType.FullName} must inherit from {typeof(AbstractPageData).FullName}", nameof(dataType));
            }

            DataType = dataType;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Type DataType { get; }

        #endregion

        #region On Action Executing

        // TODO: Should this be in a model binder instead?

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var parameters = context.ActionDescriptor.Parameters;
            var routeData = context.RouteData;

            foreach(var parameter in parameters)
            {
                if(routeData.HasParameterDataToken(parameter.Name))
                {
                    var value = routeData.GetParameterDataToken(parameter.Name);
                    context.ActionArguments[parameter.Name] = value;
                }
            }

            base.OnActionExecuting(context);
        }

        #endregion

    }

}

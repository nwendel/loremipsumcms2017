﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Internal;
using Microsoft.AspNetCore.Routing;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;
using LoremIpsumCMS.Web.Infrastructure.Http;
using Microsoft.Extensions.Options;
using System.Linq;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class PageRouteHandler : IRouter
    {

        #region Dependencies

        private readonly IHostAccessor _hostAccessor;
        private readonly IDocumentSession _documentSession;
        private readonly IContentRouteInfoCache _contentRouteInfoCache;
        private readonly MvcRouteHandler _mvcRouteHandler;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="contentRouteInfoCache"></param>
        /// <param name="hostAccessor"></param>
        /// <param name="mvcRouteHandler"></param>
        /// <param name="options"></param>
        public PageRouteHandler(
            IDocumentSession documentSession,
            IContentRouteInfoCache contentRouteInfoCache,
            IHostAccessor hostAccessor,
            MvcRouteHandler mvcRouteHandler,
            IOptions<LoremIpsumOptions> options)
        {
            _documentSession = documentSession;
            _contentRouteInfoCache = contentRouteInfoCache;
            _hostAccessor = hostAccessor;
            _mvcRouteHandler = mvcRouteHandler;
            _options = options;
        }

        #endregion

        #region Route Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task RouteAsync(RouteContext context)
        {
            var hostname = _hostAccessor.CurrentHost.Host;
            var path = context.HttpContext.Request.Path.Value;

            if (path.StartsWith($"/{_options.Value.AssetRootPath}/"))
            {
                return Task.CompletedTask;
            }

            var ignorePageRouteForExtensions = _options.Value.IgnorePageRouteForExtensions;
            if (ignorePageRouteForExtensions.Any(x => path.EndsWith(x)))
            {
                return Task.CompletedTask;
            }

            var siteOverview = _documentSession.LoadBy<SiteOverview>(x => x.Hostname(hostname));
            if(siteOverview == null)
            {
                return Task.CompletedTask;
            }
            var siteReference = new SiteReference(siteOverview);
            context.RouteData.UpdateDataTokens(siteReference);

            var pageOverview = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath(siteOverview.SiteId, path));
            if (pageOverview == null)
            {
                return Task.CompletedTask;
            }

            var dataType = pageOverview.DataTypeFullName;
            var routeInfo = _contentRouteInfoCache.GetRouteInfo(dataType);
            context.RouteData.Values["controller"] = routeInfo.ControllerName;
            context.RouteData.Values["action"] = routeInfo.ActionName;
            // TODO: Area?

            var pageReference = new PageReference(pageOverview);
            context.RouteData.UpdateDataTokens(siteReference, pageReference, routeInfo.Parameters);

            return _mvcRouteHandler.RouteAsync(context);
        }

        #endregion

        #region Get Virtual Path

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            // TODO: Anything here?
            return null;
        }

        #endregion

    }

}

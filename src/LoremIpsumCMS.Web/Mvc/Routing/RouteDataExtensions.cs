﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Microsoft.AspNetCore.Routing;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Mvc.Routing.Internal.Parameters;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public static class RouteDataExtensions
    {

        #region Fields

        private const string _keyPrefix = "LoremIpsumCMS:";
        private const string _parameterKeyPrefix = _keyPrefix + "Parameters:";

        private const string _siteReferenceKey = _keyPrefix + nameof(SiteReference);
        private const string _pageReferenceKey = _keyPrefix + nameof(PageReference);
        private const string _partialReferenceKey = _keyPrefix + nameof(PartialReference);

        #endregion

        #region Update Data Tokens

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="siteReference"></param>
        public static void UpdateDataTokens(this RouteData self, SiteReference siteReference)
        {
            self.DataTokens[_siteReferenceKey] = siteReference;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="siteReference"></param>
        /// <param name="pageReference"></param>
        /// <param name="parameters"></param>
        public static void UpdateDataTokens(this RouteData self, SiteReference siteReference, PageReference pageReference, IEnumerable<RouteParameter> parameters)
        {
            self.DataTokens[_siteReferenceKey] = siteReference;
            self.DataTokens[_pageReferenceKey] = pageReference;
            UpdateParameterDataTokens(self, siteReference.SiteId, pageReference.PageId, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="routeData"></param>
        /// <param name="siteId"></param>
        /// <param name="contentId"></param>
        /// <param name="parameters"></param>
        private static void UpdateParameterDataTokens(RouteData routeData, string siteId, string contentId, IEnumerable<RouteParameter> parameters)
        {
            foreach (var parameter in parameters)
            {
                routeData.DataTokens[_parameterKeyPrefix + parameter.Name] = parameter.GetValue(siteId, contentId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="partialReference"></param>
        public static void UpdateDataTokens(this RouteData self, PartialReference partialReference)
        {
            self.DataTokens[_partialReferenceKey] = partialReference;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="routeValues"></param>
        public static void UpdateDataTokens(this RouteData self, RouteValueDictionary routeValues)
        {
            foreach(var routeValue in routeValues)
            {
                self.DataTokens[routeValue.Key] = routeValue.Value;
            }
        }

        #endregion

        #region Has Parameter Content

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool HasParameterDataToken(this RouteData self, string name)
        {
            var found = self.DataTokens.ContainsKey(_parameterKeyPrefix + name);
            return found;
        }

        #endregion

        #region Get Parameter Content

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static object GetParameterDataToken(this RouteData self, string name)
        {
            var content = self.DataTokens[_parameterKeyPrefix + name];
            return content;
        }

        #endregion

        #region Get Site Reference

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static SiteReference GetSiteReference(this RouteData self)
        {
            var siteReference = (SiteReference)self.DataTokens[_siteReferenceKey];
            return siteReference;
        }

        #endregion

        #region Get Page Reference

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static PageReference GetPageReference(this RouteData self)
        {
            var pageReference = (PageReference)self.DataTokens[_pageReferenceKey];
            return pageReference;
        }

        #endregion

        #region Has Partial Reference

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static bool HasPartialReference(this RouteData self)
        {
            var found = self.DataTokens.ContainsKey(_partialReferenceKey);
            return found;
        }

        #endregion

        #region Get Partial Reference

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        public static PartialReference GetPartialReference(this RouteData self)
        {
            var partialReference = (PartialReference)self.DataTokens[_partialReferenceKey];
            return partialReference;
        }

        #endregion

    }

}

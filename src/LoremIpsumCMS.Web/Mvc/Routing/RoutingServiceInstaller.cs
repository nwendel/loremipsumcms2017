﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.DependencyInjection;
using FluentRegistration;
using LoremIpsumCMS.Web.Mvc.Routing.Internal;
using LoremIpsumCMS.Web.Mvc.Routing.Internal.Parameters;

namespace LoremIpsumCMS.Web.Mvc.Routing
{

    /// <summary>
    /// 
    /// </summary>
    public class RoutingServiceInstaller : IServiceInstaller
    {

        #region Install

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void Install(IServiceCollection serviceCollection)
        {
            serviceCollection.Register(c => c
                .For<PageRouteHandler>()
                .ImplementedBy<PageRouteHandler>());

            serviceCollection.Register(c => c
                .For<AssetRouteHandler>()
                .ImplementedBy<AssetRouteHandler>());

            serviceCollection.Register(r => r
                .FromThisAssembly()
                .Where(c => c.InSameNamespaceAs<ContentRouteInfoCache>())
                .WithServices.AllInterfaces());

            serviceCollection.Register(r => r
                .FromThisAssembly()
                .Where(c => c.InSameNamespaceAs<RouteParameter>())
                .WithServices.AllInterfaces());
        }

        #endregion

    }

}

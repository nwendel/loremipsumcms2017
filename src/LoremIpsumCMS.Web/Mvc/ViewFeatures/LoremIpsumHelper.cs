﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Web.Infrastructure.Mvc.Rendering;
using LoremIpsumCMS.Web.Mvc.Rendering;
using LoremIpsumCMS.Web.Mvc.Routing;

namespace LoremIpsumCMS.Web.Mvc.ViewFeatures
{

    /// <summary>
    /// 
    /// </summary>
    public class LoremIpsumHelper<TModel> : ILoremIpsumHelper<TModel>
        where TModel : AbstractContentData
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly IHtmlHelper<TModel> _htmlHelper;
        private readonly IViewComponentHelper _viewComponentHelper;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="htmlHelper"></param>
        public LoremIpsumHelper(
            IDocumentSession documentSession,
            IHtmlHelper<TModel> htmlHelper)
        {
            _documentSession = documentSession;
            _htmlHelper = htmlHelper;
            _viewComponentHelper = htmlHelper.GetViewComponentHelper();
        }

        #endregion

        #region Helper Properties

        /// <summary>
        /// 
        /// </summary>
        IHtmlHelper ILoremIpsumHelper.Html => _htmlHelper;

        /// <summary>
        /// 
        /// </summary>
        public IHtmlHelper<TModel> Html => _htmlHelper;

        /// <summary>
        /// 
        /// </summary>
        public IViewComponentHelper Component => _viewComponentHelper;

        #endregion

        #region Content Properties

        /// <summary>
        /// 
        /// </summary>
        public Site Site
        {
            get
            {
                var routeData = _htmlHelper.ViewContext.RouteData;
                var siteReference = routeData.GetSiteReference();
                var site = _documentSession.Load<Site>(siteReference.SiteId);
                return site;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public IAbstractContent<AbstractContentData, AbstractExtensionProperties> Content
        {
            get
            {
                var routeData = _htmlHelper.ViewContext.RouteData;
                var contentData = routeData.HasPartialReference()
                    ? (IAbstractContent<AbstractContentData, AbstractExtensionProperties>)Partial
                    : Page;
                return contentData;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public AbstractContentData ContentData
        {
            get
            {
                return Content.Data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Page Page
        {
            get
            {
                var routeData = _htmlHelper.ViewContext.RouteData;
                var pageReference = routeData.GetPageReference();
                var page = _documentSession.Load<Page>(pageReference.PageId);
                return page;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public AbstractPageData PageData
        {
            get
            {
                return Page.Data;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Partial Partial
        {
            get
            {
                var routeData = _htmlHelper.ViewContext.RouteData;
                var partialReference = routeData.GetPartialReference();
                var partial = _documentSession.Load<Partial>(partialReference.PartialId);
                return partial;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public AbstractPartialData PartialData
        {
            get
            {
                return Partial.Data;
            }
        }

        #endregion

    }

}

﻿'use strict';

$(document).on("click", ".delete-asset", function () {
    var path = $(this).data('assetpath');

    $(".modal-body #Path").text(path);
    $("#deleteAssetForm-Path").val(path);
});
﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class IdentityUserCommandHandler :
        IHandleCommand<CreateIdentityUserCommand>,
        IHandleCommand<CreateIdentityUserExternalAccountCommand>,
        IHandleCommand<LoginIdentityUserCommand>
    {

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreateIdentityUserCommand command)
        {
            yield return new IdentityUserCreatedEvent
            {
                DisplayName = command.DisplayName,
                FullName = command.FullName,
                Email = command.Email,
                PasswordHash = command.PasswordHash,
                PermissionNames = command.PermissionNames,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Handle Create External Account

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreateIdentityUserExternalAccountCommand command)
        {
            yield return new IdentityUserExternalAccountCreatedEvent
            {
                DisplayName = command.DisplayName,
                FullName = command.FullName,
                Email = command.Email,
                LoginProvider = command.LoginProvider,
                LoginProviderKey = command.LoginProviderKey,
                PermissionNames = command.PermissionNames,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Handle Login

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(LoginIdentityUserCommand command)
        {
            yield return new IdentityUserLoginEvent
            {
                Id = command.Id,
                At = command.At
            };
        }

        #endregion

    }

}

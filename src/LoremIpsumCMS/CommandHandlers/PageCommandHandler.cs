﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Exceptions;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PageCommandHandler :
        IValidateCommand<CreatePageCommand>,
        IHandleCommand<CreatePageCommand>,
        IValidateCommand<UpdatePageCommand>,
        IHandleCommand<UpdatePageCommand>,
        IValidateCommand<DeletePageCommand>,
        IHandleCommand<DeletePageCommand>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public PageCommandHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Validate Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void Validate(CreatePageCommand command)
        {
            var siteId = command.SiteId;
            var pagePath = command.Path;

            var siteExists = _documentSession.ExistsBy<SiteOverview>(x => x.SiteId(siteId));
            if (!siteExists)
            {
                throw new SiteNotFoundException(siteId);
            }
            var pageExists = _documentSession.ExistsBy<PageOverview>(x => x.SiteIdAndPath(siteId, pagePath));
            if (pageExists)
            {
                throw new PageExistsException(siteId, pagePath);
            }
        }

        #endregion

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreatePageCommand command)
        {
            yield return new PageCreatedEvent
            {
                SiteId = command.SiteId,
                Path = command.Path,
                Title = command.Title,
                MetaKeywords = command.MetaKeywords,
                MetaDescription = command.MetaDescription,
                Data = command.Data,
                ExtensionProperties = command.ExtensionProperties,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Validate Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void Validate(UpdatePageCommand command)
        {
            var pageId = command.Id;
            var page = _documentSession.Load<Page>(pageId);
            if (page == null)
            {
                throw new PageNotFoundException(pageId);
            }

            var pagePath = command.Path;
            var siteId = page.SiteId;
            var otherPageOverview = _documentSession.LoadBy<PageOverview>(x => x.SiteIdAndPath(siteId, pagePath));

            if (otherPageOverview != null && otherPageOverview.Id != pageId)
            {
                throw new PageExistsException(siteId, pagePath);
            }
        }

        #endregion

        #region Handle Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(UpdatePageCommand command)
        {
            yield return new PageUpdatedEvent
            {
                Id = command.Id,
                Path = command.Path,
                Title = command.Title,
                MetaKeywords = command.MetaKeywords,
                MetaDescription = command.MetaDescription,
                Data = command.Data,
                ExtensionProperties = command.ExtensionProperties,
                UpdatedAt = command.UpdatedAt
            };
        }

        #endregion

        #region Validate Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void Validate(DeletePageCommand command)
        {
            var pageId = command.Id;
            var page = _documentSession.Load<Page>(pageId);
            if (page == null)
            {
                throw new PageNotFoundException(pageId);
            }
        }

        #endregion

        #region Handle Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(DeletePageCommand command)
        {
            yield return new PageDeletedEvent
            {
                Id = command.Id
            };
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialCommandHandler :
        IValidateCommand<CreatePartialCommand>,
        IHandleCommand<CreatePartialCommand>,
        IValidateCommand<UpdatePartialCommand>,
        IHandleCommand<UpdatePartialCommand>,
        IValidateCommand<DeletePartialCommand>,
        IHandleCommand<DeletePartialCommand>
    {

        #region Validate Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public void Validate(CreatePartialCommand command)
        {
        }

        #endregion

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreatePartialCommand command)
        {
            yield return new PartialCreatedEvent
            {
                SiteId = command.SiteId,
                Name = command.Name,
                Data = command.Data,
                ExtensionProperties = command.ExtensionProperties,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Validate Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public void Validate(UpdatePartialCommand command)
        {
        }

        #endregion

        #region Handle Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(UpdatePartialCommand command)
        {
            yield return new PartialUpdatedEvent
            {
                Id = command.Id,
                Name = command.Name,
                Data = command.Data,
                ExtensionProperties = command.ExtensionProperties,
                UpdatedAt = command.UpdatedAt
            };
        }

        #endregion

        #region Validate Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public void Validate(DeletePartialCommand command)
        {
        }

        #endregion

        #region Handle Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(DeletePartialCommand command)
        {
            yield return new PartialDeletedEvent
            {
                Id = command.Id
            };
        }

        #endregion

    }

}

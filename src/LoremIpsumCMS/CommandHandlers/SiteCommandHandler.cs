﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Exceptions;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.CommandHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteCommandHandler :
        IValidateCommand<CreateSiteCommand>,
        IHandleCommand<CreateSiteCommand>,
        IValidateCommand<UpdateSiteCommand>,
        IHandleCommand<UpdateSiteCommand>,
        IValidateCommand<DeleteSiteCommand>,
        IHandleCommand<DeleteSiteCommand>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public SiteCommandHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Validate Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void Validate(CreateSiteCommand command)
        {
            var siteNameSlug = command.Name.Slugify();
            var siteExists = _documentSession.ExistsBy<SiteOverview>(x => x.NameSlug(siteNameSlug));
            if(siteExists)
            {
                throw new SiteExistsException(siteNameSlug);
            }

            foreach(var host in command.Hosts)
            {
                var otherHost = _documentSession.LoadBy<Host>(x => x.Name(host.Name));
                if(otherHost != null)
                {
                    throw new HostExistsException(host.Name);
                }
            }
        }

        #endregion

        #region Handle Create

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(CreateSiteCommand command)
        {
            yield return new SiteCreatedEvent
            {
                Name = command.Name,
                Hosts = command.Hosts,
                ExtensionProperties = command.ExtensionProperties,
                UserPermissions = command.UserPermissions,
                CreatedAt = command.CreatedAt
            };
        }

        #endregion

        #region Validate Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void Validate(UpdateSiteCommand command)
        {
            var siteId = command.Id;
            var site = _documentSession.Load<Site>(siteId);
            if(site == null)
            {
                throw new SiteNotFoundException(siteId);
            }

            var siteNameSlug = command.Name.Slugify();
            var otherSiteOverview = _documentSession.LoadBy<SiteOverview>(x => x.NameSlug(siteNameSlug));
            if (otherSiteOverview != null && otherSiteOverview.SiteId != siteId)
            {
                throw new SiteExistsException(siteNameSlug);
            }

            foreach (var host in command.Hosts)
            {
                var hostname = host.Name;
                var otherHost = _documentSession.LoadBy<Host>(x => x.Name(hostname));
                if (otherHost != null && otherHost.SiteId != siteId)
                {
                    throw new HostExistsException(hostname);
                }
            }
        }

        #endregion

        #region Handle Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(UpdateSiteCommand command)
        {
            yield return new SiteUpdatedEvent
            {
                Id = command.Id,
                Name = command.Name,
                Hosts = command.Hosts,
                ExtensionProperties = command.ExtensionProperties,
                UserPermissions = command.UserPermissions,
                UpdatedAt = command.UpdatedAt,
            };
        }

        #endregion

        #region Validate Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        public void Validate(DeleteSiteCommand command)
        {
            var siteId = command.Id;
            var site = _documentSession.Load<Site>(siteId);
            if (site == null)
            {
                throw new SiteNotFoundException(siteId);
            }
        }

        #endregion

        #region Handle Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public IEnumerable<AbstractEvent> Handle(DeleteSiteCommand command)
        {
            yield return new SiteDeletedEvent
            {
                Id = command.Id
            };
        }

        #endregion

    }

}

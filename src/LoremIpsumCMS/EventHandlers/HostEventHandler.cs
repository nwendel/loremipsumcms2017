﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Model.UniqueConstraints;

namespace LoremIpsumCMS.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class HostEventHandler :
        ISubscribeTo<SiteCreatedEvent>,
        ISubscribeTo<SiteUpdatedEvent>,
        ISubscribeTo<SiteDeletedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public HostEventHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Handle Site Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteCreatedEvent @event)
        {
            foreach (var siteHost in @event.Hosts)
            {
                var host = new Host(siteHost.Name, @event.Id, siteHost.IsAdmin);
                _documentSession.Store(host);
            }
        }

        #endregion

        #region Handle Site Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteUpdatedEvent @event)
        {
            _documentSession.Advanced.DeleteByQuery<Host, HostIndex>(x => x.SiteId == @event.Id);
            _documentSession.Advanced.DeleteByQuery<SiteAwareUniqueConstraint, SiteAwareUniqueConstrantIndex>(x => x.SiteId == @event.Id && x.EntityName == nameof(Host));

            foreach (var siteHost in @event.Hosts)
            {
                var host = new Host(siteHost.Name, @event.Id, siteHost.IsAdmin);
                _documentSession.Store(host);
            }
        }

        #endregion

        #region Handle Site Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteDeletedEvent @event)
        {
            _documentSession.Advanced.DeleteByQuery<Host, HostIndex>(x => x.SiteId == @event.Id);
            _documentSession.Advanced.DeleteByQuery<SiteAwareUniqueConstraint, SiteAwareUniqueConstrantIndex>(x => x.SiteId == @event.Id && x.EntityName == nameof(Host));
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Indexes.UniqueConstraints;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Model.UniqueConstraints;
using LoremIpsumCMS.Queries;

namespace LoremIpsumCMS.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialOverviewEventHandler :
        ISubscribeTo<PartialCreatedEvent>,
        ISubscribeTo<PartialUpdatedEvent>,
        ISubscribeTo<PartialDeletedEvent>,
        ISubscribeTo<SiteDeletedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public PartialOverviewEventHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Handle Partial Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PartialCreatedEvent @event)
        {
            var partialOverview = new PartialOverview(
                @event.Id,
                @event.SiteId,
                @event.Name,
                @event.Data.GetType().FullName,
                @event.CreatedAt);
            _documentSession.Store(partialOverview);
        }

        #endregion

        #region Handle Page Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PartialUpdatedEvent @event)
        {
            var partialOverview = _documentSession.LoadBy<PartialOverview>(x => x.PartialId(@event.Id));
            partialOverview.Update(@event.Name, @event.UpdatedAt);
        }

        #endregion

        #region Handle Page Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(PartialDeletedEvent @event)
        {
            var partialOverview = _documentSession.LoadBy<PartialOverview>(x => x.PartialId(@event.Id));
            _documentSession.Delete(partialOverview);
        }

        #endregion

        #region Handle Site Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteDeletedEvent @event)
        {
            _documentSession.Advanced.DeleteByQuery<PartialOverview, PartialOverviewIndex>(x => x.SiteId == @event.Id);
            _documentSession.Advanced.DeleteByQuery<SiteAwareUniqueConstraint, SiteAwareUniqueConstrantIndex>(x => x.SiteId == @event.Id && x.EntityName == nameof(PartialOverview));
        }

        #endregion

    }

}

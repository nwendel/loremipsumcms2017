﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Indexes;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.EventHandlers
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteResourcePermissionsEventHandler :
        ISubscribeTo<SiteCreatedEvent>,
        ISubscribeTo<SiteUpdatedEvent>,
        ISubscribeTo<SiteDeletedEvent>
    {

        #region Dependencies

        private readonly IDocumentSession _documentSession;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        public SiteResourcePermissionsEventHandler(IDocumentSession documentSession)
        {
            _documentSession = documentSession;
        }

        #endregion

        #region Handle Site Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteCreatedEvent @event)
        {
            foreach (var userPermissions in @event.UserPermissions)
            {
                var siteResourcePermissions = new SiteResourcePermissions(@event.Id, userPermissions.UserId, userPermissions.PermissionNames);
                _documentSession.Store(siteResourcePermissions);
            }
        }

        #endregion

        #region Handle Site Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteUpdatedEvent @event)
        {
            _documentSession.Advanced.DeleteByQuery<SiteResourcePermissions, SiteResourcePermissionsIndex>(x => x.SiteId == @event.Id);
            foreach (var userPermissions in @event.UserPermissions)
            {
                var siteResourcePermissions = new SiteResourcePermissions(@event.Id, userPermissions.UserId, userPermissions.PermissionNames);
                _documentSession.Store(siteResourcePermissions);
            }
        }

        #endregion

        #region Handle Site Deleted

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Handle(SiteDeletedEvent @event)
        {
            _documentSession.Advanced.DeleteByQuery<SiteResourcePermissions, SiteResourcePermissionsIndex>(x => x.SiteId == @event.Id);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Indexes;
using LoremIpsumCMS.Infrastructure.Initialization;

namespace LoremIpsumCMS.Indexes
{

    /// <summary>
    /// 
    /// </summary>
    public class IndexInitializer : IInitializer
    {

        #region Dependencies

        private readonly IEnumerable<AbstractIndexCreationTask> _indexCreationTasks;
        private readonly IDocumentStore _documentStore;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexCreationTasks"></param>
        /// <param name="documentStore"></param>
        /// <param name="options"></param>
        public IndexInitializer(
            IEnumerable<AbstractIndexCreationTask> indexCreationTasks,
            IDocumentStore documentStore,
            IOptions<LoremIpsumOptions> options)
        {
            _indexCreationTasks = indexCreationTasks;
            _documentStore = documentStore;
            _options = options;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            var databaseName = _options.Value.RavenDatabase;
            foreach (var indexCreationTask in _indexCreationTasks)
            {
                indexCreationTask.Execute(_documentStore, database: databaseName);
            }
        }

        #endregion

    }

}

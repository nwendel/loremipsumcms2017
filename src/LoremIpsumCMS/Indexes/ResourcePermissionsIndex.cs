﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Raven.Client.Documents.Indexes;
using LoremIpsumCMS.Model;
using LoremIpsumCMS.Services;

namespace LoremIpsumCMS.Indexes
{

    /// <summary>
    /// 
    /// </summary>
    public class ResourcePermissionsIndex : AbstractMultiMapIndexCreationTask<ResourcePermissionsIndex.Result>
    {

        #region Fields

        private readonly MethodInfo _addMapMethodInfo = typeof(ResourcePermissionsIndex).GetMethod(nameof(AddMap), BindingFlags.Instance | BindingFlags.NonPublic);

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public ResourcePermissionsIndex(IReflectionService reflectionService)
        {
            var assemblies = reflectionService.FindApplicationAndLoremIpsumAssemblies();
            var resourcePermissionsTypes = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(AbstractResourcePermissions).IsAssignableFrom(x))
                .Where(x => !x.IsGenericTypeDefinition && !x.IsAbstract && !x.IsInterface)
                .ToList();
            foreach (var resourcePermissionsType in resourcePermissionsTypes)
            {
                AddMapFor(resourcePermissionsType, sitePermissions => sitePermissions.Select(userSitePermissions => new Result
                {
                    ResourceId = userSitePermissions.ResourceId,
                    UserId = userSitePermissions.UserId,
                    PermissionNames = userSitePermissions.PermissionNames
                }));
            }


        }

        #endregion

        #region Add Map For

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="expression"></param>
        private void AddMapFor(Type type, Expression<Func<IEnumerable<IResourcePermissions>, IEnumerable>> expression)
        {
            var genericEnumerable = typeof(IEnumerable<>).MakeGenericType(type);
            var delegateType = typeof(Func<,>).MakeGenericType(genericEnumerable, typeof(IEnumerable));
            var lambdaExpression = Expression.Lambda(delegateType, expression.Body, Expression.Parameter(genericEnumerable, expression.Parameters[0].Name));

            _addMapMethodInfo.MakeGenericMethod(type).Invoke(this, new object[] {lambdaExpression});
        }

        #endregion

        #region Result

        /// <summary>
        /// 
        /// </summary>
        public class Result
        {

            /// <summary>
            /// 
            /// </summary>
            public string ResourceId { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public IEnumerable<string> PermissionNames { get; set; }
            
        }

        #endregion

    }

}

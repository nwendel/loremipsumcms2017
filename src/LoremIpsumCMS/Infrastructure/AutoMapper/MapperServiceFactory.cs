﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using FluentRegistration;
using LoremIpsumCMS.Infrastructure.Initialization;

namespace LoremIpsumCMS.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public class MapperServiceFactory : 
        IServiceFactory<IMapper>,
        IInitializer
    {

        #region Dependencies

        private readonly IEnumerable<Profile> _profiles;
        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Fields

        private IMapper _mapper;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="profiles"></param>
        /// <param name="serviceProvider"></param>
        public MapperServiceFactory(
            IEnumerable<Profile> profiles,
            IServiceProvider serviceProvider)
        {
            _profiles = profiles;
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        public void Initialize()
        {
            var mapperConfiguration = new MapperConfiguration(x =>
            {
                foreach(var profile in _profiles)
                {
                    x.AddProfile(profile);
                }
                x.ConstructServicesUsing(t =>
                {
                    var service = _serviceProvider.GetRequiredService(t);
                    return service;
                });
            });
            mapperConfiguration.AssertConfigurationIsValid();

            _mapper = mapperConfiguration.CreateMapper();
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IMapper Create()
        {
            if(_mapper == null)
            {
                throw new LoremIpsumException($"{nameof(MapperServiceFactory)} is not initialized");
            }

            return _mapper;
        }


        #endregion

    }

}

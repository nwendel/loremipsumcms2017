﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using AutoMapper;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.AutoMapper
{

    /// <summary>
    /// 
    /// </summary>
    public static class ObjectMapToExtensions
    {

        #region Map To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="mapper"></param>
        /// <returns></returns>
        public static T MapTo<T>(this object self, IMapper mapper)
        {
            if (self == null)
            {
                return default(T);
            }
            return mapper.Map<T>(self);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="mapper"></param>
        /// <returns></returns>
        public static T[] MapTo<T>(this object[] self, IMapper mapper)
        {
            if (self == null)
            {
                return null;
            }
            return self
                .Select(x => x.MapTo<T>(mapper))
                .ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="self"></param>
        /// <param name="mapper"></param>
        /// <returns></returns>
        public static IEnumerable<T> MapTo<T>(this IEnumerable<object> self, IMapper mapper)
        {
            if (self == null)
            {
                return null;
            }
            return self.Select(x => x.MapTo<T>(mapper));
        }

        #endregion


    }

}

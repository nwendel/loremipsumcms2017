﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Security.Cryptography;
using LoremIpsumCMS.Infrastructure.IO;

namespace LoremIpsumCMS.Infrastructure.Collections.Specialized
{

    /// <summary>
    /// 
    /// </summary>
    public class TypeIdentifierLookup
    {

        #region Fields

        private readonly MD5 _md5 = MD5.Create();
        private readonly Dictionary<Type, string> _typeToIdentifier = new Dictionary<Type, string>();
        private readonly Dictionary<string, Type> _identifierToType = new Dictionary<string, Type>();

        #endregion

        #region Add

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        public void Add(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }
            if (type.GetTypeInfo().IsAbstract)
            {
                throw new ArgumentException("Cannot be abstract", nameof(type));
            }
            if (type.GetTypeInfo().IsGenericTypeDefinition)
            {
                throw new ArgumentException("Cannot be generic type definition", nameof(type));
            }
            if (_typeToIdentifier.ContainsKey(type))
            {
                throw new ArgumentException($"{type.FullName} is already added", nameof(type));
            }

            var typeName = type.AssemblyQualifiedName;
            var stream = new StringStream(typeName);
            var hashBytes = _md5.ComputeHash(stream);
            var identifier = BitConverter.ToString(hashBytes).Replace("-", "");

            _identifierToType.Add(identifier, type);
            _typeToIdentifier.Add(type, identifier);
        }

        #endregion

        #region Add Range

        /// <summary>
        /// 
        /// </summary>
        /// <param name="types"></param>
        public void AddRange(IEnumerable<Type> types)
        {
            if(types == null)
            {
                throw new ArgumentNullException(nameof(types));
            }

            foreach(var type in types)
            {
                Add(type);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public Type this[string identifier] => _identifierToType[identifier];

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string this[Type type] => _typeToIdentifier[type];

        #endregion

    }

}

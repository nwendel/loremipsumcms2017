﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public static class DateTimeExtensions
    {

        #region To Relative String

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string ToRelativeString(this DateTime self)
        {
            if (self.Date == DateTime.Today)
            {
                return string.Format("Today {0:HH:mm}", self);
            }
            if (self.Date == DateTime.Today.AddDays(-1))
            {
                return string.Format("Yesterday {0:HH:mm}", self);
            }
            return self.ToString("yyyy-MM-dd HH:mm");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string ToRelativeString(this DateTime? self)
        {
            return self.HasValue
                ? self.Value.ToRelativeString()
                : string.Empty;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Infrastructure.EventStore
{

    /// <summary>
    /// 
    /// </summary>
    public class PersistedEvent : AbstractRootEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PersistedEvent()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aggregateType"></param>
        /// <param name="event"></param>
        public PersistedEvent(string aggregateType, AbstractEvent @event) : this()
        {
            if (aggregateType == null)
            {
                throw new ArgumentNullException(nameof(aggregateType));
            }
            if (@event == null)
            {
                throw new ArgumentNullException(nameof(@event));
            }

            At = DateTime.Now;
            AggregateType = aggregateType;
            Event = @event;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public DateTime At { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string AggregateId => Event.Id;

        /// <summary>
        /// 
        /// </summary>
        public string AggregateType { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string EventType => Event.GetType().Name;

        /// <summary>
        /// 
        /// </summary>
        public AbstractEvent Event { get; protected set; }

        #endregion

    }

}

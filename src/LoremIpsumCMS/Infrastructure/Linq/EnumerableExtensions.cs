﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoremIpsumCMS.Infrastructure.Linq
{

    /// <summary>
    /// 
    /// </summary>
    public static class EnumerableExtensions
    {

        #region Flatten

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <param name="getChildren"></param>
        /// <returns></returns>
        public static IEnumerable<TItem> Flatten<TItem>(this IEnumerable<TItem> self, Func<TItem, IEnumerable<TItem>> getChildren)
        {
            // TODO: Needs to be like this because public method is used recursivly, split it up?
            if(self == null)
            {
                return Enumerable.Empty<TItem>();
            }

            return self.SelectMany(x => getChildren(x)
                .Flatten(getChildren))
                .Concat(self);
        }

        #endregion

        #region None

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool None<TItem>(this IEnumerable<TItem> self)
        {
            return !self.Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="self"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool None<TSource>(this IEnumerable<TSource> self, Func<TSource, bool> predicate)
        {
            return !self.Any(predicate);
        }

        #endregion

        #region One

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool One<TItem>(this IEnumerable<TItem> self)
        {
            // TODO: Can be optimized
            return self.Count() == 1;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="self"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static bool One<TItem>(this IEnumerable<TItem> self, Func<TItem, bool> predicate)
        {
            // TODO: Can be optimized
            return self.Count(predicate) == 1;
        }

        #endregion

    }

}

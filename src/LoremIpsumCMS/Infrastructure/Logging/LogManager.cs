﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.Extensions.Logging;

namespace LoremIpsumCMS.Infrastructure.Logging
{

    /// <summary>
    /// 
    /// </summary>
    public static class LogManager
    {

        #region Logger Factory

        // TODO: What if the logger is replaced, shouldn't I use that instead?

        /// <summary>
        /// 
        /// </summary>
        public static ILoggerFactory LoggerFactory { get; } = new LoggerFactory();

        #endregion

        #region Create Logger

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ILogger CreateLogger<T>() => LoggerFactory.CreateLogger<T>();

        #endregion

        #region Create Current Class Logger

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static ILogger CreateCurrentClassLogger()
        {
            var stackTrace = new StackTrace();
            var stackFrame = stackTrace.GetFrame(1);
            var callingType = stackFrame.GetMethod().DeclaringType;

            var category = callingType.FullName;
            var logger = LoggerFactory.CreateLogger(category);
            return logger;
        }

        #endregion

    }

}

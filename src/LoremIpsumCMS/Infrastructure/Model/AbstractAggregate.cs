﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Reflection;

namespace LoremIpsumCMS.Infrastructure.Model
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractAggregate : AbstractRootEntity
    {

        #region Apply Event

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void ApplyEvent(AbstractEvent @event)
        {
            var applierType = typeof(IApplyEvent<>).MakeGenericType(@event.GetType());
            if (!applierType.IsAssignableFrom(GetType()))
            {
                throw new InvalidOperationException($"Aggregate {GetType().Name} does not know how to apply event {@event.GetType().Name}");
            }

            var method = applierType.GetMethod(nameof(IApplyEvent<AbstractEvent>.Apply));
            method.InvokeAndUnwrapException(this, new object[] { @event });
        }

        #endregion

    }

}

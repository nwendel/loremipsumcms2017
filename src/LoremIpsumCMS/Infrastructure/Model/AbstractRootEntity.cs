﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Newtonsoft.Json;

namespace LoremIpsumCMS.Infrastructure.Model
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractRootEntity : AbstractEntity
    {

        #region Id

        /// <summary>
        /// 
        /// </summary>
        public string Id { get; protected set; }

        #endregion

        #region Equals

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var other = obj as AbstractRootEntity;
            return Equals(other);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public virtual bool Equals(AbstractRootEntity other)
        {
            if (other == null)
            {
                return false;
            }
            if (!GetType().IsInstanceOfType(other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (other.IsTransient && IsTransient)
            {
                return ReferenceEquals(other, this);
            }

            return Id.Equals(other.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        private bool IsTransient => Equals(Id, null);

        #endregion

        #region Get Hash Code

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return IsTransient
                ? base.GetHashCode()
                : Id.GetHashCode();
        }

        #endregion

    }

}

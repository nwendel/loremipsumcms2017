﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS.Infrastructure.Logging;

namespace LoremIpsumCMS.Infrastructure.Modules
{

    /// <summary>
    /// 
    /// </summary>
    public class ModuleLoader
    {

        #region Depencies

        private readonly ILogger _logger = LogManager.CreateLogger<ModuleLoader>();
        private readonly DependencyContext _dependencyContext = DependencyContext.Default;

        #endregion

        #region Load All

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Type> LoadAll()
        {
            // TODO: Something smarter here how I detect module assemblies?

            var complieLibraries = _dependencyContext.CompileLibraries;
            var candidateLibraries = complieLibraries
                .Where(x => x.Name == "LoremIpsumCMS" || x.Dependencies.Any(d => d.Name == "LoremIpsumCMS" || d.Name == "LoremIpsumCMS.Web"))
                .ToList();
            var candidateAssemblies = candidateLibraries
                .Select(x => Assembly.Load(new AssemblyName(x.Name)))
                .ToList();

            foreach (var candidateAssembly in candidateAssemblies)
            {
                _logger.LogDebug("Found candidate module assembly: {0}", candidateAssembly.FullName);
            }

            // TODO: Check here that same assembly does not contain multiple modules?

            var moduleTypes = candidateAssemblies
               .SelectMany(x => x.GetTypes())
               .Where(x => !x.GetTypeInfo().IsAbstract && typeof(IModule).IsAssignableFrom(x))
               .ToList();
            foreach (var moduleType in moduleTypes)
            {
                _logger.LogDebug("Found module: {0}", moduleType.GetTypeInfo().FullName);
            }

            return moduleTypes;
        }

        #endregion

    }

}

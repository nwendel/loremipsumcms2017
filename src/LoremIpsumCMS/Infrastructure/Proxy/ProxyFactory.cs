﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace LoremIpsumCMS.Infrastructure.Proxy
{

    /// <summary>
    /// 
    /// </summary>
    public class ProxyFactory
    {

        #region Fields

        private static readonly IDictionary<Type, Type> _proxyTypeCache = new Dictionary<Type, Type>();

        #endregion

        #region Create Type

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static Type CreateType<T>()
        {
            if(!typeof(T).GetTypeInfo().IsInterface)
            {
                throw new ArgumentException("T must be an interface");
            }

            // TODO: Change this to not lock when found in cache?

            lock (_proxyTypeCache)
            {
                var found = _proxyTypeCache.TryGetValue(typeof(T), out Type proxyType);
                if (found)
                {
                    return proxyType;
                }

                var typeBuilder = CreateTypeBuilder<T>();
                var creatorFieldBuilder = typeBuilder.DefineField("_creator", typeof(Func<T>), FieldAttributes.Private);
                DefineConstructor<T>(typeBuilder, creatorFieldBuilder);
                ImplementProxyTargetInterface<T>(typeBuilder, creatorFieldBuilder);

                ImplementInterface<T>(typeBuilder, creatorFieldBuilder);
                foreach (var interfaceType in typeof(T).GetInterfaces())
                {
                    ImplementInterface(typeBuilder, creatorFieldBuilder, interfaceType);
                }

                proxyType = typeBuilder.CreateTypeInfo().AsType();
                _proxyTypeCache.Add(typeof(T), proxyType);
                return proxyType;
            }
        }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="creator"></param>
        /// <returns></returns>
        public static T Create<T>(Func<T> creator)
            where T : class
        {
            var proxyType = CreateType<T>();
            return (T)Activator.CreateInstance(proxyType, creator);
        }

        #endregion

        #region Create Type Builder

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private static TypeBuilder CreateTypeBuilder<T>()
        {
            var typeName = typeof(T).Name;

            var assemblyBuilder = AssemblyBuilder.DefineDynamicAssembly(new AssemblyName(typeName + "$Proxy"), AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule(typeName+"$Proxy");
            var typeBuilder = moduleBuilder.DefineType(typeName+"$Proxy", TypeAttributes.Public | TypeAttributes.Class);

            // TODO: This does not work for generic types?

            return typeBuilder;
        }

        #endregion

        #region Define Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="typeBuilder"></param>
        /// <param name="creatorFieldBuilder"></param>
        private static void DefineConstructor<T>(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder)
        {
            var creatorType = typeof(Func<T>);

            var constructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.Public, CallingConventions.HasThis, new[] { creatorType });
            var parameterBuilder = constructorBuilder.DefineParameter(1, ParameterAttributes.None, "creator");
            var constructorEmitter = constructorBuilder.GetILGenerator();

            constructorEmitter.Emit(OpCodes.Ldarg_0);
            constructorEmitter.Emit(OpCodes.Ldarg_1);
            constructorEmitter.Emit(OpCodes.Stfld, creatorFieldBuilder);
            constructorEmitter.Emit(OpCodes.Ret);
        }

        #endregion

        #region Implement Proxy Target Interface

        /// <summary>
        /// 
        /// </summary>
        public static void ImplementProxyTargetInterface<T>(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder)
        {
            var proxyTargetType = typeof(IProxyTarget<T>);
            var getMethod = proxyTargetType.GetProperty(nameof(IProxyTarget<T>.Target)).GetGetMethod();

            typeBuilder.AddInterfaceImplementation(proxyTargetType);
            var methodBuilder = typeBuilder.DefineMethod(getMethod.Name, MethodAttributes.Public | MethodAttributes.Virtual, typeof(T), Type.EmptyTypes);
            typeBuilder.DefineMethodOverride(methodBuilder, getMethod);

            var ilGenerator = methodBuilder.GetILGenerator();
            ilGenerator.Emit(OpCodes.Ldarg_0);
            ilGenerator.Emit(OpCodes.Ldfld, creatorFieldBuilder);
            ilGenerator.EmitCall(OpCodes.Callvirt, typeof(Func<T>).GetTypeInfo().GetMethod(nameof(Func<T>.Invoke)), null);
            ilGenerator.Emit(OpCodes.Ret);
        }

        #endregion

        #region Implement Interface

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="typeBuilder"></param>
        /// <param name="creatorFieldBuilder"></param>
        private static void ImplementInterface<T>(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder)
        {
            ImplementInterface(typeBuilder, creatorFieldBuilder, typeof(T));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="typeBuilder"></param>
        /// <param name="creatorFieldBuilder"></param>
        private static void ImplementInterface(TypeBuilder typeBuilder, FieldBuilder creatorFieldBuilder, Type type)
        {
            var invokeMethodInfo = typeof(Func<>).MakeGenericType(new[] { type }).GetTypeInfo().GetMethod(nameof(Func<object>.Invoke));

            typeBuilder.AddInterfaceImplementation(type);
            foreach (var methodInfo in type.GetMethods())
            {
                var parameterTypes = methodInfo.GetParameters().Select(x => x.ParameterType).ToArray();
                var methodBuilder = typeBuilder.DefineMethod(methodInfo.Name, MethodAttributes.Public | MethodAttributes.Virtual, methodInfo.ReturnType, parameterTypes);
                CopyGenericArguments(methodBuilder, methodInfo.GetGenericArguments());
                typeBuilder.DefineMethodOverride(methodBuilder, methodInfo);

                var ilGenerator = methodBuilder.GetILGenerator();
                ilGenerator.Emit(OpCodes.Ldarg_0);
                ilGenerator.Emit(OpCodes.Ldfld, creatorFieldBuilder);
                ilGenerator.EmitCall(OpCodes.Callvirt, invokeMethodInfo, null);
                for (var ix = 0; ix < parameterTypes.Length; ix++)
                {
                    ilGenerator.Emit(OpCodes.Ldarg_S, ix + 1);
                }
                ilGenerator.EmitCall(OpCodes.Callvirt, methodInfo, null);
                ilGenerator.Emit(OpCodes.Ret);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodBuilder"></param>
        /// <param name="genericArguments"></param>
        private static void CopyGenericArguments(MethodBuilder methodBuilder, Type[] genericArguments)
        {
            if(genericArguments.Length == 0)
            {
                return;
            }

            var genericTypeParameters = methodBuilder.DefineGenericParameters(genericArguments.Select(x => x.Name).ToArray());
            for (var ix = 0; ix < genericTypeParameters.Length; ix++)
            {
                var genericArgumentTypeInfo = genericArguments[ix].GetTypeInfo();
                var constraints = genericArgumentTypeInfo.GetGenericParameterConstraints();
                var baseType = constraints.SingleOrDefault(x => x.GetTypeInfo().IsClass);
                var interfaces = constraints.Where(x => x.GetTypeInfo().IsInterface).ToArray();
                genericTypeParameters[ix].SetBaseTypeConstraint(baseType);
                genericTypeParameters[ix].SetInterfaceConstraints(interfaces);

                var attributes = genericArgumentTypeInfo.GenericParameterAttributes;
                genericTypeParameters[ix].SetGenericParameterAttributes(attributes);
            }
        }

        #endregion

    }

}

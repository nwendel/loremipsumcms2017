﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Model;
using Raven.Client.Documents.Indexes;
using Raven.Client.Documents.Operations;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public static class AdvancedSessionOperationsExtensions
    {

        #region Delete By Query

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TIndexCreator"></typeparam>
        /// <param name="self"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static void DeleteByQuery<TEntity, TIndexCreator>(this IAdvancedSessionOperations self, Expression<Func<TEntity, bool>> expression)
            where TEntity : AbstractRootEntity
            where TIndexCreator : AbstractIndexCreationTask, new()
        {
            var documentStore = self.DocumentStore;
            var databaseName = self.GetDatabaseName();
            var command = new DeleteByQueryOperation<TEntity, TIndexCreator>(expression);

            // TODO: Is something like this needed?
            //_documentSession.EnsureNonStaleIndex<Host, HostIndex>();
            var _ = self.DocumentQuery<TEntity, TIndexCreator>()
                .WaitForNonStaleResults()
                .Take(0)
                .ToList();

            var operation = documentStore.Operations.ForDatabase(databaseName).Send(command);

            // TODO: Wait here or outside?
            operation.WaitForCompletion();
        }

        #endregion

        #region Transaction Scope

        private const string _transactionScopeKey = "LoremIpsumCMS:InTransactionScope";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static bool GetIsInTransactionScope(this IAdvancedSessionOperations self)
        {
            var result = self.ExternalState.ContainsKey(_transactionScopeKey);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="transactionScope"></param>
        public static void SetTransactionScope(this IAdvancedSessionOperations self, TransactionScope transactionScope)
        {
            if (transactionScope != null)
            {
                self.ExternalState[_transactionScopeKey] = transactionScope;
            }
            else
            {
                self.ExternalState.Remove(_transactionScopeKey);
            }
        }

        #endregion

        #region Get Database Name

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetDatabaseName(this IAdvancedDocumentSessionOperations self)
        {
            // TODO: Is this correct? Is there a simpler way?
            return self.GetCurrentSessionNode().Result.Database;
        }

        #endregion

    }

}

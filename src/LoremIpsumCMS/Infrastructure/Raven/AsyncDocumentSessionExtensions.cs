﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Threading.Tasks;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public static class AsyncDocumentSessionExtensions
    {

        #region Exists By

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="self"></param>
        ///// <param name="by"></param>
        ///// <returns></returns>
        //public static async Task<bool> ExistsByAsync<T>(this IAsyncDocumentSession self, Func<AsyncDocumentSessionLoadBy<T>, Task<T>> by)
        //    where T : AbstractRootEntity
        //{
        //    var value = await self.LoadByAsync(by);
        //    return value != null;
        //}

        #endregion

        #region Load By

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="self"></param>
        ///// <param name="by"></param>
        ///// <returns></returns>
        //public static async Task<T> LoadByAsync<T>(this IAsyncDocumentSession self, Func<AsyncDocumentSessionLoadBy<T>, Task<T>> by)
        //    where T : AbstractRootEntity
        //{
        //    var loadBy = new AsyncDocumentSessionLoadBy<T>(self);
        //    var value = await by.Invoke(loadBy);
        //    return value;
        //}

        #endregion

    }

}

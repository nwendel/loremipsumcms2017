﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using Raven.Client.Documents.Session;
using Raven.Client.Documents.Indexes;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public static class DocumentSessionExtensions
    {

        #region Exists By

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static bool ExistsBy<TDocument>(this IDocumentSession self, Func<DocumentSessionLoadBy<TDocument>, TDocument> by)
            where TDocument : AbstractRootEntity
        {
            var value = self.LoadBy(by);
            return value != null;
        }

        #endregion

        #region Load By

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static TDocument LoadBy<TDocument>(this IDocumentSession self, Func<DocumentSessionLoadBy<TDocument>, TDocument> by)
            where TDocument : AbstractRootEntity
        {
            var loadBy = new DocumentSessionLoadBy<TDocument>(self);
            var value = by.Invoke(loadBy);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="by"></param>
        /// <returns></returns>
        public static TDocument[] LoadBy<TDocument>(this IDocumentSession self, Func<DocumentSessionLoadBy<TDocument>, TDocument[]> by)
            where TDocument : AbstractRootEntity
        {
            var loadBy = new DocumentSessionLoadBy<TDocument>(self);
            var value = by.Invoke(loadBy);
            return value;
        }

        #endregion

        #region Ensure Non Stale Index

        // TODO: Move this to Advanced?

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TIndexCreator"></typeparam>
        /// <param name="self"></param>
        public static void EnsureNonStaleIndex<T, TIndexCreator>(this IDocumentSession self)
            where TIndexCreator : AbstractIndexCreationTask, new()
        {
            self.Query<T, TIndexCreator>()
                .Customize(o => o.WaitForNonStaleResults())
                .Take(0)
                .ToList();
        }

        #endregion

    }

}

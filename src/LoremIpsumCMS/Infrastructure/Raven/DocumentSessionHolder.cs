﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.Extensions.Options;
using Raven.Client.Documents;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentSessionHolder : IDocumentSessionHolder
    {

        #region Dependencies

        private readonly IDocumentStore _documentStore;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentStore"></param>
        /// <param name="options"></param>
        public DocumentSessionHolder(
                IDocumentStore documentStore,
                IOptions<LoremIpsumOptions> options)
        {
            _documentStore = documentStore;
            _options = options;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IDocumentSession DocumentSession { get; private set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public IAsyncDocumentSession AsyncDocumentSession { get; private set; }

        #endregion

        #region Open Session

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public void OpenDocumentSession()
        {
            DocumentSession = _documentStore.OpenSession(_options.Value.RavenDatabase);
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //public void OpenAsyncDocumentSession()
        //{
        //    AsyncDocumentSession = _documentStore.OpenAsyncSession(_options.Value.RavenDatabase);
        //}

        #endregion

        #region Close Session

        /// <summary>
        /// 
        /// </summary>
        public void CloseDocumentSession()
        {
            DocumentSession?.Dispose();
            DocumentSession = null;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public void CloseAsyncDocumentSession()
        //{
        //    AsyncDocumentSession?.Dispose();
        //    AsyncDocumentSession = null;
        //}

        #endregion

    }

}

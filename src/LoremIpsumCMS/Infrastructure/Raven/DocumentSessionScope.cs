﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public class DocumentSessionScope : AbstractDisposable
    {

        #region Fields

        private readonly IDocumentSessionHolder _documentSessionHolder;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSessionHolder"></param>
        public DocumentSessionScope(IDocumentSessionHolder documentSessionHolder)
        {
            _documentSessionHolder = documentSessionHolder;

            _documentSessionHolder.OpenDocumentSession();
            //_ravenSessionHolder.OpenAsyncDocumentSession();
        }

        #endregion

        #region Managed Dispose

        /// <summary>
        /// 
        /// </summary>
        protected override void ManagedDispose()
        {
            _documentSessionHolder.CloseDocumentSession();
            //_ravenSessionHolder.CloseAsyncDocumentSession();

            base.ManagedDispose();
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Infrastructure.Raven
{

    /// <summary>
    /// 
    /// </summary>
    public interface IDocumentSessionHolder
    {
    
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        IDocumentSession DocumentSession { get; }
        
        ///// <summary>
        ///// 
        ///// </summary>
        //IAsyncDocumentSession AsyncDocumentSession { get; }

        #endregion

        #region Open Session

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        void OpenDocumentSession();
        
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        //void OpenAsyncDocumentSession();

        #endregion

        #region Close Session

        /// <summary>
        /// 
        /// </summary>
        void CloseDocumentSession();
        
        ///// <summary>
        ///// 
        ///// </summary>
        //void CloseAsyncDocumentSession();

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractUniqueConstraintAttribute : Attribute
    {

        #region Create Constraint Document

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        /// <param name="entity"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public UniqueConstraint CreateUniqueConstraintDocument(object entity, string documentId, string name, string value)
        {
            var entityName = entity.GetType().Name;
            // TODO: This should be done differently since I am making assumptions on the Id format
            var collectionName = documentId.Substring(0, documentId.IndexOf('/'));
            var id = $"UniqueConstraints/{collectionName}/{name}/{value}:";
            var uniqueConstraint = CreateInternal(entityName, id, entity, documentId);
            return uniqueConstraint;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        protected abstract UniqueConstraint CreateInternal(string entityName, string id, object entity, string documentId);

        #endregion

    }

}

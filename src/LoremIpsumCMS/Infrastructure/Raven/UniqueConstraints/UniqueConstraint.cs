﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraint : AbstractRootEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected UniqueConstraint()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="id"></param>
        /// <param name="documentId"></param>
        public UniqueConstraint(string entityName, string id, string documentId)
        {
            EntityName = entityName;
            Id = id;
            DocumentId = documentId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string EntityName { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string DocumentId { get; protected set; }

        #endregion

    }

}

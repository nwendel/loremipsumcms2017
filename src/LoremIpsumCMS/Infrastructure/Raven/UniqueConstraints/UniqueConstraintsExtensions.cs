﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Linq.Expressions;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public static class DocumentSessionExtensions
    {

        #region Load By Unique Constraint

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="keySelector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TDocument LoadByUniqueConstraint<TDocument>(this IDocumentSession self, Expression<Func<TDocument, object>> keySelector, string value)
            where TDocument : AbstractRootEntity
        {
            var collectionName = self.Advanced.DocumentStore.Conventions.GetCollectionName(typeof(TDocument));
            var name = GetPropropertyNameForKeySelector(keySelector);
            var id = $"uniqueconstraints/{collectionName}/{name}/{value}:";

            var uniqueConstraint = self
                .Include<UniqueConstraint>(x => x.DocumentId)
                .Load<UniqueConstraint>(id);
            if (uniqueConstraint == null)
            {
                return null;
            }
            var entity = self.Load<TDocument>(uniqueConstraint.DocumentId);
            if (entity == null)
            {
                // TODO: Is this correct?  A unique constraint points to a non-existing document
                throw new LoremIpsumException("TODO");
            }
            return entity;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TDocument"></typeparam>
        /// <param name="self"></param>
        /// <param name="keySelector"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static TDocument[] LoadByUniqueConstraint<TDocument>(this IDocumentSession self, Expression<Func<TDocument, object>> keySelector, string[] values)
            where TDocument : AbstractRootEntity
        {
            var collectionName = self.Advanced.DocumentStore.Conventions.GetCollectionName(typeof(TDocument));
            var name = GetPropropertyNameForKeySelector(keySelector);
            var ids = values
                .Select(value => $"uniqueconstraints/{collectionName}/{name}/{value}:")
                .ToArray();

            var uniqueConstraints = self
                .Include<UniqueConstraint>(x => x.DocumentId)
                .Load<UniqueConstraint>(ids);
            var documentIds = uniqueConstraints
                // TODO: Some other solution to this?
                .Select(x => x.Value?.DocumentId ?? "some-id-that-is-guaranteed-not-to-exist")
                .ToArray();

            var entity = self.Load<TDocument>(documentIds);

            // TODO: Similar check here as in non-array overload

            return entity.Values.ToArray();
        }

        #endregion

        #region Load By Unique Constraint Async

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="self"></param>
        ///// <param name="keySelector"></param>
        ///// <param name="value"></param>
        ///// <returns></returns>
        //public static async Task<T> LoadByUniqueConstraintAsync<T>(this IAsyncDocumentSession self, Expression<Func<T, object>> keySelector, string value)
        //    where T : AbstractRootEntity
        //{
        //    var collectionName = self.Advanced.DocumentStore.Conventions.GetCollectionName(typeof(T));
        //    var name = GetPropropertyNameForKeySelector(keySelector);
        //    var id = $"uniqueconstraints/{collectionName}/{name}/{value}";

        //    var uniqueConstraint = await self
        //        .Include<UniqueConstraint>(x => x.DocumentId)
        //        .LoadAsync(id);
        //    if (uniqueConstraint == null)
        //    {
        //        return null;
        //    }
        //    var entity = await self.LoadAsync<T>(uniqueConstraint.DocumentId);
        //    return entity;
        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="self"></param>
        ///// <param name="keySelector"></param>
        ///// <param name="values"></param>
        ///// <returns></returns>
        //public static async Task<T[]> LoadByUniqueConstraintAsync<T>(this IAsyncDocumentSession self, Expression<Func<T, object>> keySelector, string[] values)
        //    where T : AbstractRootEntity
        //{
        //    var collectionName = self.Advanced.DocumentStore.Conventions.GetCollectionName(typeof(T));
        //    var name = GetPropropertyNameForKeySelector(keySelector);
        //    var ids = values
        //        .Select(value => $"uniqueconstraints/{collectionName}/{name}/{value}")
        //        .ToArray();

        //    var uniqueConstraints = await self
        //        .Include<UniqueConstraint>(x => x.DocumentId)
        //        .LoadAsync(ids);
        //    var documentIds = uniqueConstraints
        //        .Where(x => x.Value != null)
        //        .Select(x => x.Value.Id)
        //        .ToArray();

        //    var entity = await self.LoadAsync<T>(documentIds);
        //    return entity.Values.ToArray();
        //}

        #endregion

        #region Get Property Name For Key Selector

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static string GetPropropertyNameForKeySelector<TDocument>(Expression<Func<TDocument, object>> keySelector)
        {
            var body = GetMemberExpression(keySelector);
            var propertyName = body.Member.Name;
            return propertyName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="keySelector"></param>
        /// <returns></returns>
        private static MemberExpression GetMemberExpression<T>(Expression<Func<T, object>> keySelector)
        {
            MemberExpression body;
            if (keySelector.Body is MemberExpression)
            {
                body = ((MemberExpression)keySelector.Body);
            }
            else
            {
                var op = ((UnaryExpression)keySelector.Body).Operand;
                body = ((MemberExpression)op);
            }

            var isDef = body.Member.IsDefined(typeof(AbstractUniqueConstraintAttribute), true);

            if (isDef == false)
            {
                var msg = string.Format(
                    "You are calling LoadByUniqueConstraint on {0}.{1}, but you haven't marked this property with [UniqueConstraint]",
                    body.Member.DeclaringType.Name, body.Member.Name);
                throw new InvalidOperationException(msg);
            }
            return body;
        }

        #endregion

    }

}

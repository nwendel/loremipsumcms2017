﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Linq;
using System.Reflection;
using Raven.Client.Documents.Session;
using LoremIpsumCMS.Infrastructure.Linq;
using Raven.Client.Documents.Commands.Batches;
using Sparrow.Json.Parsing;
using Raven.Client.Documents.Commands;

namespace LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    public class UniqueConstraintsListener
    {

        #region On Before Store

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnBeforeStore(object sender, BeforeStoreEventArgs e)
        {
            var entity = e.Entity;
            var entityType = e.Entity.GetType();

            if (typeof(UniqueConstraint).IsAssignableFrom(entityType))
            {
                return;
            }

            var id = e.DocumentId;

            var properties = entityType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(x => x.GetCustomAttributes<AbstractUniqueConstraintAttribute>(true).One())
                .ToList();
            if (properties.None())
            {
                return;
            }


            //var blittableDoc = e.Session.EntityToBlittable.ConvertEntityToBlittable(null, null);
            //var jsonValue = new DynamicJsonValue(blittableDoc);

            //e.Session.DocumentStore.Conventions.GetCollectionName(null);

            //e.Session.Defer(new PutCommandData("asdf",null,DynamicJsonValue))
            //e.Session.Defer(new PutDocumentCommand(null,null,null));

            // TODO: A different session here is bad, but use it as workaround for now
            using (var localSession = e.Session.DocumentStore.OpenSession(e.Session.DatabaseName))
            {
                var oldEntity = localSession.Load<object>(id);
                if (oldEntity != null)
                {
                    localSession.Advanced.Evict(oldEntity);
                }
                foreach (var property in properties)
                {
                    var value = (string) property.GetValue(entity);
                    var attribute = property.GetCustomAttribute<AbstractUniqueConstraintAttribute>(true);
                    var uniqueConstraint = attribute.CreateUniqueConstraintDocument(entity, id, property.Name, value);

                    if (oldEntity == null && value != null)
                    {
                        localSession.Store(uniqueConstraint);
                    }
                    else
                    {
                        var oldValue = oldEntity != null
                            ? (string) property.GetValue(oldEntity)
                            : null;
                        if (oldValue != value)
                        {
                            if (oldValue != null)
                            {
                                var oldUniqueConstraintId = attribute.CreateUniqueConstraintDocument(oldEntity, id, property.Name, oldValue).Id;
                                var oldUniqueConstraint = localSession.Load<UniqueConstraint>(oldUniqueConstraintId);
                                localSession.Delete(oldUniqueConstraint);
                            }
                            if (value != null)
                            {
                                localSession.Store(uniqueConstraint);
                            }
                        }
                    }
                }
                localSession.SaveChanges();
            }
        }

        #endregion

        #region On Before Delete

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnBeforeDelete(object sender, BeforeDeleteEventArgs e)
        {
            var entity = e.Entity;
            var entityType = e.Entity.GetType();

            if (typeof(UniqueConstraint).IsAssignableFrom(entityType))
            {
                return;
            }

            var id = e.DocumentId;

            var properties = entityType
                .GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(x => x.GetCustomAttributes<AbstractUniqueConstraintAttribute>(true).One())
                .ToList();
            if (properties.None())
            {
                return;
            }

            // TODO: A different session here is bad, but use it as workaround for now
            using (var localSession = e.Session.DocumentStore.OpenSession(e.Session.DatabaseName))
            {
                foreach (var property in properties)
                {
                    var value = (string) property.GetValue(entity);
                    var attribute = property.GetCustomAttribute<AbstractUniqueConstraintAttribute>(true);
                    var uniqueConstraintId = attribute.CreateUniqueConstraintDocument(entity, id, property.Name, value).Id;
                    if (value != null)
                    {
                        var uniqueConstraint = localSession.Load<UniqueConstraint>(uniqueConstraintId);
                        localSession.Delete(uniqueConstraint);
                    }
                }
                localSession.SaveChanges();
            }
        }

        #endregion

    }

}


﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Reflection
{

    /// <summary>
    /// 
    /// </summary>
    public static class AssemblyExtensions
    {

        #region Get Root Namespace

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string GetRootNamespace(this Assembly self)
        {
            var fullName = self.FullName;
            var index = fullName.IndexOfAny(new[] { '.', ',' });
            var rootNamespace = fullName.Substring(0, index);
            return rootNamespace;
        }

        #endregion

        #region Is In Root Namespace

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="rootNamespace"></param>
        /// <returns></returns>
        public static bool IsInRootNamespace(this AssemblyName self, string rootNamespace)
        {
            return self.FullName.StartsWith(rootNamespace + ".") || self.FullName == rootNamespace;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="rootNamespace"></param>
        /// <returns></returns>
        public static bool IsInRootNamespace(this AssemblyName self, IEnumerable<string> rootNamespaces)
        {
            return rootNamespaces.Any(x => self.IsInRootNamespace(x));
        }

        #endregion

    }

}

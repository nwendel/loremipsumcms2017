﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LoremIpsumCMS.Infrastructure
{

    /// <summary>
    /// 
    /// </summary>
    public static class StringExtensions
    {

        #region Slugify

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string Slugify(this string self)
        {
            if (self == null)
            {
                return null;
            }

            self = RemoveDiacritics(self);
            self = self.ToLowerInvariant();
            self = ReplaceNonWordWithDashes(self);
            self = self.Trim(' ', '-');

            return self;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string RemoveDiacritics(string value)
        {
            var normalizedValue = value.Normalize(NormalizationForm.FormD);
            var builder = new StringBuilder();

            foreach (var character in normalizedValue)
            {
                var unicodeCharacter = CharUnicodeInfo.GetUnicodeCategory(character);
                if (unicodeCharacter != UnicodeCategory.NonSpacingMark)
                {
                    builder.Append(character);
                }
            }

            value = builder.ToString();
            value = value.Normalize(NormalizationForm.FormC);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static string ReplaceNonWordWithDashes(string value)
        {
            // Remove Apostrophe Tags
            value = Regex.Replace(value, "[’'“”\"&]{1,}", "", RegexOptions.None);

            // Replaces all non-alphanumeric character by a space
            var builder = new StringBuilder();
            foreach (var character in value)
            {
                builder.Append(char.IsLetterOrDigit(character) ? character : ' ');
            }
            value = builder.ToString();

            // Replace multiple spaces into a single dash
            value = Regex.Replace(value, "[ ]{1,}", "-", RegexOptions.None);

            return value;
        }

        #endregion

        #region Substring Until

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="until"></param>
        /// <returns></returns>
        public static string SubstringUntil(this string self, char until)
        {
            var index = self.IndexOf(until);
            if (index != -1)
            {
                return self.Substring(0, index);
            }
            return self;
        }

        #endregion

        #region Trim Ends With

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="endsWith"></param>
        /// <returns></returns>
        public static string TrimEndsWith(this string self, params string[] endsWith)
        {
            if (endsWith == null)
            {
                throw new ArgumentNullException(nameof(endsWith));
            }
            if (endsWith.Any(x => string.IsNullOrEmpty(x)))
            {
                throw new ArgumentNullException(nameof(endsWith));
            }

            if (self == null)
            {
                return null;
            }

            foreach (var candidate in endsWith.OrderByDescending(x => x.Length))
            {
                if (!self.EndsWith(candidate))
                {
                    continue;
                }
                var result = self.Substring(0, self.Length - candidate.Length);
                return result;
            }
            return self;
        }

        #endregion

        #region Wordify

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string Wordify(this string self)
        {
            if (self == null || self.Length < 2)
            {
                return self;
            }

            var chars = self.ToCharArray();
            var wordLengths = new List<int>();

            var index = 0;
            while (index < chars.Length && char.IsUpper(chars[index]))
            {
                index += 1;
            }
            if (index > 1 && index < chars.Length)
            {
                wordLengths.Add(index - 1);
            }

            for (; index < chars.Length; index++)
            {
                if (!char.IsUpper(chars[index]))
                {
                    continue;
                }
                wordLengths.Add(index);
                index += 1;
                if (index >= chars.Length || !char.IsUpper(chars[index]))
                {
                    continue;
                }
                while (++index < chars.Length)
                {
                    if (char.IsUpper(chars[index]))
                    {
                        continue;
                    }
                    wordLengths.Add(index - 1);
                    break;
                }
            }

            var result = new char[chars.Length + wordLengths.Count];
            var lastIndex = 0;
            for (var wordIndex = 0; wordIndex < wordLengths.Count; wordIndex++)
            {
                var currentIndex = wordLengths[wordIndex];
                Array.Copy(chars, lastIndex, result, lastIndex + wordIndex, currentIndex - lastIndex);
                result[currentIndex + wordIndex] = ' ';
                lastIndex = currentIndex;
            }

            var lastWordIndex = lastIndex + wordLengths.Count;
            Array.Copy(chars, lastIndex, result, lastWordIndex, result.Length - lastWordIndex);

            return new string(result);
        }

        #endregion

    }

}

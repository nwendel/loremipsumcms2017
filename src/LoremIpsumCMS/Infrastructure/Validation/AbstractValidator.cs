﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LoremIpsumCMS.Infrastructure.Validation.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractValidator<T> : IValidator<T>
    {

        #region Fields

        private readonly IList<IInstanceRule<T>> _instanceRules = new List<IInstanceRule<T>>();

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        IEnumerable<ValidationMessage> IValidator.Validate(IValidationContext<dynamic> dynamicContext)
        {
            var context = new ValidationContext<T>(dynamicContext.PropertyChain, dynamicContext.Instance, dynamicContext.ServiceProvider);
            return Validate(context);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(IValidationContext<T> context)
        {
            var messages = _instanceRules.SelectMany(x => x.Validate(context));
            return messages;
        }

        #endregion

        #region Rule For

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        protected IRuleBuilder<TResult> RuleFor<TResult>(Expression<Func<T, TResult>> propertyExpression)
        {
            var rule = new PropertyRule<T, TResult>(propertyExpression);
            _instanceRules.Add(rule);

            var builder = new PropertyRuleBuilder<T, TResult>(rule);
            return builder;
        }

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        protected IRuleBuilderOptions ValidateUsing<TValidator>()
            where TValidator : IValidator<T>
        {
            var rule = new InstanceRule<T>();
            _instanceRules.Add(rule);

            var builder = new InstanceRuleBuilder<T>(rule);
            builder.ValidateUsing<TValidator>();
            return builder;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validator"></param>
        protected IRuleBuilderOptions ValidateUsing(Func<IValidationContext<T>, IEnumerable<ValidationMessage>> validator)
        {
            var rule = new InstanceRule<T>();
            _instanceRules.Add(rule);

            var builder = new InstanceRuleBuilder<T>(rule);
            builder.ValidateUsing(validator);
            return builder;
        }

        #endregion

    }

    }

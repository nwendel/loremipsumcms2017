﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractRuleBuilder<T> : IRuleBuilder<T>, IValueValidatorContainer<T>
    {

        #region Fields

        private readonly IRuleOptions _ruleOptions;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        protected AbstractRuleBuilder(IRuleOptions ruleOptions)
        {
            _ruleOptions = ruleOptions;
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IRuleBuilder<T> Validate()
        {
            AddValueValidator(new ValidateUsingValueValidator<T, IValidator<T>>());
            return this;
        }

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        public IRuleBuilder<T> ValidateUsing<TValidator>()
            where TValidator : IValidator<T>
        {
            AddValueValidator(new ValidateUsingValueValidator<T, TValidator>());
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T2"></typeparam>
        /// <param name="validator"></param>
        /// <returns></returns>
        public IRuleBuilder<T> ValidateUsing(Func<IValidationContext<T>, IEnumerable<ValidationMessage>> validator)
        {
            AddValueValidator(new ValidateUsingMethodValueValidator<T>(validator));
            return this;
        }

        #endregion

        #region Polymorphic Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IRuleBuilder<T> PolymorphicValidate()
        {
            AddValueValidator(new PolymorphicValidateValueValidator<T>());
            return this;
        }

        #endregion

        #region With Message

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public IRuleBuilderOptions WithMessage(string message)
        {
            _ruleOptions.WithMessage = message;
            return this;
        }

        #endregion

        #region Add Value Validator

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validator"></param>
        public abstract void AddValueValidator(IValueValidator<T> validator);

        #endregion

    }

}

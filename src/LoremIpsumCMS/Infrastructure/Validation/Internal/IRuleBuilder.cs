﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public interface IRuleBuilder<out T> : IRuleBuilderOptions, IFluentInterface
    {

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IRuleBuilder<T> Validate();

        #endregion

        #region Validate Using

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        IRuleBuilder<T> ValidateUsing<TValidator>()
            where TValidator : IValidator<T>;

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TValidator"></typeparam>
        /// <returns></returns>
        IRuleBuilder<T> ValidateUsing(Func<IValidationContext<T>, IEnumerable<ValidationMessage>> validator);

        #endregion

        #region Polymorphic Validate

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IRuleBuilder<T> PolymorphicValidate();

        #endregion

    }

}

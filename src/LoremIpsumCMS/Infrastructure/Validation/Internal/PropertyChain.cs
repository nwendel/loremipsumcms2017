﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Linq;

namespace LoremIpsumCMS.Infrastructure.Validation.Internal
{

    /// <summary>
    /// 
    /// </summary>
    public class PropertyChain
    {

        #region Fields

        private readonly List<string> _propertyNames = new List<string>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public PropertyChain()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="propertyName"></param>
        public PropertyChain(PropertyChain parent, string propertyName)
        {
            _propertyNames.AddRange(parent._propertyNames);
            _propertyNames.Add(propertyName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="index"></param>
        public PropertyChain(PropertyChain parent, int index)
        {
            _propertyNames.AddRange(parent._propertyNames);
            var lastIndex = _propertyNames.Count - 1;
            _propertyNames[lastIndex] = _propertyNames[lastIndex] + $"[{index}]";
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name => _propertyNames.None() ? string.Empty : _propertyNames.Last().SubstringUntil('[');

        /// <summary>
        /// 
        /// </summary>
        public string FullName => string.Join(".", _propertyNames);

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderComparableExtensions
    {

        #region Less Than

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="lessThan"></param>
        /// <returns></returns>
        public static IRuleBuilder<TResult> LessThan<TResult>(this IRuleBuilder<TResult> self, TResult lessThan)
            where TResult : IComparable<TResult>
        {
            self.AddValueValidator(new LessThanValueValidator<TResult>(lessThan));
            return self;
        }

        #endregion

        #region Less Than or Equal To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="lessThanOrEqualTo"></param>
        /// <returns></returns>
        public static IRuleBuilder<TResult> LessThanOrEqualTo<TResult>(this IRuleBuilder<TResult> self, TResult lessThanOrEqualTo)
            where TResult : IComparable<TResult>
        {
            self.AddValueValidator(new LessThanOrEqualToValueValidator<TResult>(lessThanOrEqualTo));
            return self;
        }

        #endregion

        #region Greater Than

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="greaterThan"></param>
        /// <returns></returns>
        public static IRuleBuilder<TResult> GreaterThan<TResult>(this IRuleBuilder<TResult> self, TResult greaterThan)
            where TResult : IComparable<TResult>
        {
            self.AddValueValidator(new GreaterThanValueValidator<TResult>(greaterThan));
            return self;
        }

        #endregion

        #region Greater Than or Equal To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        /// <param name="greaterThanOrEqualTo"></param>
        /// <returns></returns>
        public static IRuleBuilder<TResult> GreaterThanOrEqualTo<TResult>(this IRuleBuilder<TResult> self, TResult greaterThanOrEqualTo)
            where TResult : IComparable<TResult>
        {
            self.AddValueValidator(new GreaterThanOrEqualToValueValidator<TResult>(greaterThanOrEqualTo));
            return self;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderExtensions
    {

        #region Default

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        public static IRuleBuilder<TResult> Default<TResult>(this IRuleBuilder<TResult> self)
            where TResult : struct
        {
            self.AddValueValidator(new DefaultValueValidator<TResult>());
            return self;
        }

        #endregion

        #region Not Default

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        public static IRuleBuilder<TResult> NotDefault<TResult>(this IRuleBuilder<TResult> self)
            where TResult : struct
        {
            self.AddValueValidator(new NotDefaultValueValidator<TResult>());
            return self;
        }

        #endregion

        #region Null

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        public static IRuleBuilder<TResult> Null<TResult>(this IRuleBuilder<TResult> self)
            where TResult : class
        {
            self.AddValueValidator(new NullValueValidator<TResult>());
            return self;
        }

        #endregion

        #region Not Null

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        public static IRuleBuilder<TResult> NotNull<TResult>(this IRuleBuilder<TResult> self)
            where TResult : class
        {
            self.AddValueValidator(new NotNullValueValidator<TResult>());
            return self;
        }

        #endregion

    }

}

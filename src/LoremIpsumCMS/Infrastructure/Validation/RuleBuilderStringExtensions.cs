﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;

namespace LoremIpsumCMS.Infrastructure.Validation
{

    /// <summary>
    /// 
    /// </summary>
    public static class RuleBuilderStringExtensions
    {

        #region Not Null or White Space

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="self"></param>
        public static IRuleBuilder<string> NotNullOrWhiteSpace(this IRuleBuilder<string> self)
        {
            self.AddValueValidator(new NotNullOrWhiteSpaceValueValidator());
            return self;
        }

        #endregion

        #region Length

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minimum"></param>
        /// <param name="maximum"></param>
        /// <returns></returns>
        public static IRuleBuilder<string> Length(this IRuleBuilder<string> self, int minimum, int maximum)
        {
            self.AddValueValidator(new LengthValueValidator(minimum, maximum));
            return self;
        }

        #endregion

    }

}

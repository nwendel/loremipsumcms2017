﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractSimpleValueValidator<T> : IValueValidator<T>
    {

        #region Fields

        private readonly string _text;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        protected AbstractSimpleValueValidator(string text)
        {
            _text = text;
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(PropertyChain propertyChain, T value, IServiceProvider serviceProvider)
        {
            if(!IsValid(value))
            {
                var text = _text.Replace("{PropertyName}", propertyChain.Name.Wordify());
                yield return new ValidationMessage(propertyChain.FullName, text);
            }
        }

        #endregion

        #region Is Valid

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected abstract bool IsValid(T value);

        #endregion

    }

}

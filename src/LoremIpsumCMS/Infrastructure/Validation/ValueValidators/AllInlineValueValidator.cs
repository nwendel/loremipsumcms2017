﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Linq;
using LoremIpsumCMS.Infrastructure.Validation.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class AllInlineValueValidator<T, TItem> : IValueValidator<T>
        where T : IEnumerable<TItem>
    {

        #region Fields

        private readonly InlineValidator<TItem> _inlineValidator = new InlineValidator<TItem>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="action"></param>
        public AllInlineValueValidator(Action<InlineValidator<TItem>> action)
        {
            action(_inlineValidator);
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(PropertyChain propertyChain, T value, IServiceProvider serviceProvider)
        {
            var safeValue = (IEnumerable<TItem>)value ?? new List<TItem>();
            var messages = safeValue.SelectMany((item, ix) =>
            {
                var itemPropertyChain = new PropertyChain(propertyChain, ix);
                var context = new ValidationContext<TItem>(itemPropertyChain, item, serviceProvider);
                var itemMessages = _inlineValidator.Validate(context);
                return itemMessages;
            });
            return messages;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LessThanOrEqualToValueValidator<T> : AbstractSimpleValueValidator<T>
        where T : IComparable<T>
    {

        #region Fields

        private readonly T _lessThanOrEqualTo;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lessThanOrEqualTo"></param>
        public LessThanOrEqualToValueValidator(T lessThanOrEqualTo) : 
            base($"{{PropertyName}} must be less than or equal to {lessThanOrEqualTo}")
        {
            _lessThanOrEqualTo = lessThanOrEqualTo;
        }

        #endregion

        #region Is Valid

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override bool IsValid(T value)
        {
            return value == null || value.CompareTo(_lessThanOrEqualTo) <= 0;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class NotNullOrWhiteSpaceValueValidator : AbstractSimpleValueValidator<string>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        public NotNullOrWhiteSpaceValueValidator() : base("{PropertyName} must be set")
        {
        }

        #endregion

        #region Is Valid

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected override bool IsValid(string value)
        {
            var isValid = !string.IsNullOrWhiteSpace(value);
            return isValid;
        }

        #endregion

    }

}

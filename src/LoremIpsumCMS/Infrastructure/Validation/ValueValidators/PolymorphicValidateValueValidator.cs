﻿using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Reflection;

namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class PolymorphicValidateValueValidator<T> : IValueValidator<T>
    {

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(PropertyChain propertyChain, T value, IServiceProvider serviceProvider)
        {
            if (value == null)
            {
                return new ValidationMessage[0];
            }

            var validatorType = typeof(IValidator<>).MakeGenericType(value.GetType());
            var validators = serviceProvider.GetServices(validatorType).Cast<IValidator>();          
            var context = new ValidationContext<dynamic>(propertyChain, value, serviceProvider);
            var messages = validators.SelectMany(v => v.Validate(context));
            return messages;
        }

        #endregion

    }

}

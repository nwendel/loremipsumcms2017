﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation.Internal;

namespace LoremIpsumCMS.Infrastructure.Validation.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class ValidateUsingMethodValueValidator<T> : IValueValidator<T>
    {

        #region Fields

        private readonly Func<ValidationContext<T>, IEnumerable<ValidationMessage>> _validator;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="validator"></param>
        public ValidateUsingMethodValueValidator(Func<ValidationContext<T>, IEnumerable<ValidationMessage>> validator)
        {
            _validator = validator;
        }

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(PropertyChain propertyChain, T value, IServiceProvider serviceProvider)
        {
            if (value == null)
            {
                return new ValidationMessage[0];
            }

            var context = new ValidationContext<T>(propertyChain, value, serviceProvider);
            var messages = _validator(context);
            return messages;
        }

        #endregion

    }

}

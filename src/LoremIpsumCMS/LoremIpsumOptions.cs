﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Reflection;
using LoremIpsumCMS.Infrastructure.Reflection;

namespace LoremIpsumCMS
{

    /// <summary>
    /// 
    /// </summary>
    public class LoremIpsumOptions
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public LoremIpsumOptions()
        {
            ApplicationRootNamespaces = new[] { Assembly.GetEntryAssembly().GetRootNamespace() };
            RavenDatabase = "loremipsum";
            RavenFileSystem = "loremipsum";
            RavenUrl = "http://localhost:8080";
            IsHostnameOverrideEnabled = false;
            HostnameOverrideQueryParameterName = "hostnameoverride";
            AssetRootPath = "assets";
            IgnorePageRouteForExtensions = new[] { ".css", ".jpg", ".js", ".png", ".woff", ".woff2"};
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> ApplicationRootNamespaces { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RavenDatabase { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RavenFileSystem { get; set; }

        // TODO: Support several like raven?

        /// <summary>
        /// 
        /// </summary>
        public string RavenUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsHostnameOverrideEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string HostnameOverrideQueryParameterName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AssetRootPath { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> IgnorePageRouteForExtensions { get; set; }

        #endregion

    }

}

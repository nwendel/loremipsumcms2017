﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using LoremIpsumCMS.Events.Internal;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Model.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class AbstractContent<TData, TExtension> : AbstractAggregate, IAbstractContent<TData, TExtension>, ISiteIdAware
        where TData : AbstractContentData
        where TExtension : AbstractExtensionProperties
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected AbstractContent()
        {
            ExtensionPropertiesInner = new List<TExtension>();
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public TData Data { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        protected IList<TExtension> ExtensionPropertiesInner { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public IEnumerable<TExtension> ExtensionProperties => new ReadOnlyCollection<TExtension>(ExtensionPropertiesInner);

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdatedAt { get; protected set; }

        #endregion

        #region Apply Content Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        protected void ApplyContentCreated(IContentCreatedEvent @event)
        {
            SiteId = @event.SiteId;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        protected void ApplyUpdated(IUpdatedEvent @event)
        {
            LastUpdatedAt = @event.UpdatedAt;
        }

        #endregion

        #region Apply Content

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        protected void ApplyContent(IContentEvent<TData, TExtension> @event)
        {
            Data = @event.Data;
            ExtensionPropertiesInner = @event.ExtensionProperties?.ToList();
        }

        #endregion

    }

}

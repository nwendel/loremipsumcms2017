﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Model.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Asset : AbstractAggregate, ISiteIdAware,
        IApplyEvent<AssetCreatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string SiteIdAndPath => $"{SiteId}/{Path}";

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; private set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(AssetCreatedEvent @event)
        {
            SiteId = @event.SiteId;
            Path = @event.Path;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

    }

}

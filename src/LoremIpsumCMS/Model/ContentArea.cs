﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using Newtonsoft.Json;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentArea : AbstractEntity
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        protected IList<AbstractContentAreaItem> InnerItems { get; set; } = new List<AbstractContentAreaItem>();

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public IEnumerable<AbstractContentAreaItem> Items => InnerItems;

        #endregion

        #region List Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void Add(AbstractContentAreaItem item)
        {
            InnerItems.Add(item);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="item"></param>
        public void Remove(AbstractContentAreaItem item)
        {
            InnerItems.Remove(item);
        }

        /// <summary>
        /// 
        /// </summary>
        public void Clear()
        {
            InnerItems.Clear();
        }

        #endregion

    }

}

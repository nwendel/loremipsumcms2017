﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentTypeInfo<T>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        public ContentTypeInfo(string name, Type type)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            Name = name;
            Type = type;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        public Type Type { get; private set; }

        #endregion

        #region Create

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public T Create()
        {
            return (T)Activator.CreateInstance(Type);
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public static class ExtensionPropertiesAwareExtensions
    {

        #region Get Extension Properties

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExtension"></typeparam>
        /// <returns></returns>
        public static TExtension GetExtensionProperty<TExtension>(this IExtensionPropertiesAware<AbstractExtensionProperties> self)
            where TExtension : AbstractExtensionProperties
        {
            var properties = self.GetExtensionPropertyInner(typeof(TExtension));
            return (TExtension)properties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static AbstractExtensionProperties GetExtensionProperty(this IExtensionPropertiesAware<AbstractExtensionProperties> self, Type type)
        {
            var properties = self.GetExtensionPropertyInner(type);
            return properties;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static AbstractExtensionProperties GetExtensionPropertyInner(this IExtensionPropertiesAware<AbstractExtensionProperties> self, Type type)
        {
            var extensionPropertiesAwareType = self.GetType().GetTypeInfo().GetInterfaces()
                .Single(x => typeof(IExtensionPropertiesAware<AbstractExtensionProperties>).IsAssignableFrom(x));
            var extensionPropertiesType = extensionPropertiesAwareType.GenericTypeArguments[0];
            if(!extensionPropertiesType.IsAssignableFrom(type))
            {
                throw new ArgumentException($"Extension property type {extensionPropertiesType.FullName} must inherit from {extensionPropertiesType.FullName}");
            }

            var extensionProperty = self.ExtensionProperties
                .SingleOrDefault(x => x.GetType() == type);
            if (extensionProperty == null)
            {
                extensionProperty = (AbstractExtensionProperties)Activator.CreateInstance(type);
            }
            return extensionProperty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExtension"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public static string GetExtensionProperty<TExtension>(this IExtensionPropertiesAware<AbstractExtensionProperties> self, Expression<Func<TExtension, string>> propertyExpression)
            where TExtension : AbstractExtensionProperties
        {
            var value = self.GetExtensionPropertyInner(propertyExpression);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExtension"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public static bool GetExtensionProperty<TExtension>(this IExtensionPropertiesAware<AbstractExtensionProperties> self, Expression<Func<TExtension, bool>> propertyExpression)
            where TExtension : AbstractExtensionProperties
        {
            var value = self.GetExtensionPropertyInner(propertyExpression);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExtension"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        public static int GetExtensionProperty<TExtension>(this IExtensionPropertiesAware<AbstractExtensionProperties> self, Expression<Func<TExtension, int>> propertyExpression)
            where TExtension : AbstractExtensionProperties
        {
            var value = self.GetExtensionPropertyInner(propertyExpression);
            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TExtension"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="propertyExpression"></param>
        /// <returns></returns>
        private static TValue GetExtensionPropertyInner<TExtension, TValue>(this IExtensionPropertiesAware<AbstractExtensionProperties> self, Expression<Func<TExtension, TValue>> propertyExpression)
            where TExtension : AbstractExtensionProperties
        {
            var extensionProperty = self.GetExtensionProperty<TExtension>();
            var property = propertyExpression.Compile();
            var value = property(extensionProperty);
            return value;
        }

        #endregion

    }

}

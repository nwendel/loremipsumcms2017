﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class IdentityUser : AbstractAggregate,
        IApplyEvent<IdentityUserCreatedEvent>,
        IApplyEvent<IdentityUserExternalAccountCreatedEvent>,
        IApplyEvent<IdentityUserLoginEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string FullName { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string DisplayNameSlug => DisplayName.Slugify();

        /// <summary>
        /// 
        /// </summary>
        public IdentityUserAccount Account { get; set; }

        /// <summary>
        /// 
        /// </summary>
        protected IList<IdentityUserExternalAccount> ExternalAccountsInner { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [JsonIgnore]
        public IEnumerable<IdentityUserExternalAccount> ExternalAccounts => new ReadOnlyCollection<IdentityUserExternalAccount>(ExternalAccountsInner);

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> PermissionNames { get; protected set; } = new List<string>();

        /// <summary>
        /// 
        /// </summary>
        public DateTime LastLoginAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(IdentityUserCreatedEvent @event)
        {
            DisplayName = @event.DisplayName;
            FullName = @event.FullName;
            Email = @event.Email;
            PermissionNames = @event.PermissionNames;
            Account = new IdentityUserAccount(@event.PasswordHash);
            ExternalAccountsInner = new List<IdentityUserExternalAccount>();
            LastLoginAt = @event.CreatedAt;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

        #region Apply External Account Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(IdentityUserExternalAccountCreatedEvent @event)
        {
            DisplayName = @event.DisplayName;
            FullName = @event.FullName;
            Email = @event.Email;
            PermissionNames = @event.PermissionNames;
            ExternalAccountsInner = new List<IdentityUserExternalAccount>(new[]
            {
                new IdentityUserExternalAccount(@event.LoginProvider, @event.LoginProviderKey, @event.Email)
            });
            LastLoginAt = @event.CreatedAt;
            CreatedAt = @event.CreatedAt;
        }

        #endregion

        #region Apply Login

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(IdentityUserLoginEvent @event)
        {
            LastLoginAt = @event.At;
        }

        #endregion

    }

}

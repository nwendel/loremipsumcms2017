﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class IdentityUserExternalAccount : AbstractEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected IdentityUserExternalAccount()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="loginProviderKey"></param>
        /// <param name="email"></param>
        public IdentityUserExternalAccount(string loginProvider, string loginProviderKey, string email)
        {
            LoginProvider = loginProvider;
            LoginProviderKey = loginProviderKey;
            Email = email;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string LoginProvider { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string LoginProviderKey { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; protected set; }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using LoremIpsumCMS.Events;
using LoremIpsumCMS.Events.Internal;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class Page : AbstractContent<AbstractPageData, AbstractPageExtensionProperties>,
        IApplyEvent<PageCreatedEvent>,
        IApplyEvent<PageUpdatedEvent>
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string SiteIdAndPath => $"{SiteId}/{Path}";

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string MetaKeywords { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string MetaDescription { get; protected set; }

        #endregion

        #region Apply Created

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PageCreatedEvent @event)
        {
            ApplyContentCreated(@event);
            ApplyContent(@event);
            ApplyPage(@event);
        }

        #endregion

        #region Apply Updated

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        public void Apply(PageUpdatedEvent @event)
        {
            ApplyUpdated(@event);
            ApplyContent(@event);
            ApplyPage(@event);
        }

        #endregion

        #region Apply Page

        /// <summary>
        /// 
        /// </summary>
        /// <param name="event"></param>
        private void ApplyPage(IPageEvent @event)
        {
            Path = @event.Path;
            Title = @event.Title;
            MetaKeywords = @event.MetaKeywords;
            MetaDescription = @event.MetaDescription;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Model.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PageOverview : AbstractReadModel, ISiteIdAware
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PageOverview()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageId"></param>
        /// <param name="siteId"></param>
        /// <param name="path"></param>
        /// <param name="title"></param>
        /// <param name="dataTypeFullName"></param>
        /// <param name="createdAt"></param>
        public PageOverview(string pageId, string siteId, string path, string title, string dataTypeFullName, DateTime createdAt)
        {
            PageId = pageId;
            SiteId = siteId;
            Path = path;
            Title = title;
            DataTypeFullName = dataTypeFullName;
            CreatedAt = createdAt;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string PageId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string SiteIdAndPath => $"{SiteId}/{Path}";

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string DataTypeFullName { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdatedAt { get; protected set; }

        #endregion

        #region Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="title"></param>
        /// <param name="updatedAt"></param>
        public void Update(string path, string title, DateTime updatedAt)
        {
            Path = path;
            Title = title;
            LastUpdatedAt = updatedAt;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion

using System;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PageReference : AbstractEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PageReference()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageId"></param>
        public PageReference(string pageId)
        {
            if (pageId == null)
            {
                throw new ArgumentNullException(nameof(pageId));
            }

            PageId = pageId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageOverview"></param>
        public PageReference(PageOverview pageOverview)
        {
            if (pageOverview == null)
            {
                throw new ArgumentNullException(nameof(pageOverview));
            }
            if (pageOverview.PageId == null)
            {
                throw new ArgumentNullException(nameof(pageOverview));
            }

            PageId = pageOverview.PageId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string PageId { get; set; }

        #endregion

    }

}

﻿namespace LoremIpsumCMS.Model
{


    /// <summary>
    /// 
    /// </summary>
    public class PartialContentAreaItem : AbstractContentAreaItem
    {

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string PartialId { get; set; }

        #endregion

    }

}

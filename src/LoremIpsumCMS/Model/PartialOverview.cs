﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Model.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialOverview : AbstractReadModel, ISiteIdAware
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PartialOverview()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partialId"></param>
        /// <param name="siteId"></param>
        /// <param name="name"></param>
        /// <param name="dataTypeFullName"></param>
        /// <param name="createdAt"></param>
        public PartialOverview(string partialId, string siteId, string name, string dataTypeFullName, DateTime createdAt)
        {
            PartialId = partialId;
            SiteId = siteId;
            Name = name;
            DataTypeFullName = dataTypeFullName;
            CreatedAt = createdAt;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string PartialId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        [SiteAwareUniqueConstraint]
        public string SiteIdAndNameSlug => $"{SiteId}/{NameSlug}";

        /// <summary>
        /// 
        /// </summary>
        public string DataTypeFullName { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUpdatedAt { get; protected set; }

        #endregion

        #region Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="updatedAt"></param>
        public void Update(string name, DateTime updatedAt)
        {
            Name = name;
            LastUpdatedAt = updatedAt;
        }

        #endregion

    }

}

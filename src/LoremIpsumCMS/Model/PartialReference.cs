﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Model;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class PartialReference : AbstractEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected PartialReference()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partialId"></param>
        public PartialReference(string partialId)
        {
            if (partialId == null)
            {
                throw new ArgumentNullException(nameof(partialId));
            }

            PartialId = partialId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="partialOverview"></param>
        public PartialReference(PartialOverview partialOverview)
        {
            if (partialOverview == null)
            {
                throw new ArgumentNullException(nameof(partialOverview));
            }
            if (partialOverview.PartialId == null)
            {
                throw new ArgumentNullException(nameof(partialOverview));
            }

            PartialId = partialOverview.PartialId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string PartialId { get; set; }

        #endregion

    }

}

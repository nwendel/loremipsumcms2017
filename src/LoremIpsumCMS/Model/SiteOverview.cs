﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Infrastructure.Model;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteOverview : AbstractRootEntity
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected SiteOverview()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="name"></param>
        /// <param name="hosts"></param>
        public SiteOverview(string siteId, string name, IEnumerable<SiteHost> hosts)
        {
            if(string.IsNullOrWhiteSpace(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }
            if(string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException(nameof(name));
            }
            if(hosts == null)
            {
                throw new ArgumentNullException(nameof(hosts));
            }

            SiteId = siteId;
            Name = name;
            Hosts = hosts;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string SiteId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        [UniqueConstraint]
        public string NameSlug => Name.Slugify();

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<SiteHost> Hosts { get; protected set; }

        #endregion

        #region Update

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="hosts"></param>
        public void Update(string name, IEnumerable<SiteHost> hosts)
        {
            Name = name;
            Hosts = hosts;
        }

        #endregion

    }

}

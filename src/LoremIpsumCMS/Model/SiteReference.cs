﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteReference
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteId"></param>
        public SiteReference(string siteId)
        {
            if(string.IsNullOrWhiteSpace(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }

            SiteId = siteId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="siteOverview"></param>
        public SiteReference(SiteOverview siteOverview)
        {
            if (siteOverview == null)
            {
                throw new ArgumentNullException(nameof(siteOverview));
            }

            SiteId = siteOverview.SiteId;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string SiteId { get; protected set; }

        #endregion

    }

}

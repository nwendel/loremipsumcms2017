﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;

namespace LoremIpsumCMS.Model.UniqueConstraints
{

    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SiteAwareUniqueConstraintAttribute : AbstractUniqueConstraintAttribute
    {

        #region Create Internal

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityName"></param>
        /// <param name="id"></param>
        /// <param name="entity"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        protected override UniqueConstraint CreateInternal(string entityName, string id, object entity, string documentId)
        {
            if (!(entity is ISiteIdAware siteIdAware))
            {
                throw new LoremIpsumException($"{entity.GetType().Name} must implement {nameof(ISiteIdAware)}");
            }
            if(siteIdAware.SiteId == null)
            {
                // TODO: Is this correct, or should it be allowed and not create a constraint document?
                throw new LoremIpsumException($"SiteId must not be null");
            }

            var uniqueConstraint = new SiteAwareUniqueConstraint(entityName, id, documentId, siteIdAware.SiteId);
            return uniqueConstraint;
        }

        #endregion

    }

}

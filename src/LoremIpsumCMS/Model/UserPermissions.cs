﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Linq;

namespace LoremIpsumCMS.Model
{

    /// <summary>
    /// 
    /// </summary>
    public class UserPermissions
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        protected UserPermissions()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="permissionNames"></param>
        public UserPermissions(string userId, IEnumerable<string> permissionNames)
        {
            if (userId == null)
            {
                throw new ArgumentNullException(nameof(userId));
            }
            permissionNames = permissionNames?.ToList();
            if (permissionNames == null || permissionNames.None() || permissionNames.Any(x => x == null))
            {
                throw new ArgumentNullException(nameof(permissionNames));
            }

            UserId = userId;
            PermissionNames = permissionNames;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; protected set; }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<string> PermissionNames { get; protected set; }

        #endregion

    }

}

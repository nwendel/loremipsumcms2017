﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Threading.Tasks;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class IdentityUserLoadByExtensions
    {

        #region Display Name Slug

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="displayNameSlug"></param>
        /// <returns></returns>
        public static IdentityUser DisplayNameSlug(this DocumentSessionLoadBy<IdentityUser> self, string displayNameSlug)
        {
            if (string.IsNullOrWhiteSpace(displayNameSlug))
            {
                throw new ArgumentNullException(nameof(displayNameSlug));
            }

            var identityUser = self.Session.LoadByUniqueConstraint<IdentityUser>(x => x.DisplayNameSlug, displayNameSlug);
            return identityUser;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="self"></param>
        ///// <param name="displayNameSlug"></param>
        ///// <returns></returns>
        //public static Task<IdentityUser> DisplayNameSlug(this AsyncDocumentSessionLoadBy<IdentityUser> self, string displayNameSlug)
        //{
        //    if (string.IsNullOrWhiteSpace(displayNameSlug))
        //    {
        //        throw new ArgumentNullException(nameof(displayNameSlug));
        //    }

        //    var identityUser = self.AsyncSession.LoadByUniqueConstraintAsync<IdentityUser>(x => x.DisplayNameSlug, displayNameSlug);
        //    return identityUser;
        //}

        #endregion

        #region Login Provider And Key

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="loginProvider"></param>
        /// <param name="loginProviderKey"></param>
        /// <returns></returns>
        public static IdentityUser LoginProviderAndKey(this DocumentSessionLoadBy<IdentityUser> self, string loginProvider, string loginProviderKey)
        {
            if (string.IsNullOrWhiteSpace(loginProvider))
            {
                throw new ArgumentNullException(nameof(loginProvider));
            }
            if (string.IsNullOrWhiteSpace(loginProviderKey))
            {
                throw new ArgumentNullException(nameof(loginProviderKey));
            }

            var externalAccount = self.Session.LoadByUniqueConstraint<ExternalAccount>(x => x.LoginProviderAndKey, $"{loginProvider}/{loginProviderKey}");
            if (externalAccount == null)
            {
                return null;
            }

            var identityUser = self.Session.Load<IdentityUser>(externalAccount.UserId);
            return identityUser;
        }

        #endregion

    }

}

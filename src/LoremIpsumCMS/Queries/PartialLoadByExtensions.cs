﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class PartialLoadByExtensions
    {

        #region Site Id And Name Slug

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="siteId"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public static Partial SiteIdAndNameSlug(this DocumentSessionLoadBy<Partial> self, string siteId, string nameSlug)
        {
            if (string.IsNullOrEmpty(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }
            if(string.IsNullOrEmpty(nameSlug))
            {
                throw new ArgumentNullException(nameof(nameSlug));
            }

            var partial = self.Session.LoadByUniqueConstraint<Partial>(x => x.SiteIdAndNameSlug, $"{siteId}/{nameSlug}");
            return partial;
        }

        #endregion

    }

}

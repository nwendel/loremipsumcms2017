﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Linq;
using LoremIpsumCMS.Infrastructure.Raven;
using LoremIpsumCMS.Infrastructure.Raven.UniqueConstraints;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Queries
{

    /// <summary>
    /// 
    /// </summary>
    public static class PartialOverviewLoadByExtensions
    {

        #region Partial Id

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="partialId"></param>
        /// <returns></returns>
        public static PartialOverview PartialId(this DocumentSessionLoadBy<PartialOverview> self, string partialId)
        {
            if (string.IsNullOrEmpty(partialId))
            {
                throw new ArgumentNullException(nameof(partialId));
            }

            var partialOverview = self.Session.LoadByUniqueConstraint<PartialOverview>(x => x.PartialId, partialId);
            return partialOverview;
        }

        #endregion

        #region Partial Ids

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="partialIds"></param>
        /// <returns></returns>
        public static PartialOverview[] PartialIds(this DocumentSessionLoadBy<PartialOverview> self, string[] partialIds)
        {
            if (partialIds == null)
            {
                throw new ArgumentNullException(nameof(partialIds));
            }

            var partialOverviews = self.Session.LoadByUniqueConstraint<PartialOverview>(x => x.PartialId, partialIds);
            return partialOverviews;
        }

        #endregion

        #region Site Id And Name Slug

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="siteId"></param>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public static PartialOverview SiteIdAndNameSlug(this DocumentSessionLoadBy<PartialOverview> self, string siteId, string nameSlug)
        {
            if (string.IsNullOrEmpty(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }
            if (string.IsNullOrEmpty(nameSlug))
            {
                throw new ArgumentNullException(nameof(nameSlug));
            }

            var partialOverview = self.Session.LoadByUniqueConstraint<PartialOverview>(x => x.SiteIdAndNameSlug, $"{siteId}/{nameSlug}");
            return partialOverview;
        }

        #endregion

        #region Site Id And Name Slugs

        /// <summary>
        /// 
        /// </summary>
        /// <param name="self"></param>
        /// <param name="siteId"></param>
        /// <param name="nameSlugs"></param>
        /// <returns></returns>
        public static PartialOverview[] SiteIdAndNameSlugs(this DocumentSessionLoadBy<PartialOverview> self, string siteId, string[] nameSlugs)
        {
            if (string.IsNullOrEmpty(siteId))
            {
                throw new ArgumentNullException(nameof(siteId));
            }
            if (nameSlugs == null)
            {
                throw new ArgumentNullException(nameof(nameSlugs));
            }

            var values = nameSlugs
                .Select(x => $"{siteId}/{x}")
                .ToArray();

            var partialOverviews = self.Session.LoadByUniqueConstraint<PartialOverview>(x => x.SiteIdAndNameSlug, values);
            return partialOverviews;
        }

        #endregion

    }

}

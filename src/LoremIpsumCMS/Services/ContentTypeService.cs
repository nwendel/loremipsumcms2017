﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using LoremIpsumCMS.Infrastructure;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class ContentTypeService : IContentTypeService
    {

        // TODO: Too much repetetive code, rewrite needed
        // TODO: Assembly scanning and reflection each time, rewrite needed

        #region Dependencies

        private readonly IReflectionService _reflectionService;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reflectionService"></param>
        public ContentTypeService(IReflectionService reflectionService)
        {
            _reflectionService = reflectionService;
        }

        #endregion

        #region Find Site Extension Property Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentTypeInfo<AbstractSiteExtensionProperties>> FindSiteExtensionPropertyTypes()
        {
            var types = FindTypesBasedOn<AbstractSiteExtensionProperties>();
            var typeInfos = types.Select(x =>
            {
                var instance = (AbstractSiteExtensionProperties)Activator.CreateInstance(x);
                var name = instance.GetName();
                return new ContentTypeInfo<AbstractSiteExtensionProperties>(name, x);
            }).ToList();
            return typeInfos;
        }

        #endregion

        #region Find Page Data Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentTypeInfo<AbstractPageData>> FindPageDataTypes()
        {
            var types = FindTypesBasedOn<AbstractPageData>();
            var typeInfos = types.Select(x =>
            {
                var instance = (AbstractPageData)Activator.CreateInstance(x);
                var name = instance.GetName();
                return new ContentTypeInfo<AbstractPageData>(name, x);
            }).ToList();
            return typeInfos;
        }

        #endregion

        #region Get Page Data Type

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public ContentTypeInfo<AbstractPageData> GetPageDataTypeByNameSlug(string nameSlug)
        {
            if (nameSlug == null)
            {
                throw new ArgumentNullException(nameof(nameSlug));
            }

            return FindPageDataTypes().SingleOrDefault(x => x.NameSlug == nameSlug);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        public ContentTypeInfo<AbstractPageData> GetPageDataTypeByTypeFullName(string typeFullName)
        {
            if (typeFullName == null)
            {
                throw new ArgumentNullException(nameof(typeFullName));
            }

            return FindPageDataTypes().SingleOrDefault(x => x.Type.FullName == typeFullName);
        }

        #endregion

        #region Find Page Extension Property Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentTypeInfo<AbstractPageExtensionProperties>> FindPageExtensionPropertyTypes()
        {
            var types = FindTypesBasedOn<AbstractPageExtensionProperties>();
            var typeInfos = types.Select(x =>
            {
                var instance = (AbstractPageExtensionProperties)Activator.CreateInstance(x);
                var name = instance.GetName();
                return new ContentTypeInfo<AbstractPageExtensionProperties>(name, x);
            }).ToList();
            return typeInfos;
        }

        #endregion

        #region Find Partial Data Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentTypeInfo<AbstractPartialData>> FindPartialDataTypes()
        {
            var types = FindTypesBasedOn<AbstractPartialData>();
            var typeInfos = types.Select(x =>
            {
                var instance = (AbstractPartialData)Activator.CreateInstance(x);
                var name = instance.GetName();
                return new ContentTypeInfo<AbstractPartialData>(name, x);
            }).ToList();
            return typeInfos;
        }

        #endregion

        #region Get Partial Data Type

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        public ContentTypeInfo<AbstractPartialData> GetPartialDataTypeByNameSlug(string nameSlug)
        {
            if (nameSlug == null)
            {
                throw new ArgumentNullException(nameof(nameSlug));
            }

            return FindPartialDataTypes().Single(x => x.NameSlug == nameSlug);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        public ContentTypeInfo<AbstractPartialData> GetPartialDataTypeByTypeFullName(string typeFullName)
        {
            if (typeFullName == null)
            {
                throw new ArgumentNullException(nameof(typeFullName));
            }

            return FindPartialDataTypes().Single(x => x.Type.FullName == typeFullName);
        }

        #endregion

        #region Find Partial Extension Property Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ContentTypeInfo<AbstractPartialExtensionProperties>> FindPartialExtensionPropertyTypes()
        {
            var types = FindTypesBasedOn<AbstractPartialExtensionProperties>();
            var typeInfos = types.Select(x =>
            {
                var instance = (AbstractPartialExtensionProperties)Activator.CreateInstance(x);
                var name = instance.GetName();
                return new ContentTypeInfo<AbstractPartialExtensionProperties>(name, x);
            }).ToList();
            return typeInfos;
        }

        #endregion

        #region Find Types Based On

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IEnumerable<Type> FindTypesBasedOn<T>()
        {
            var assemblies = _reflectionService.FindApplicationAndLoremIpsumAssemblies();
            var types = assemblies
                .SelectMany(x => x.GetTypes())
                .Where(x => typeof(T).IsAssignableFrom(x))
                .Where(x => !x.GetTypeInfo().IsAbstract)
                .Where(x => x.GetTypeInfo().GetGenericArguments().Length == 0)
                .Where(x => x.HasParamterlessConstructor())
                .ToList();
            return types;
        }

        #endregion

    }

}

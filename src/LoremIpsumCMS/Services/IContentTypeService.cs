﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Model;

namespace LoremIpsumCMS.Services
{

    /// <summary>
    /// 
    /// </summary>
    public interface IContentTypeService
    {

        #region Find Site Extension Property Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ContentTypeInfo<AbstractSiteExtensionProperties>> FindSiteExtensionPropertyTypes();

        #endregion

        #region Find Page Data Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ContentTypeInfo<AbstractPageData>> FindPageDataTypes();

        #endregion

        #region Get Page Data Type

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        ContentTypeInfo<AbstractPageData> GetPageDataTypeByNameSlug(string nameSlug);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        ContentTypeInfo<AbstractPageData> GetPageDataTypeByTypeFullName(string typeFullName);

        #endregion

        #region Find Page Extension Property Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ContentTypeInfo<AbstractPageExtensionProperties>> FindPageExtensionPropertyTypes();

        #endregion

        #region Find Partial Data Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ContentTypeInfo<AbstractPartialData>> FindPartialDataTypes();

        #endregion

        #region Get Partial Data Type

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nameSlug"></param>
        /// <returns></returns>
        ContentTypeInfo<AbstractPartialData> GetPartialDataTypeByNameSlug(string nameSlug);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <returns></returns>
        ContentTypeInfo<AbstractPartialData> GetPartialDataTypeByTypeFullName(string typeFullName);

        #endregion

        #region Find Partial Extension Property Types

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<ContentTypeInfo<AbstractPartialExtensionProperties>> FindPartialExtensionPropertyTypes();

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.Options;
using LoremIpsumCMS.Infrastructure.Reflection;
using LoremIpsumCMS.Infrastructure.Modules;

namespace LoremIpsumCMS.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class ReflectionService : IReflectionService
    {

        #region Dependencies

        private readonly IEnumerable<IModule> _modules;
        private readonly IOptions<LoremIpsumOptions> _options;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modules"></param>
        /// <param name="options"></param>
        public ReflectionService(
            IEnumerable<IModule> modules,
            IOptions<LoremIpsumOptions> options)
        {
            _modules = modules;
            _options = options;
        }

        #endregion

        #region Find Application Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Assembly> FindApplicationAssemblies()
        {
            var applicationNames = _options.Value.ApplicationRootNamespaces;
            var assemblyNames = GetRuntimeAssemblyNames();
            var applicationAssemblyNames = assemblyNames.Where(x => x.IsInRootNamespace(applicationNames)).ToList();

            return applicationAssemblyNames.Select(x => Assembly.Load(x)).ToList();
        }

        #endregion

        #region Find Lorem Ipsum Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Assembly> FindLoremIpsumAssemblies()
        {
            var assemblies = _modules
                .Select(x => x.Assembly)
                .Distinct()
                .ToList();
            return assemblies;
        }

        #endregion

        #region Find Application and Lorem Ipsum Assemblies

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Assembly> FindApplicationAndLoremIpsumAssemblies()
        {
            var assemblies = FindApplicationAssemblies()
                .Concat(FindLoremIpsumAssemblies())
                .Distinct()
                .ToList();
            return assemblies;
        }

        #endregion

        #region Get Runtime Assembly Names

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<AssemblyName> GetRuntimeAssemblyNames()
        {
            var runtimeIdentifier = RuntimeEnvironment.GetRuntimeIdentifier();
            var assemblyNames = DependencyContext.Default.GetRuntimeAssemblyNames(runtimeIdentifier);
            return assemblyNames;
        }

        #endregion

    }

}

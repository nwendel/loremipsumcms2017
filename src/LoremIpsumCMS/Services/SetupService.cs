﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using LoremIpsumCMS.Commands;
using LoremIpsumCMS.Infrastructure.Cqrs;
using LoremIpsumCMS.Model;
using Raven.Client.Documents.Session;

namespace LoremIpsumCMS.Services
{

    /// <summary>
    /// 
    /// </summary>
    public class SetupService : ISetupService
    {

        #region Fields

        private readonly object _lock = new object();
        private bool _isFirstTime = true;

        #endregion

        #region Dependencies

        private readonly IDocumentSession _documentSession;
        private readonly ICommandSender _commandSender;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentSession"></param>
        /// <param name="commandSender"></param>
        public SetupService(
            IDocumentSession documentSession,
            ICommandSender commandSender)
        {
            _documentSession = documentSession;
            _commandSender = commandSender;
        }

        #endregion

        #region Setup

        /// <summary>
        /// 
        /// </summary>
        public bool Setup(string hostname)
        {
            if (!_isFirstTime)
            {
                return false;
            }

            lock (_lock)
            {
                var siteCount = _documentSession.Query<SiteOverview>()
                    .Customize(o => o.WaitForNonStaleResults())
                    .Count();
                if (siteCount != 0)
                {
                    return false;
                }

                var command = new CreateSiteCommand
                {
                    Name = "Placeholder",
                    Hosts = new List<SiteHost> {new SiteHost(hostname, true)},
                    ExtensionProperties = new AbstractSiteExtensionProperties[0],
                    UserPermissions = new UserPermissions[0],
                    CreatedAt = DateTime.Now,
                    // TODO: Not sure how to handle this without making a proper setup wizard...
                    CreatedByUserId = null
                };
                _commandSender.Send(command);

                _isFirstTime = false;
                return true;
            }
        }

        #endregion

    }

}

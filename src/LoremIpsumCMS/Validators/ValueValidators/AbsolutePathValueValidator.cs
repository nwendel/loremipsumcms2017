﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LoremIpsumCMS.Infrastructure.Validation;
using LoremIpsumCMS.Infrastructure.Validation.Internal;
using LoremIpsumCMS.Infrastructure.Validation.ValueValidators;

namespace LoremIpsumCMS.Validators.ValueValidators
{

    /// <summary>
    /// 
    /// </summary>
    public class AbsolutePathValueValidator : IValueValidator<string>
    {

        #region Fields

        private static Regex _pathRegex = new Regex("^((\\/\\w+)+)|\\/$", RegexOptions.Singleline | RegexOptions.Compiled);

        #endregion

        #region Validate

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyChain"></param>
        /// <param name="value"></param>
        /// <param name="serviceProvider"></param>
        /// <returns></returns>
        public IEnumerable<ValidationMessage> Validate(PropertyChain propertyChain, string value, IServiceProvider serviceProvider)
        {
            if(string.IsNullOrWhiteSpace(value))
            {
                yield break;
            }

            var isMatch = _pathRegex.IsMatch(value);
            if(isMatch)
            {
                yield break;
            }

            yield return new ValidationMessage(propertyChain.FullName, "Not a valid path");
        }

        #endregion

    }

}

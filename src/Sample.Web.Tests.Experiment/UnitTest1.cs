using FluentRegistration;
using LoremIpsumCMS.Web.Module.Blogs.Areas.AdminBlogs.Controllers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Sample.Web.Tests.Experiment
{

    /// <summary>
    /// 
    /// </summary>
    public class UnitTest1
    {

        private TestServer _server;

        /// <summary>
        /// 
        /// </summary>
        [Fact(Skip = "asdf")]
        public void Test1()
        {
            _server = new TestServer(
                new WebHostBuilder()
                    .UseContentRoot(@"..\..\..\..\Sample.Web")
                    .UseWebRoot(@"..\..\..\..\Sample.Web\wwwroot")
                    .ConfigureServices(s => s.Register(r => r.For<IHttpContextAccessor>().ImplementedBy<HttpContextAccessor>()))
                    .ConfigureServices(s => s.AddMvc().AddApplicationPart(typeof(AdminShellController).Assembly))
                    .UseStartup<Startup>());

            var client = _server.CreateClient();
            var asdf = client.GetAsync("/admin").Result;
        }

    }

}

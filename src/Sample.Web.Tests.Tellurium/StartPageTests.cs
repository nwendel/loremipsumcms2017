#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Xunit;
using Xunit.AssemblyFixture;
using Sample.Web.Tests.Tellurium.PageObjects;

namespace Sample.Web.Tests.Tellurium
{

    /// <summary>
    /// 
    /// </summary>
    public class StartPageTests : IAssemblyFixture<SampleWebHostFixture>
    {

        private readonly SampleWebHostFixture _sampleWebHost;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sampleWebHost"></param>
        public StartPageTests(SampleWebHostFixture sampleWebHost)
        {
            _sampleWebHost = sampleWebHost;
        }

        /// <summary>
        /// 
        /// </summary>
        [Fact(Skip = "Depends on RavenDB")]
        public void CanOpen()
        {
            using (var driver = _sampleWebHost.CreateDriver())
            {
                var startPage = driver.NavigateTo<StartPage>();
                var startPageTitle = startPage.Title;
                var aboutPage = startPage.NavigateToAboutPage();

                Assert.Equal("Home Page - Sample.Web", startPageTitle);
            }
        }

    }

}

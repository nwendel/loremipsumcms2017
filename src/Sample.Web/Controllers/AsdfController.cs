﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using Microsoft.AspNetCore.Mvc;
using LoremIpsumCMS.Web.Infrastructure.Mvc;

namespace Sample.Web.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class AsdfController : Controller
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult Asdf()
        {
            return null;
            //return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult Asdf2(int id)
        {
            return null;
            //return View();
        }


    }

}

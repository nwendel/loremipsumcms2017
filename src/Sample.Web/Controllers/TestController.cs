﻿using LoremIpsumCMS.Web.Infrastructure.Mvc;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sample.Web.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class TestController : Controller
    {

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet("test")]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult First()
        {
            return PartialView();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ViewComponentAction]
        public IActionResult Second()
        {
            return PartialView();
        }


    }

}

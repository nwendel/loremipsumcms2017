﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using FluentRegistration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using LoremIpsumCMS;
using LoremIpsumCMS.Infrastructure.Logging;
using LoremIpsumCMS.Web;

namespace Sample.Web
{

    /// <summary>
    /// 
    /// </summary>
    public class Startup
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var loggerFactory = LogManager.LoggerFactory;
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        #endregion

        #region Configure Services

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceCollection"></param>
        public void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddMvc();
                //.AddSessionStateTempDataProvider();

            serviceCollection.AddDistributedMemoryCache();
            serviceCollection.AddSession();

            //services.AddIdentity<object, object>()
                //.AddEntityFrameworkStores<ApplicationDbContext>()
                //.AddDefaultTokenProviders();


            serviceCollection.AddAuthentication()
                .AddGoogle(o =>
                {
                    o.ClientId = "22479939463-mul60f1anp4g0gfqe8db0a105umgbiel.apps.googleusercontent.com";
                    o.ClientSecret = "xwlEv1CEOVBdt6-crbyGTD3E";
                });

            serviceCollection.AddLoremIpsum(o =>
            {
                o.IsHostnameOverrideEnabled = true;
            });

            serviceCollection.Install(i => i.FromThisAssembly());
        }

        #endregion

        #region Configure

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseSession();
            app.UseStatusCodePages();

            app.UseMvc();
            app.UseLoremIpsum();
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System.Collections.Generic;
using LoremIpsumCMS.Infrastructure.Validation;
using Sample.Web.Model;

namespace Sample.Web.Validators
{

    /// <summary>
    /// 
    /// </summary>
    public class ContactPartialDataValidator : AbstractValidator<ContactPartialData>
    {

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public ContactPartialDataValidator()
        {
            RuleFor(x => x.Name)
                .NotNullOrWhiteSpace();

            ValidateUsing(ValidateEmailOrAddressRequired);
        }

        #endregion

        #region Validaate Email Or Address Required

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private IEnumerable<ValidationMessage> ValidateEmailOrAddressRequired(IValidationContext<ContactPartialData> context)
        {
            var data = context.Instance;

            if(string.IsNullOrWhiteSpace(data.Email) && string.IsNullOrWhiteSpace(data.Address1))
            {
                yield return new ValidationMessage<ContactPartialData>(context.PropertyChain, x => x.Email, "Address or Email is required");
                yield return new ValidationMessage<ContactPartialData>(context.PropertyChain, x => x.Address1, "Address or Email is required");
            }
        }

        #endregion

    }

}

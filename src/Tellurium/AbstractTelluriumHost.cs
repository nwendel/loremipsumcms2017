﻿using System;

namespace Tellurium
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractTelluriumHost : IDisposable
    {

        #region Fields

        private readonly int _port;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="port"></param>
        protected AbstractTelluriumHost(int port)
        {
            _port = port;
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public int Port => _port;

        #endregion

        #region Dispose

        /// <summary>
        /// 
        /// </summary>
        public abstract void Dispose();

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
namespace Tellurium.PageObjects
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class AbstractPage
    {

        #region Fields

        private AbstractTelluriumHost _host;
        private TelluriumDriver _driver;

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string Url => _driver.InnerDriver.Url;

        /// <summary>
        /// 
        /// </summary>
        public string Title => _driver.InnerDriver.Title;

        #endregion

        #region Initialize

        /// <summary>
        /// 
        /// </summary>
        internal void Initialize(AbstractTelluriumHost host, TelluriumDriver driver)
        {
            _host = host;
            _driver = driver;
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.IO;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.PhantomJS;
using Tellurium.PageObjects;

namespace Tellurium
{

    /// <summary>
    /// 
    /// </summary>
    public class TelluriumDriver : IDisposable
    {

        #region Fields

        private readonly AbstractTelluriumHost _host;
        private readonly IWebDriver _driver;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        public TelluriumDriver(AbstractTelluriumHost host)
        {
            _host = host;
            _driver = CreatePhantomJsDriver();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IWebDriver CreatePhantomJsDriver()
        {
            var callingAssembly = Assembly.GetCallingAssembly();
            var location = callingAssembly.Location;
            var path = Path.GetDirectoryName(location);
            return new PhantomJSDriver(path);
        }

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public IWebDriver InnerDriver => _driver;

        #endregion

        #region Navigate To

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPage"></typeparam>
        /// <param name="pagePath"></param>
        /// <returns></returns>
        public TPage NavigateTo<TPage>(string pagePath)
            where TPage : AbstractPage, new()
        {
            var page = CreatePage<TPage>();
            return NavigateToCore(page, pagePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPage"></typeparam>
        /// <returns></returns>
        public TPage NavigateTo<TPage>()
            where TPage : AbstractPage, IPagePathAware, new()
        {
            var page = CreatePage<TPage>();
            var pagePath = page.PagePath;
            return NavigateToCore(page, pagePath);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPage"></typeparam>
        /// <param name="page"></param>
        /// <param name="pagePath"></param>
        /// <returns></returns>
        private TPage NavigateToCore<TPage>(TPage page, string pagePath)
            where TPage : AbstractPage, new()
        {
            var url = $"http://localhost:{_host.Port}{pagePath}";
            _driver.Navigate().GoToUrl(url);

            return page;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TPage"></typeparam>
        /// <returns></returns>
        private TPage CreatePage<TPage>()
            where TPage : AbstractPage, new()
        {
            var page = new TPage();
            page.Initialize(_host, this);
            return page;
        }

        #endregion

        #region Dispose

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _driver.Dispose();
        }

        #endregion

    }

}

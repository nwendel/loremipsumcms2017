﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Hosting;

namespace Tellurium
{

    /// <summary>
    /// 
    /// </summary>
    public class TelluriumHost3<TStartup> : AbstractTelluriumHost
        where TStartup : class
    {

        #region Fields

        private readonly IWebHost _webHost;
        private readonly string _webProjectName;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="webProjectName"></param>
        /// <param name="port"></param>
        public TelluriumHost3(string webProjectName, int port) 
            : base(port)
        {
            _webProjectName = webProjectName;
            _webHost = StartHost();
            _webHost.Start();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IWebHost StartHost()
        {
            var callingAssembly = Assembly.GetCallingAssembly();
            var assemblyLocation = callingAssembly.Location;
            var assemblyPath = Path.GetDirectoryName(assemblyLocation);
            var assemblyRelativeBinDirectory = assemblyPath.Substring(assemblyPath.IndexOf("\\bin\\", StringComparison.Ordinal) + 1);
            var solutionFolder = FindSolutionFolder(assemblyPath);
            var projectPath = FindWebProjectFolder(solutionFolder, _webProjectName);
            //var projectBinPath = Path.Combine(projectPath, assemblyRelativeBinDirectory);


            return new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(projectPath)
                .UseIISIntegration()
                .UseStartup<TStartup>()
                .UseApplicationInsights()
                .UseUrls($"http://localhost:{Port}")
                .Build();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string FindSolutionFolder(string path)
        {
            var directoryInfo = new DirectoryInfo(path);

            while (directoryInfo != null)
            {
                var foundSolution = directoryInfo.GetFiles("*.sln").Any();
                if (foundSolution)
                {
                    return directoryInfo.FullName;
                }
                directoryInfo = directoryInfo.Parent;
            }
            
            throw new TelluriumException("Solution file not found");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="folderName"></param>
        /// <returns></returns>
        private string FindWebProjectFolder(string path, string folderName)
        {
            var directoryInfo = new DirectoryInfo(path)
                .GetDirectories(folderName, SearchOption.AllDirectories)
                .Single(x => x.Name == folderName);
            return directoryInfo.FullName;
        }

        #endregion

        #region Dispose

        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            _webHost.StopAsync().Wait();
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Xunit.AssemblyFixture
{

    /// <summary>
    /// 
    /// </summary>
    public class AssemblyFixtureTestAssemblyRunner : XunitTestAssemblyRunner
    {

        #region Fields

        private readonly IDictionary<Type, object> _assemblyFixtureMappings = new Dictionary<Type, object>();

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testAssembly"></param>
        /// <param name="testCases"></param>
        /// <param name="diagnosticMessageSink"></param>
        /// <param name="executionMessageSink"></param>
        /// <param name="executionOptions"></param>
        public AssemblyFixtureTestAssemblyRunner(ITestAssembly testAssembly, IEnumerable<IXunitTestCase> testCases, IMessageSink diagnosticMessageSink, 
            IMessageSink executionMessageSink, ITestFrameworkExecutionOptions executionOptions) 
            : base(testAssembly, testCases, diagnosticMessageSink, executionMessageSink, executionOptions)
        {
        }

        #endregion

        #region After Test Assembly Starting Async

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Task AfterTestAssemblyStartingAsync()
        {
            CreateAssemblyFixtures();
            return base.AfterTestAssemblyStartingAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateAssemblyFixtures()
        {
            var testClassTypes = TestCases
                .Select(x => x.TestMethod.TestClass.Class.ToRuntimeType())
                .ToList();
            var assemblyFixtureTypes = testClassTypes
                .SelectMany(t => t.GetInterfaces())
                .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IAssemblyFixture<>))
                .Select(i => i.GetGenericArguments().Single())
                .Distinct()
                .ToList();
            foreach (var assemblyFixtureType in assemblyFixtureTypes)
            {
                Aggregator.Run(() =>
                {
                    var assemblyFixture = Activator.CreateInstance(assemblyFixtureType);
                    _assemblyFixtureMappings.Add(assemblyFixtureType, assemblyFixture);
                });
            }
        }

        #endregion

        #region Run Test Collection Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBus"></param>
        /// <param name="testCollection"></param>
        /// <param name="testCases"></param>
        /// <param name="cancellationTokenSource"></param>
        /// <returns></returns>
        protected override Task<RunSummary> RunTestCollectionAsync(IMessageBus messageBus, ITestCollection testCollection, IEnumerable<IXunitTestCase> testCases, CancellationTokenSource cancellationTokenSource)
        {
            return new AssemblyFixtureTestCollectionRunner(testCollection, testCases, DiagnosticMessageSink, messageBus, TestCaseOrderer, Aggregator, _assemblyFixtureMappings, cancellationTokenSource).RunAsync();
        }

        #endregion

        #region Before Test Assembly Finished Async

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected override Task BeforeTestAssemblyFinishedAsync()
        {
            DisposeAssemblyFixtures();

            return base.BeforeTestAssemblyFinishedAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        private void DisposeAssemblyFixtures()
        {
            foreach (var disposable in _assemblyFixtureMappings.Values.OfType<IDisposable>())
            {
                Aggregator.Run(disposable.Dispose);
            }
        }

        #endregion

    }

}

﻿#region License
// Copyright (c) Niklas Wendel 2017-2018
// 
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
// 
// http://www.apache.org/licenses/LICENSE-2.0 
// 
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License.
#endregion
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Xunit.AssemblyFixture
{

    /// <summary>
    /// 
    /// </summary>
    public class AssemblyFixtureTestCollectionRunner : XunitTestCollectionRunner
    {

        #region Fields

        private readonly IMessageSink _diagnosticMessageSink;
        private readonly IDictionary<Type, object> _assemblyFixtureMappings;

        #endregion

        #region Constructor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testCollection"></param>
        /// <param name="testCases"></param>
        /// <param name="diagnosticMessageSink"></param>
        /// <param name="messageBus"></param>
        /// <param name="testCaseOrderer"></param>
        /// <param name="aggregator"></param>
        /// <param name="assemblyFixtureMappings"></param>
        /// <param name="cancellationTokenSource"></param>
        public AssemblyFixtureTestCollectionRunner(ITestCollection testCollection, IEnumerable<IXunitTestCase> testCases, IMessageSink diagnosticMessageSink, 
            IMessageBus messageBus, ITestCaseOrderer testCaseOrderer, ExceptionAggregator aggregator, IDictionary<Type, object> assemblyFixtureMappings, CancellationTokenSource cancellationTokenSource) 
            : base(testCollection, testCases, diagnosticMessageSink, messageBus, testCaseOrderer, aggregator, cancellationTokenSource)
        {
            _diagnosticMessageSink = diagnosticMessageSink;
            _assemblyFixtureMappings = assemblyFixtureMappings;
        }

        #endregion

        #region Run Test Class Async

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testClass"></param>
        /// <param name="class"></param>
        /// <param name="testCases"></param>
        /// <returns></returns>
        protected override Task<RunSummary> RunTestClassAsync(ITestClass testClass, IReflectionTypeInfo @class, IEnumerable<IXunitTestCase> testCases)
        {
            var combinedFixtures = new Dictionary<Type, object>(_assemblyFixtureMappings);
            foreach (var collectionFixtureMapping in CollectionFixtureMappings)
            {
                combinedFixtures.Add(collectionFixtureMapping.Key, collectionFixtureMapping.Value);
            }

            return new XunitTestClassRunner(testClass, @class, testCases, _diagnosticMessageSink, MessageBus, TestCaseOrderer, new ExceptionAggregator(Aggregator), CancellationTokenSource, combinedFixtures).RunAsync();
        }

        #endregion

    }

}
